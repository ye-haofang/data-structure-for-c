#define _CRT_SECURE_NO_WARNINGS 1	
//HJ55 ��7
#include<stdio.h>
#include<stdbool.h>

bool getNumSeven(int n)
{
    if (n % 7 == 0)
        return true;
    while (n)
    {
        if (n % 10 == 7)
            return true;
        n /= 10;
    }

    return false;
}

int main() {
    int num = 0;
    scanf("%d", &num);
    int count = 0;
    for (int i = 1; i <= num; i++)
    {
        if (getNumSeven(i))
            count++;
    }

    printf("%d\n", count);
    return 0;
}