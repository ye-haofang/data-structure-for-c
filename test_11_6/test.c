#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct ListNode {
    int val;
    struct ListNode* next;
}SLTNode;

//#include<stdlib.h>
////分割链接
//SLTNode* partition(SLTNode* pHead, int x)
//{
//    //创建两个带头新链表
//    struct ListNode* L1tail, * L1guard; //带头链表1
//    struct ListNode* L2tail, * L2guard; //带头链表2
//    L1guard = L1tail = (SLTNode*)malloc(sizeof(SLTNode));   //在堆上开辟，确保一致性
//    L2guard = L2tail = (SLTNode*)malloc(sizeof(SLTNode));
//
//    struct ListNode* cur = pHead;   //节点委托
//    while (cur)
//    {
//        //带头以后，尾插就很容易
//        if (cur->val < x)
//        {
//            L1tail->next = cur;
//            L1tail = L1tail->next;
//        }
//        else
//        {
//            L2tail->next = cur;
//            L2tail = L2tail->next;
//        }
//        cur = cur->next;
//    }
//
//    L1tail->next = L2guard->next;   //将链表1和链表2链接起来
//    L2tail->next = NULL;    //链表2的尾节点需要指向NULL
//
//    pHead = L1guard->next;  //更新题目链表的头节点信息
//
//    free(L1guard);  //释放申请的空间
//    free(L2guard);
//
//    return pHead;   //返回新的头节点信息
//}

////CM11。链表分割
////不带头版
//SLTNode* partition(SLTNode* pHead, int x)
//{
//    SLTNode* p1Head, * p1tail, * p2Head, * p2tail;  //这是不带头版
//    p1Head = p1tail = p2Head = p2tail = NULL;   //首先两对头尾都置空
//    SLTNode* cur = pHead;   //节点托管
//    while (cur)
//    {
//        //如果小于x，链接给链表1
//        if (cur->val < x)
//        {
//            if (!p1Head)
//                p1Head = p1tail = cur;  //不带头节点，尾插时的特殊处理
//            else
//            {
//                p1tail->next = cur;
//                p1tail = p1tail->next;
//            }
//        }
//        //否则链接给链表2
//        else
//        {
//            if (!p2Head)
//                p2Head = p2tail = cur;
//            else
//            {
//                p2tail->next = cur;
//                p2tail = p2tail->next;
//            }
//        }
//        cur = cur->next;
//    }
//    //如果链表1为空，说明元素都分给链表2了，返回链表2就行了
//    //如果两者都是空，那么返回链表2也行
//    if (!p1tail)
//        return p2Head;
//    p1tail->next = p2Head;  //将链表1和链表2链接起来
//    //如果链表2不为空，就需要把它的尾节点指向空
//    if (p2tail)
//        p2tail->next = NULL;
//    return p1Head;  //最后返回链表1
//}

////OR36.链表的回文结构
//struct ListNode* middleNode(struct ListNode* head) {
//    //思路：快指针和慢指针呈两倍关系
//    struct ListNode* pfast = head;  //快指针
//    struct ListNode* plow = head;   //慢指针
//    while (pfast && (pfast->next)) {
//        pfast = pfast->next->next;  //快指针每次走两步
//        plow = plow->next;  //慢指针每次走一步
//    }
//
//    return plow;    //当快指针走到头后，返回慢指针
//}
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* cur = head;
//    struct ListNode* prev = NULL;
//    struct ListNode* tail = NULL;
//    while (cur) {
//        prev = cur->next;   //翻转前要先记录下一个节点信息，不然待会就找不到了
//        cur->next = tail;   //将节点链接至新链表
//        tail = cur; //新链表信息更新
//        cur = prev; //老链表信息更新，指向下一个节点
//    }
//    return tail;    //此时的tail就是新链表的头节点
//}
//bool chkPalindrome(ListNode* A) {
//    ListNode* mid = middleNode(A);
//    mid = reverseList(mid);
//    while (mid)
//    {
//        if (A->val != mid->val)
//            return false;
//        A = A->next;
//        mid = mid->next;
//    }
//    return true;
//}

//typedef SLTNode ListNode;
////OR36.链表的回文结构
////三步走
////1.找到中间位置
////2.翻转中间位置后的链表
////3.从中间位置和头节点处开始往后比对，如果发现不相等
////就说明这不是回文链表
//ListNode* findMid(ListNode* head)
//{
//    ListNode* pfast = head; //快指针与慢指针
//    ListNode* pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;  //快指针走两步
//        pslow = pslow->next;    //慢指针走一步1
//    }
//
//    return pslow;
//}
//ListNode* reverList(ListNode* phead)
//{
//    ListNode* left = NULL;
//    ListNode* mid = phead;
//    ListNode* right = phead->next;  //框定新链表
//    while (mid)
//    {
//        mid->next = left;   //链接
//        left = mid; //移动
//
//        mid = right;    //移动
//        if (right)
//            right = right->next;    //合法才能移动
//    }
//
//    return left;    //此时的左就是头节点
//}
//bool chkPalindrome(ListNode* A)
//{
//    ListNode* mid = findMid(A); //获取中间节点
//    mid = reverList(mid);   //翻转中间节点后的链表
//    while (mid)
//    {
//        //如果发现不相等的元素，那么就不是回文结构
//        if (A->val != mid->val)
//            return false;
//        mid = mid->next;    //向后移动
//        A = A->next;
//    }
//
//    return true;
//}

extern void SLTPushBack(SLTNode**, int*, int);

int main()
{
    int arr[] = { 1,2,3,1 };
    SLTNode* s = NULL;
    SLTPushBack(&s, arr, sizeof(arr)/sizeof(arr[0]));
    bool b = chkPalindrome(s);
    if (b)
        printf("true\n");
    else
        printf("false\n");
    return 0;
}
//
////160.相交链表
//#include<math.h>
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
//{
//    size_t countA, countB;
//    countA = countB = 0;
//    //获取headA的长度
//    struct ListNode* curA = headA;
//    while (curA->next)
//    {
//        countA++;
//        curA = curA->next;
//    }
//
//    //获取headB的长度
//    struct ListNode* curB = headB;
//    while (curB->next)
//    {
//        countB++;
//        curB = curB->next;
//    }
//
//    //特殊处理，如果尾节点不同，就说明肯定没有相交点
//    if (curA != curB)
//        return NULL;
//
//    size_t gap = (size_t)abs((int)(countA - countB));   //获取差值
//    struct ListNode* longList, * shortList; //设立长短链表
//    longList = headA;   //尝试赋值
//    shortList = headB;
//    if (countA < countB)
//    {
//        longList = headB;   //如果条件成立，修正赋值
//        shortList = headA;
//    }
//
//    //较长链表先向后走gap步
//    while (gap--)
//        longList = longList->next;
//
//    //一起向后走并比较，如果相等，就说明找到了相交点
//    while (longList)
//    {
//        if (longList == shortList)
//            return longList;
//
//        longList = longList->next;
//        shortList = shortList->next;
//    }
//
//    return NULL;
//}

// //141.环形链表
//bool hasCycle(struct ListNode* head)
//{
//    struct ListNode* pfast = head;  //快慢指针
//    struct ListNode* pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;  //快指针走两步
//        pslow = pslow->next;    //慢指针走一步
//        //如果有环，慢指针早晚会追上快指针
//        if (pfast == pslow)
//            return true;
//    }
//    return false;
//}

// //142.环形链表二
// //解法二，通过强行变成相交问题解决
//#include<math.h>
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
//{
//    size_t countA, countB;
//    countA = countB = 0;
//    //获取headA的长度
//    struct ListNode* curA = headA;
//    while (curA->next)
//    {
//        countA++;
//        curA = curA->next;
//    }
//
//    //获取headB的长度
//    struct ListNode* curB = headB;
//    while (curB->next)
//    {
//        countB++;
//        curB = curB->next;
//    }
//
//    //特殊处理，如果尾节点不同，就说明肯定没有相交点
//    if (curA != curB)
//        return NULL;
//
//    size_t gap = (size_t)abs((int)(countA - countB));   //获取差值
//    struct ListNode* longList, * shortList; //设立长短链表
//    longList = headA;   //尝试赋值
//    shortList = headB;
//    if (countA < countB)
//    {
//        longList = headB;   //如果条件成立，修正赋值
//        shortList = headA;
//    }
//
//    //较长链表先向后走gap步
//    while (gap--)
//        longList = longList->next;
//
//    //一起向后走并比较，如果相等，就说明找到了相交点
//    while (longList)
//    {
//        if (longList == shortList)
//            return longList;
//
//        longList = longList->next;
//        shortList = shortList->next;
//    }
//
//    return NULL;
//}
//struct ListNode* detectCycle(struct ListNode* head) {
//    struct ListNode* pfast = head;  //快慢指针
//    struct ListNode* pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;  //快指针走两步
//        pslow = pslow->next;    //慢指针走一步
//        //如果有环，慢指针早晚会追上快指针
//        if (pfast == pslow)
//        {
//            struct ListNode* otherList = pfast->next;
//            pfast->next = NULL; //手动创建一个链表
//            struct ListNode* cur = getIntersectionNode(head, otherList);
//            return cur;
//        }
//    }
//    return NULL;    //如果没有环，就返回空
//}