#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>


//3320.Jessica's Reading Problem
//http://poj.org/problem?id=3320

int main()
{
	int len = 0;
	scanf("%d", &len);
	int* space = (int*)calloc(len, sizeof(int));
	assert(space);

	int pos = 0;
	while (pos < len)
		scanf("%d", &space[pos++]);

	int conLen = -1;	//最大连续长度
	int conTmp = 0;
	int conPos = 0;	//下标
	int posTmp = 0;
	int tmp = 0;

	pos = 0;
	while (pos < len - 1)
	{
		tmp = space[pos + 1];
		if (space[pos] == tmp)
			conTmp++;
		else
		{
			if (conTmp > conLen)
			{
				conLen = conTmp;
				conPos = posTmp;
			}
			posTmp = pos + 1;
			conTmp = 0;
		}

		pos++;
	}

	//补偿
	if(conTmp > conLen)
		conPos = posTmp;

	printf("%d\n", conPos + 1);

	free(space);
	space = NULL;

	return 0;
}