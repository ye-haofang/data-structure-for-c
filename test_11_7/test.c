#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

//class Partition {
//public:
//    //CM11.链表分割
//#include<stdio.h>
//    ListNode* partition(ListNode* pHead, int x)
//    {
//        ListNode* guard1, * tail1, * guard2, * tail2;  //两个带哨兵位链表
//        guard1 = tail1 = (ListNode*)malloc(sizeof(ListNode));   //在堆上申请空间
//        guard2 = tail2 = (ListNode*)malloc(sizeof(ListNode));
//        ListNode* cur = pHead;  //节点委托
//        //遍历整个链表
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                tail1->next = cur;
//                tail1 = cur;
//            }
//            else
//            {
//                tail2->next = cur;
//                tail2 = cur;
//            }
//
//            cur = cur->next;
//        }
//        tail1->next = guard2->next;
//        tail2->next = NULL;
//
//        pHead = guard1->next;   //链接
//        free(guard1);   //释放申请的空间
//        free(guard2);
//        guard1 = guard2 = NULL;
//
//        return pHead;   //返回最新的头节点
//    }
//};'

struct ListNode {
    int val;
    struct ListNode* next;
};

//
////OR36 链表的回文结构
////1.找到中点
//struct ListNode* findMidNode(struct ListNode* phead)
//{
//    assert(phead);
//    struct ListNode* pfast = phead;
//    struct ListNode* pslow = phead;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;  //快指针走两步
//        pslow = pslow->next;    //慢指针走一步
//    }
//
//    return pslow;   //返回慢指针
//}
////2.反转链表
//struct ListNode* reverList(struct ListNode* phead)
//{
//    assert(phead);
//    struct ListNode* newhead = NULL;   //新链表
//    struct ListNode* cur = phead;
//    while (cur)
//    {
//        struct ListNode* tmp = cur->next;
//        cur->next = newhead;
//        newhead = cur;
//
//        cur = tmp;
//    }
//
//    return newhead;
//}
////3.对比链表
//bool chkPalindrome(struct ListNode* A) {
//    // write code here
//    struct ListNode* mid = findMidNode(A);
//    mid = reverList(mid);
//
//    struct ListNode* cur = A;
//    while (mid)
//    {
//        if (cur->val != mid->val)
//            return false;
//        cur = cur->next;
//        mid = mid->next;
//    }
//
//    return true;
//}

////160.相交链表
//#include<assert.h>
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
//{
//    assert(headA && headB);
//
//    //找出两个链表间的差值
//    size_t len1 = 0;
//    size_t len2 = 0;
//
//    struct ListNode* pA, * pB;
//    pA = headA, pB = headB;
//    while (pA->next)
//    {
//        len1++;
//        pA = pA->next;
//    }
//
//    while (pB->next)
//    {
//        len2++;
//        pB = pB->next;
//    }
//
//    //如果连尾都不相等，就说明肯定不相交
//    if (pA != pB)
//        return NULL;
//
//    size_t gap = (size_t)abs((int)(len1 - len2));   //差值
//
//    struct ListNode* longList, * shortList;
//    longList = headA, shortList = headB;    //假设
//    if (len1 < len2)
//        longList = headB, shortList = headA;    //修正
//
//    //长链表向后走
//    while (gap--)
//        longList = longList->next;
//
//    //比对，找到相交点
//    while (longList)
//    {
//        if (longList == shortList)
//            return longList;
//        longList = longList->next;
//        shortList = shortList->next;
//    }
//
//    return NULL;    //其实这条语句都进不去
//}

////141.环形链表
////两倍差值法
//bool hasCycle(struct ListNode* head)
//{
//    struct ListNode* pfast, * pslow; //快慢指针 
//    pfast = head, pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;  //快指针走两步
//        pslow = pslow->next;    //慢指针走一步
//
//        //如果两个指针相等，就说明有环
//        if (pfast == pslow)
//            return true;
//    }
//
//    return false;
//}

// //142.环形链表二
// //解法一，数学推论法
// //假设链表头到入环口的距离为 L
// //整个环的长度为 C，k 为走的圈数
// //入环口到快慢指针相遇点处的距离为 N
// //依据：快指针走的速度是慢指针的两倍
// //当两者相遇后，走过的路程可以表示为： 2(L+N) = L+k*C+N
// //推导：L+N = k*C -> L = k*C - N 
// //其中 k*C - N 可以看作从相遇点到入环口的距离
//struct ListNode* detectCycle(struct ListNode* head)
//{
//    struct ListNode* pfast, * pslow;
//    pfast = pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;
//        pslow = pslow->next;
//        if (pfast == pslow)
//        {
//            struct ListNode* meet = pfast;
//            struct ListNode* cur = head;
//            //这个循环肯定会结束的
//            while (cur != meet)
//            {
//                cur = cur->next;
//                meet = meet->next;
//            }
//            return cur; //此时的相遇点就是入环口
//        }
//    }
//
//    return NULL;
//}

////142.环形链表二
////解法2，转为相交链表
////160.相交链表
//struct ListNode* detectCycle(struct ListNode* head)
//{
//    struct ListNode* pfast, * pslow;
//    pfast = pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;
//        pslow = pslow->next;
//        if (pfast == pslow)
//        {
//            struct ListNode* otherList = pfast->next;   //强行断成一个新链表
//            pfast->next = NULL;
//            return getIntersectionNode(head, otherList);    //直接调用函数
//        }
//    }
//
//    return NULL;
//}

struct Node 
{
    int val;
    struct Node* next;
    struct Node* random;
};

////138.复制带随机指针的链表
//
//#include<stdlib.h>
//#include<assert.h>
//
//
//struct Node* buynewnode(int x)
//{
//    //常规买节点
//    struct Node* newnode = (struct Node*)malloc(sizeof(struct Node));
//    assert(newnode);
//
//    newnode->val = x;
//    newnode->next = NULL;   //两端最好都置空一下
//    newnode->random = NULL;
//
//    return newnode;
//}
//
//struct Node* findNode(struct Node* head, struct Node* cur, struct Node* tmp)
//{
//    //地址唯一性，通过步数解决重复问题
//    size_t step = 0;
//    //找random中存储地址与相同的节点地址之间的步数
//    while (head != cur)
//    {
//        head = head->next;
//        step++;
//    }
//
//    while (step--)
//        tmp = tmp->next;    //同步指向
//
//    return tmp; //返回指针，即使指向空，也能解决
//}
//
//struct Node* copyRandomList(struct Node* head)
//{
//    struct Node* guard, * tail; //带哨兵位的链表
//    guard = tail = (struct Node*)malloc(sizeof(struct Node));
//    assert(guard);
//
//    struct Node* cur = head;
//    //给新链表中尾插上数据
//    while (cur)
//    {
//        struct Node* newnode = buynewnode(cur->val);
//        tail->next = newnode;
//        tail = newnode;
//
//        cur = cur->next;
//    }
//
//    //如果head为空，就说明原表为空表，返回空
//    if (!head)
//        return NULL;
//
//    struct Node* tmp = guard->next; //存储新链表的头节点
//    cur = head; //矫正
//    //同样的，循环n次，将所有的random指向正确位置
//    while (tmp)
//    {
//        struct Node* prev = findNode(head, cur->random, guard->next); //待链接的节点
//        tmp->random = prev; //链接
//
//        cur = cur->next;    //向后移动
//        tmp = tmp->next;
//    }
//
//    head = guard->next; //更新头节点信息
//    free(guard);    //释放
//    guard = tail = NULL;
//
//    return head;
//}
//
//typedef struct Node SLTNode;
//
//extern void SLTPushBack(SLTNode**, int*, int);
//
//int main()
//{
//    SLTNode* n1 = (SLTNode*)malloc(sizeof(SLTNode));
//    SLTNode* n2 = (SLTNode*)malloc(sizeof(SLTNode));
//    SLTNode* n3 = (SLTNode*)malloc(sizeof(SLTNode));
//    SLTNode* n4 = (SLTNode*)malloc(sizeof(SLTNode));
//    SLTNode* n5 = (SLTNode*)malloc(sizeof(SLTNode));
//
//    n1->val = 7;
//    n1->next = n2;
//    n1->random = NULL;
//
//    n2->val = 13;
//    n2->next = n3;
//    n2->random = n1;
//
//    n3->val = 11;
//    n3->next = n4;
//    n3->random = n5;
//
//    n4->val = 10;
//    n4->next = n5;
//    n4->random = n3;
//
//    n5->val = 1;
//    n5->next = NULL;
//    n5->random = n1;
//
//    n1 = copyRandomList(n1);
//
//    return 0;
//}