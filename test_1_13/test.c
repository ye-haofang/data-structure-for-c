#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<math.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

////写错了，不是层序遍历
//typedef struct TreeNode* QListDataType;	//队列的数据类型
//
////这是队列中，单个节点的信息
//typedef struct QListNode
//{
//    QListDataType data;
//    struct QListNode* next;
//}QNode;
//
////这是整个队列的信息，包含了队头和队尾两个指针
//typedef struct QueueNode
//{
//    QNode* front;	//队头指针
//    QNode* rear;	//队尾指针
//    size_t size;	//队列长度
//}Queue;
//
//bool QueueEmpty(Queue* pq);	//判断当前队空情况
//
//void QueueInit(Queue* pq)	//队列的初始化
//{
//    assert(pq);
//
//    pq->front = pq->rear = NULL;
//    pq->size = 0;   //初始化状态
//}
//
//void QueueDestroy(Queue* pq)	//队列的销毁
//{
//    assert(pq);
//
//    if (pq->front)
//    {
//        while (pq->front)
//        {
//            QNode* tmp = pq->front->next;
//            free(pq->front);
//            pq->front = tmp;
//        }
//    }
//
//    pq->front = pq->rear = NULL;
//    pq->size = 0;
//}
//
//static QNode* BuyNewNode(QListDataType x)   //买节点
//{
//    QNode* newNode = (QNode*)malloc(sizeof(QNode) * 1);
//    assert(newNode);
//
//    newNode->data = x;
//    newNode->next = NULL;
//
//    return newNode;
//}
//
//void QueuePush(Queue* pq, QListDataType x)	//入队
//{
//    assert(pq);
//
//    QNode* newNode = BuyNewNode(x);
//
//    if (pq->front)
//    {
//        pq->rear->next = newNode;
//        pq->rear = newNode;
//    }
//    else
//        pq->front = pq->rear = newNode;
//
//    pq->size++;
//}
//
//void QueuePop(Queue* pq)	//出队
//{
//    assert(pq);
//    assert(!QueueEmpty(pq));    //栈空不能出队
//
//    QNode* tmp = pq->front->next;
//    free(pq->front);
//    pq->front = tmp;
//    pq->size--;
//}
//
//QListDataType QueueFront(Queue* pq)	//查看队头元素
//{
//    assert(pq);
//    assert(!QueueEmpty(pq));
//
//    return pq->front->data;
//}
//
//QListDataType QueueRear(Queue* pq)	//查看队尾元素
//{
//    assert(pq);
//    assert(!QueueEmpty(pq));
//
//    return pq->rear->data;
//}
//
//int QueueSize(Queue* pq)	//当前队列的有效元素数
//{
//    assert(pq);
//
//    return pq->size;
//}
//
//bool QueueEmpty(Queue* pq)	//判断当前队空情况
//{
//    assert(pq);
//
//    return pq->size == 0;
//}
//
////1022. 从根到叶的二进制数之和
//
//// 二叉树叶子节点个数
//int BinaryTreeLeafSize(struct TreeNode* root)
//{
//    if (root == NULL)
//        return 0;
//
//    return 1 + BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
//}
//
//// 层序遍历
//void BinaryTreeLevelOrder(struct TreeNode* root, int* pa)
//{
//    //思路：利用队列，一层出带一层入
//    Queue Q1;
//    QueueInit(&Q1);
//
//    if (root)
//        QueuePush(&Q1, root);
//
//    int pos = 0;
//    while (!QueueEmpty(&Q1))
//    {
//        struct TreeNode* tmp = QueueFront(&Q1);
//        pa[pos++] = tmp->val;
//        QueuePop(&Q1);
//        if (tmp->left)
//            QueuePush(&Q1, tmp->left);
//        if (tmp->right)
//            QueuePush(&Q1, tmp->right);
//    }
//
//    QueueDestroy(&Q1);
//}
//
//int sumRootToLeaf(struct TreeNode* root) {
//    //层序遍历获取值，然后就是普通的二进制转十进制问题了
//    //先获取二叉树的节点数，动态开辟空间
//    int nodeNum = 0;
//    nodeNum = BinaryTreeLeafSize(root);
//
//    int* pa = (int*)malloc(sizeof(int) * nodeNum);
//    assert(pa);
//
//    BinaryTreeLevelOrder(root, pa);
//
//    int pos = nodeNum - 1;
//    int nodeVal = 0;
//    int weight = 0;
//    while (pos >= 0)
//    {
//        nodeVal += (pa[pos] * (int)pow((double)2, (double)weight));
//        pos--;
//        weight++;
//    }
//
//    free(pa);
//    pa = NULL;
//
//    return nodeVal;
//}
//
// 

////1022. 从根到叶的二进制数之和
//
////二叉树的深度
//int BinaryTreeDepth(struct TreeNode* root)
//{
//    if (!root)
//        return 0;
//
//    //大问题化小问题：求左右子树的最大深度
//
//    int leftDepth = BinaryTreeDepth(root->left);
//    int rightDepth = BinaryTreeDepth(root->right);
//
//    return (leftDepth > rightDepth ? leftDepth : rightDepth) + 1;	//左右根
//}
//
////递归获取权重值
//int getBinSum(struct TreeNode* root, int tmp, int weight)
//{
//    if (!root)
//        return 0;
//
//    int sum = tmp + root->val * (int)pow(2, weight);
//
//    if (!root->left && !root->right)
//        return sum;
//
//    //左右路递归
//    int leftSum = getBinSum(root->left, sum, weight - 1);
//    int rightSum = getBinSum(root->right, sum, weight - 1);
//
//    return leftSum + rightSum;
//}
//
//int sumRootToLeaf(struct TreeNode* root) {
//    //先获取深度，根据权重值玩递归
//    int depth = BinaryTreeDepth(root);
//    int weight = depth - 1;
//    int binSum = getBinSum(root, 0, weight);
//
//    return binSum;
//}

//1022. 从根到叶的二进制数之和


//权重方案废弃
//重改：递归至左右节点都为空的地方，就计算并返回
//第三次改动：改用数组，防止越界问题

int noneSpace[32] = { 0 };  //申请使用全局变量

int getBinVal(int pos)
{
    int val = 0;
    int weight = 0;
    while (pos >= 0)
    {
        val += ((noneSpace[pos] % 10) * (int)pow(2, weight));
        pos--;
        weight++;
    }

    return val;
}

int getBinSum(struct TreeNode* root, int pos)
{
    if (!root)
        return 0;

    noneSpace[pos++] = root->val;

    if (!root->left && !root->right)
        return getBinVal(pos - 1);

    return getBinSum(root->left, pos) + getBinSum(root->right, pos);
}

int sumRootToLeaf(struct TreeNode* root) {
    //先获取深度，根据权重值玩递归
    int binSum = getBinSum(root, 0);

    return binSum;
}

// 通过前序遍历的数组"A B D # # E # H # # C F # # G # #"构建二叉树
struct TreeNode* BinaryTreeCreate(char* a, int n, int* pi)
{
    assert(a);

    //如果一开始就为 # 就没必要创建了
    if (a[*pi] == '#')
    {
        (*pi)++;	//向后移动，找到下一个值
        return NULL;
    }

    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    assert(node);

    node->val = a[(*pi)++] - '0';	//赋值，并向后移动

    node->left = BinaryTreeCreate(a, n, pi);	//左右链接
    node->right = BinaryTreeCreate(a, n, pi);

    return node;	//最开始的节点就是根节点
}

int main()
{
    char arr[] = "100##1##10##1##";
    int pos = 0;
    struct TreeNode* root = BinaryTreeCreate(arr, 0, &pos);
    int n = sumRootToLeaf(root);
    return 0;
}
