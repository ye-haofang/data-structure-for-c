#define _CRT_SECURE_NO_WARNINGS 1	
//1024 程序员节快乐

#include<stdio.h>

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//	int c = a + b;
//	printf("%d\n", c);
//	return 0;
//}


////思路一
////暴力解决
//#include<stdlib.h>
////qsort 中的比较函数
//int cmp(void* e1, void* e2)
//{
//	return *(int*)e1 - *(int*)e2;	//需要强转为整型
//}
//int missingNumber(int* nums, int numsSize) {
//	qsort(nums, numsSize, sizeof(int), cmp);	//先排序
//	int i = 0;
//	//注意：条件是 i < numsSize,如果写成 i <= numsSize 数组会越界的
//	for (i = 0; i < numsSize; i++)
//	{
//		if (i != nums[i])
//			break;
//	}
//	//如果循环不是终止，而是正常结束的，说明在整个数组中都没有找到目标
//	//即目标是 N ，此时也不用担心，循环正常结束后，i 还会加一次
//	return i;
//}


////思路二
////相加相减
//int missingNumber(int* nums, int numsSize) {
//	int tmp = 0;	//临时存储数组和值
//	int i = 0;
//	for (i = 0; i < numsSize; i++)
//		tmp += nums[i];
//	int sum = 0;	//存储理想状态下的和值
//	for (i = 0; i <= numsSize; i++)
//		sum += i;
//
//	return sum - tmp;	//直接返回二者的差值
//}

////思路三
////异或求值
//int missingNumber(int* nums, int numsSize) {
//	int tmp = 0;	//临时存储数组元素异或值
//	int i = 0;
//	for (i = 0; i < numsSize; i++)
//		tmp ^= nums[i];	//逐元素异或
//
//	int sum = 0;	//存储理想状态下的异或值
//	for (i = 0; i <= numsSize; i++)
//		sum ^= i;	//也是逐元素异或
//
//	return tmp ^ sum;	//返回二者的异或结果
//}
//
//int main()
//{
//	int i = 0;
//	int sum = 0;
//	for (i = 0; i < 7; i++)
//	{
//		sum ^= i;
//	}
//	int ret = sum;
//	sum ^= 3;
//	printf("%d %d\n", sum, ret);
//}

////思路一
////分割重组
//void rotate(int* nums, int numsSize, int k) {
//	int ret = k;
//	int tmp[3];
//	int i = 0;
//	int j = numsSize - k;
//	//分割存储
//	while (ret--)
//	{
//		tmp[i] = nums[j];
//		i++;
//		j++;
//	}
//
//	ret = numsSize - k;
//	i = numsSize - 1;
//	j = numsSize - k - 1;
//	//移动
//	while (ret--)
//	{
//		nums[i] = nums[j];
//		i--;
//		j--;
//	}
//
//	ret = k;
//	i = 0;
//	//重新赋值
//	while (ret--)
//	{
//		nums[i] = tmp[i];
//		i++;
//	}
//}
//int main()
//{
//	int nums[] = { 1,2,3,4,5,6,7 };
//	rotate(nums, 7, 3);
//	return 0;
//}

//三步翻转
//翻转函数，通过下标操作
void rever(int* nums, int left, int right) 
{
	while (left < right)
	{
		//交换需要借助第三个变量
		int tmp = nums[left];
		nums[left] = nums[right];
		nums[right] = tmp;
		left++;
		right--;
	}
}
void rotate(int* nums, int numsSize, int k) {
	rever(nums, 0, numsSize - 1);	//第一次翻转
	rever(nums, 0, k - 1);	//第二次翻转
	rever(nums, k, numsSize - 1);	//第三次翻转
}