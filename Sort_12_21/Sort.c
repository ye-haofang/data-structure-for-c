#define _CRT_SECURE_NO_WARNINGS 1	
#include"Sort.h"

//直接插入排序
void InsertSort(int* pa, int n)
{
	assert(pa);

	//从后往前比较，找到合适位置就插入
	for (int i = 1; i < n; i++)
	{
		int end = i;
		int tmp = pa[end];
		while (end)
		{
			if (pa[end - 1] > tmp)
				pa[end] = pa[end - 1];
			else
				break;
			end--;
		}
		pa[end] = tmp;
	}
}

//希尔排序
void ShellSort(int* pa, int n)
{
	assert(pa);

	//思路：在插入排序的基础上，先分为n个区间，使数组尽可能有序（预排序）
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;	//确保gap最后为1
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = pa[end + gap];
			while (end >= 0)
			{
				//此时的end位于tmp之前s
				if (pa[end] > tmp)
					pa[end + gap] = pa[end];
				else
					break;

				end -= gap;
			}

			pa[end + gap] = tmp;
		}
	}
}

void swap(int*pnum1, int* pnum2)
{
	assert(pnum1 && pnum2);

	int tmp = *pnum1;
	*pnum1 = *pnum2;
	*pnum2 = tmp;
}

//简单选择排序
void SelectSort(int* pa, int n)
{
	assert(pa);

	//思路：选最小的放前面，选最大的放后面
	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		int maxi = begin;
		int mini = begin;

		for (int i = begin + 1; i <= end; i++)
		{
			if (pa[i] > pa[maxi])
				maxi = i;
			if (pa[i] < pa[mini])
				mini = i;
		}
		swap(&pa[begin], &pa[mini]);
		if (maxi == begin)
			maxi = mini;

		swap(&pa[end], &pa[maxi]);

		begin++, end--;
	}
}

void AdjustDown(int* pa, int n, int parent)
{
	assert(pa);

	//大堆，找大孩子，调整
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && pa[child + 1] > pa[child])
			child++;

		if (pa[child] > pa[parent])
		{
			swap(&pa[child], &pa[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}

//堆排序
void HeapSort(int* pa, int n)
{
	assert(pa);

	//思路：升序建大堆，将堆顶元素沉底，然后再调整
	int parent = (n - 1 - 1) / 2;	//找父亲
	for (int i = parent; i >= 0; i--)
		AdjustDown(pa, n, i);

	//将堆顶元素沉底后调整
	int end = n - 1;
	while (end > 0)
	{
		swap(&pa[0], &pa[end]);
		AdjustDown(pa, end--, 0);
	}
}

/*-----------------分割线-------------------*/
//排序博客，上半部分已发布

//冒泡排序
void BubbleSort(int* pa, int n)
{
	assert(pa);

	//思路：升序，当前值比后值大，就交换
	for (int i = 0; i < n - 1; i++)
	{
		bool flag = true;	//一个小优化，虽然没什么用

		//冒泡的次数，与 i 挂钩
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (pa[j] > pa[j + 1])
			{
				swap(&pa[j], &pa[j + 1]);
				flag = false;
			}
		}

		if (flag)
			break;	//如果一次交换都没有出现，说明数组有序，直接结束
	}
}

//快排优化方案
//优化一、三数取中
void GetMid(int*pa, int begin, int end)
{
	assert(pa);

	int mid = (begin + end) / 2;	//之前的三数取中，mid 取的是中间位
	//int mid = begin + rand() % (end - begin);	//mid 取随机位置
	int midVali = begin;	//假设最左值为中值
	if (pa[midVali] > pa[mid])
	{
		//1.begin > mid > end
		if (pa[mid] > pa[end])
			midVali = mid;

		//2.end > begin > mid
		else if (pa[end] > pa[midVali])
			midVali = begin;

		//3.end = begin > mid
		else
			midVali = end;
	}
	else
	{
		//1.mid > begin > end
		if (pa[end] < pa[midVali])
			midVali = begin;

		//2.end > mid > begin
		else if (pa[mid] < pa[end])
			midVali = mid;

		else
			midVali = end;
	}

	swap(&pa[begin], &pa[midVali]);
}

//优化二、小区间优化
//当待排数据范围较小时，采用直接插入排序，效率会更高


//优化三、三路划分
//将与key相同的值，分到中间，避免过多key而导致的性能下降
//FV的意思是完全版本
void QuicSortFV(int* pa, int begin, int end)
{
	assert(pa);

	if (begin >= end)
		return;

	if ((end - begin + 1) < 20)
		InsertSort(pa + begin, end - begin + 1);
	else
	{
		GetMid(pa, begin, end);

		int key = pa[begin];
		int lefti = begin;
		int righti = end;
		int curi = begin + 1;

		while (curi <= righti)
		{
			if (pa[curi] > key)
				swap(&pa[curi], &pa[righti--]);	//扩大右路
			else if (pa[curi] < key)
				swap(&pa[curi++], &pa[lefti++]);	//扩大左路
			else
				curi++;	//此时等于key，扩大中路的范围就行了
		}

		QuicSortFV(pa, begin, lefti - 1);
		QuicSortFV(pa, righti + 1, end);
	}
}

//霍尔版
int PartSort1(int* pa, int begin, int end)
{
	assert(pa);

	GetMid(pa, begin, end);

	//选 key 在左边，右边先走
	int keyi = begin;
	int lefti = begin;
	int righti = end;
	while (lefti < righti)
	{
		while (lefti < righti && pa[righti] > pa[keyi])
			righti--;
		while (lefti < righti && pa[lefti] <= pa[keyi])
			lefti++;

		swap(&pa[lefti], &pa[righti]);
	}

	swap(&pa[keyi], &pa[lefti]);
	keyi = lefti;

	return keyi;
}

//挖坑法
int PartSort2(int* pa, int begin, int end)
{
	assert(pa);

	GetMid(pa, begin, end);

	//挖坑，先在key处挖坑，右边先走，找到小于等于key的，填入坑中，此处形成新坑
	int key = pa[begin];
	int lefti = begin;
	int righti = end;
	int holei = lefti;	//坑位
	while (lefti < righti)
	{
		while (lefti < righti && pa[righti] > key)
			righti--;
		pa[holei] = pa[righti];	//将当前值填入坑中
		holei = righti;	//挖新坑

		while (lefti < righti && pa[lefti] <= key)
			lefti++;
		pa[holei] = pa[lefti];
		holei = lefti;
	}

	pa[holei] = key;

	return holei;
}

//双指针法
int PartSort3(int* pa, int begin, int end)
{
	assert(pa);

	GetMid(pa, begin, end);	//三数取中，后面会提

	//思想：cur找比key小的，++prev后，交换
	int* pKey = pa + begin;
	int* prev = pKey;
	int* cur = prev + 1;
	int* pend = pa + end;
	while (cur <= pend)
	{
		if (*cur <= *pKey)
			swap(++prev, cur);

		cur++;
	}

	swap(prev, pKey);

	return prev - pa;
}

//快速排序
void QuickSort(int* pa, int begin, int end)
{
	assert(pa);

	//思路：选出key，key 的右边小于key，key 的左边大于key
	if (begin >= end)
		return;

	if ((end - begin + 1) < 20)
		InsertSort(pa + begin, (end - begin + 1));	//这就是小区间优化
	else
	{

		//int keyi = PartSort1(pa, begin, end);	//霍尔法
		//int keyi = PartSort2(pa, begin, end);	//挖坑法
		int keyi = PartSort3(pa, begin, end);	//双指针法

		//[begin, keyi - 1] keyi [keyi + 1, end]
		QuickSort(pa, begin, keyi - 1);
		QuickSort(pa, keyi + 1, end);
	}
}

#include"Stack.h"

//快排，迭代版
void QuickSortNonR(int* pa, int begin, int end)
{
	assert(pa);

	//思路：利用栈的特性，先排序大范围，再排序小范围
	Stack s;
	StackInit(&s);
	StackPush(&s, begin);	//先将最开始的区间入栈
	StackPush(&s, end);
	while (!StackEmpty(&s))
	{
		int righti = StackTop(&s);	//先取的是右，再取左
		StackPop(&s);
		int lefti = StackTop(&s);
		StackPop(&s);

		int keyi = (lefti + righti) / 2;

		//小区间优化
		if ((righti - lefti + 1) < 20)
			InsertSort(pa + lefti, righti - lefti + 1);
		else
			keyi = PartSort3(pa, lefti, righti);	//排序

		//keyi = PartSort3(pa, lefti, righti);	//排序

		//判断是否符合条件入栈
		if ((keyi + 1) < righti)
		{
			StackPush(&s, (keyi + 1));
			StackPush(&s, righti);	//这里入的是右，与前面呼应
		}

		if ((keyi - 1) > lefti)
		{
			StackPush(&s, lefti);
			StackPush(&s, (keyi - 1));	////这里入的是右，与前面呼应
		}
	}

	StackDestroy(&s);
}

//归并排序
void _MergeSort(int* pa, int begin, int end, int* tmp)
{
	assert(pa && tmp);


	//思路：令数组左右两边有序，然后合并两个有序数组
	if (begin >= end)
		return;

	int mid = (begin + end) / 2;	//分成左右两个区间进行递出

	_MergeSort(pa, begin, mid, tmp);	//递归左半区间
	_MergeSort(pa, mid + 1, end, tmp);	//递归右半区间

	//下面是合并有序数组部分

	int left1 = begin;	//左半区间左边界
	int right1 = mid;	//左半区间右边界

	int left2 = mid + 1;	//右半区间左边界
	int right2 = end;	//右半区间右边界

	int pos = begin;	//这是额外空间的下标，会随着递归层度而变化
	while (left1 <= right1 && left2 <= right2)
	{
		//升序，取小的放前面
		if (pa[left1] <= pa[left2])
			tmp[pos++] = pa[left1++];
		else
			tmp[pos++] = pa[left2++];
	}

	//确保合并完成
	while (left1 <= right1)
		tmp[pos++] = pa[left1++];
	while (left2 <= right2)
		tmp[pos++] = pa[left2++];

	//将额外空间中的数据拷贝回原数组中
	memcpy(pa + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

void MergeSort(int* pa, int n)
{
	assert(pa);

	int* tmp = (int*)malloc(sizeof(int) * n);	//额外辅助空间
	assert(tmp);

	_MergeSort(pa, 0, n - 1, tmp);	//归并主程序

	free(tmp);	//记得释放
	tmp = NULL;
}

//归并，迭代版
void MergeSortNonR(int* pa, int n)
{
	assert(pa);

	//思路：通过一个变量来控制归并范围，范围从1开始，到n-1结束
	int* tmp = (int*)malloc(sizeof(int) * n);
	assert(tmp);

	int rangeN = 1;	//范围从1开始
	while (rangeN < n)
	{
		for (int i = 0; i < n; i += rangeN * 2)
		{
			//第一组
			int begin1 = i;
			int end1 = i + rangeN - 1;

			//第二组
			int begin2 = i + rangeN;
			int end2 = i + rangeN * 2 + 1;

			//迭代版需要考虑边界问题，不能越界
			if (end1 >= n)
				break;	//左半区间的右边界越界，直接跳出（只有一个数组，也没有合并的必要）
			else if (begin2 >= n)
				break;	//右半区间的左边界越界，也是直接跳出（右半区间非法）
			else if (end2 >= n)
				end2 = n - 1;	//右半区间的右边界越界，将其矫正至 n - 1 处，不能跳出，否则会有数据遗漏

			//因为是迭代，需要面面俱到
			int pos = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (pa[begin1] <= pa[begin2])
					tmp[pos++] = pa[begin1++];
				else
					tmp[pos++] = pa[begin2++];
			}

			//确保数据合并完成
			while (begin1 <= end1)
				tmp[pos++] = pa[begin1++];
			while (begin2 <= end2)
				tmp[pos++] = pa[begin2++];

			memcpy(pa + i, tmp + i, sizeof(int) * (end2 - i + 1));	//一块一块的拷贝
		}

		rangeN *= 2;
	}

	free(tmp);
	tmp = NULL;
}
//
////优化方案二，修正范围
//void MergeSortNonR(int* pa, int n)	//归并排序_迭代版
//{
//	assert(pa);
//
//	//需要开辟一块同样大的空间进行归并
//	int* tmp = (int*)malloc(sizeof(int) * n);
//	if (!tmp)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	//迭代思想：通过一个变量 rangeN 来控制合并的数据数
//	int rangeN = 1;	//从1开始
//
//	while (rangeN < n)
//	{
//		for (int i = 0; i < n; i += rangeN * 2)
//		{
//			int begin1 = i;
//			int end1 = i + rangeN - 1;
//			int begin2 = i + rangeN;
//			int end2 = i + rangeN * 2 - 1;
//
//			修正范围不会跳出循环
//			//
//			if (end1 >= n)
//			{
//				end1 = n - 1;
//				begin2 = n;
//				end2 = n - 1;
//			}
//			else if (begin2 >= n)
//			{
//				begin2 = n;
//				end2 = n - 1;
//			}
//			else if (end2 >= n)
//				end2 = n - 1;
//
//			int j = i;
//			//合并
//			while (begin1 <= end1 && begin2 <= end2)
//			{
//				if (pa[begin1] < pa[begin2])
//					tmp[j++] = pa[begin1++];
//				else
//					tmp[j++] = pa[begin2++];
//			}
//
//			//确认没有遗漏的值
//			while (begin1 <= end1)
//				tmp[j++] = pa[begin1++];
//			while (begin2 <= end2)
//				tmp[j++] = pa[begin2++];
//		}
//
//		//将数据拷贝到原数组中，整体拷贝
//		memcpy(pa, tmp, sizeof(int) * n);
//
//		rangeN *= 2;
//	}
//
//	free(tmp);
//	tmp = NULL;
//}

//计数排序
void CountSort(int* pa, int n)
{
	assert(pa);

	//思路：映射，将所有数映射到一片空间中，依次拷贝即可
	int max = pa[0];
	int min = pa[0];

	for (int i = 1; i < n; i++)
	{
		if (pa[i] > max)
			max = pa[i];
		if (pa[i] < min)
			min = pa[i];
	}

	//相对映射
	int* mapSpace = (int*)malloc(sizeof(int) * (max - min + 1));
	assert(mapSpace);
	memset(mapSpace, 0, sizeof(int) * (max - min + 1));	//初始化为0

	for (int i = 0; i < n; i++)
		mapSpace[pa[i] - min]++;

	int j = 0;
	for(int i = 0 ; i < (max - min + 1);i++)
	{
		while (mapSpace[i]--)
			pa[j++] = i + min;	//拷贝至原数组
	}

	free(mapSpace);
	mapSpace = NULL;
}

//打印
void SortPrint(int* pa, int n)
{
	assert(pa);

	for (int i = 0; i < n; i++)
		printf("%d ", pa[i]);

	printf("\n");
}