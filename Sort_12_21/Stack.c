#define _CRT_SECURE_NO_WARNINGS 1	
#include"Stack.h"

void StackInit(Stack* ps)	//栈的初始化
{
	assert(ps);	//此处断言都是防止空指针解引用，后续不再解释

	ps->data = NULL;
	ps->top = ps->capacity = 0;	//默认栈空时，栈顶为0
	//因此栈顶元素等于栈顶-1
}

void StackDestroy(Stack* ps)	//栈的销毁
{
	assert(ps);


	//只有为栈开辟空间了，才能正常销毁
	if (ps->data)
	{
		free(ps->data);
		ps->data = NULL;
		ps->top = ps->capacity = 0;
	}
}

//void StackPrint(Stack* ps)	//打印栈_测试用
//{
//	assert(ps);
//
//	//可以根据栈顶进行循环打印
//	int i = ps->top - 1;
//	for (; i >= 0; i--)
//	{
//		printf("%d ", ps->data[i]);
//	}
//	printf("\n");
//}

void StackPrint(Stack* ps)	//打印栈_用户用
{
	assert(ps);

	//根据栈的特性，查看所有元素是要付出代价的
	//即元素被Pop
	while (!StackEmpty(ps))
	{
		printf("%d ", StackTop(ps));
		StackPop(ps);
	}
	printf("\n");
}

void StackPush(Stack* ps, STDataType x)	//入栈
{
	assert(ps);

	//判断栈是否满
	if (ps->top == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;	//二倍扩容
		STDataType* tmp = (STDataType*)realloc(ps->data, sizeof(STDataType) * newCapacity);
		//如果扩容失败，需要报错并结束程序
		if (tmp == NULL)
		{
			perror("realloc :: fail");
			exit(-1);
		}

		ps->data = tmp;
		ps->capacity = newCapacity;
	}

	//入栈，就是在栈顶处放入元素，然后栈顶+1
	ps->data[ps->top] = x;
	ps->top++;
}

void StackPop(Stack* ps)	//出栈
{
	assert(ps);
	assert(ps->top > 0);	//有元素才能出栈

	//出栈，直接栈顶-1
	ps->top--;
}

STDataType StackTop(Stack* ps)	//查看栈顶元素
{
	assert(ps);
	assert(ps->top > 0);	//有元素才能看

	//栈顶元素，在栈顶-1处，因为栈顶是从0开始的
	return ps->data[ps->top - 1];
}

int StackSize(Stack* ps)	//查看栈的有效元素个数
{
	assert(ps);

	//栈的大小就是当前栈的有效元素个数
	return ps->top;
}

bool StackEmpty(Stack* ps)	//判断栈是否为空
{
	assert(ps);

	//栈顶为0.说明栈空
	return ps->top == 0;
}