#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<stdbool.h>
#include<time.h>

//直接插入排序
void InsertSort(int* pa, int n);

//希尔排序
void ShellSort(int* pa, int n);

//简单选择排序
void SelectSort(int* pa, int n);

//堆排序
void HeapSort(int* pa, int n);

//冒泡排序
void BubbleSort(int* pa, int n);

//快速排序
void QuickSort(int* pa, int begin, int end);
//快排，迭代版
void QuickSortNonR(int* pa, int begin, int end);
//快排，完全优化版（三数取中+小区间优化+三路划分）
void QuicSortFV(int* pa, int begin, int end);

//归并排序
void MergeSort(int* pa, int n);
//归并，迭代版
void MergeSortNonR(int* pa, int n);

//计数排序
void CountSort(int* pa, int n);

//打印
void SortPrint(int* pa, int n);