#define _CRT_SECURE_NO_WARNINGS 1	
#include"Sort.h"

void SortPrint(int* pa, int n)
{
	assert(pa);

	int i = 0;
	for (i = 0; i < n; i++)
		printf("%d ", pa[i]);
	printf("\n");
}

void InsertSort(int* pa, int n)	//直接插入排序
{
	assert(pa);

	//可以从第二个数据开始排序
	for (int i = 1; i < n; i++)
	{
		int end = i;
		int tmp = pa[end];	//临时存储待插入的值
		while (end)
		{
			if (tmp < pa[end - 1])
			{
				pa[end] = pa[end - 1];
				end--;	//此时需要将数据往后移动
			}
			else
				break;	//此时的 tmp 已经找到属于自己的位置了
		}
		pa[end] = tmp;	//插入数据
	}
}

//从后往前找版
void ShellSort(int* pa, int n)	//希尔排序
{
	assert(pa);
	int gap = n;	//默认gap为n
	while (gap > 1)
	{
		gap /= 2;	//要确保至少出现一次直接插入排序
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = pa[end + gap];
			while (end >= 0)
			{
				if (tmp < pa[end])
				{
					pa[end + gap] = pa[end];
					end -= gap;
				}
				else
					break;
			}
			pa[end + gap] = tmp;
		}
	}
}

////从前往后找版
//void ShellSort(int* pa, int n)	//希尔排序
//{
//	int gap = n;
//	while (gap > 1)
//	{
//		gap /= 2;
//		for (int i = gap; i < n; i++)
//		{
//			int end = i;
//			int tmp = pa[end - gap];
//			while (end <= n - 1)
//			{
//				if (tmp > pa[end])
//				{
//					pa[end - gap] = pa[end];
//					end += gap;
//				}
//				else
//					break;
//			}
//			pa[end - gap] = tmp;
//		}
//	}
//}

void Swap(int* n1, int* n2)
{
	int tmp = *n1;
	*n1 = *n2;
	*n2 = tmp;
}

////只选出最大的数往后放（升序）
//void SelectSort(int* pa, int n)	//直接选择排序
//{
//	assert(pa);
//
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		int j = 0;
//		int max = pa[j];
//		for (j = 1; j < n - i; j++)
//		{
//			if (pa[j] > max)
//			{
//				Swap(&max, &pa[j]);
//			}
//			pa[j - 1] = pa[j];
//		}
//		pa[n - i - 1] = max;
//	}
//}

//进阶版，同时选择最大和最小的数进行交换
void SelectSort(int* pa, int n)	//直接选择排序
{
	assert(pa);

	int begin = 0;
	int end = n - 1;	//确定首位位置
	while (begin < end)
	{
		int i = begin;
		int maxi = i;
		int mini = i;
		for (i += 1; i <= end; i++)
		{
			if (pa[maxi] < pa[i])
				maxi = i;
			if (pa[mini] > pa[i])
				mini = i;
		}
		if (mini == end)
			mini = maxi;	//这个条件的设置和后面的交换顺序有关，需要考虑
		Swap(&pa[maxi], &pa[end]);
		Swap(&pa[mini], &pa[begin]);
		begin++;
		end--;
	}
}

void AdjustDown(int* pa, int len, int parent)
{
	assert(pa);

	//向下调整，找到大孩子，判断是否需要更新父亲
	int child = parent * 2 + 1;	//假设左孩子为大孩子
	while (child < len)
	{
		if (child + 1 < len && pa[child + 1] > pa[child])
		{
			//条件符合，就更新大孩子为右孩子
			child += 1;
		}

		//如果孩子比父亲大，就交换
		if (pa[child] > pa[parent])
		{
			Swap(&pa[parent], &pa[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}

void HeapSort(int* pa, int n)	//堆排序
{
	assert(pa);

	//堆排序，需要堆，先写一个向下调整函数，建堆
	int parent = (n - 2) / 2;
	while (parent >= 0)
	{
		AdjustDown(pa, n, parent);
		parent--;
	}

	//判断当前元素是否大于堆顶元素，大于就入堆
	int i = n - 1;	//准备交换
	while (i > 0)
	{
		Swap(&pa[0], &pa[i]);
		AdjustDown(pa, i, 0);
		i--;
	}
}

//冒泡“优化”版
void BubbleSort(int* pa, int n)	//冒泡排序
{
	assert(pa);

	int i = 0;
	for (i = 0; i < n; i++)
	{
		int j = 1;
		int flag = 1;
		for (; j < n - i; j++)
		{
			if (pa[j] < pa[j - 1])
			{
				Swap(&pa[j], &pa[j - 1]);
				flag = 0;
			}
		}
		if (flag)
			break;
		//SortPrint(pa, n);
	}
}


//三数取中
int GetMid(int* pa, int begin, int end)
{
	//int mid = (begin + end) / 2;
	//随机取 key
	int mid = begin + rand() % (end - begin);	//取得随机数
	if (pa[begin] < pa[mid])
	{
		if (pa[mid] < pa[end])
			return mid;
		else if (pa[end] < pa[begin])
			return begin;
		else
			return end;
	}
	else
	{
		if (pa[mid] > pa[end])
			return mid;
		else if (pa[end] > pa[begin])
			return begin;
		else
			return end;
	}
}

//Hoare版
int PartSort1(int* pa, int begin, int end)
{
	//优化1，三数取中
	int mid = GetMid(pa, begin, end);
	Swap(&pa[begin], &pa[mid]);

	int keyi = begin;
	int lefti = keyi + 1;
	int righti = end;
	while (lefti < righti)
	{
		//小于 | key | 大于
		while (lefti < righti && pa[righti] >= pa[keyi])
			righti--;
		while (lefti < righti && pa[lefti] <= pa[keyi])
			lefti++;	//此时要找比 key 小的值

		//交换左右两处的值
		Swap(&pa[lefti], &pa[righti]);
	}
	Swap(&pa[lefti], &pa[keyi]);	//此时用左右都行，因为他们相遇了

	return lefti;
}

//挖坑法
int  PartSort2(int* pa, int begin, int end)
{
	//优化1，三数取中
	int mid = GetMid(pa, begin, end);
	Swap(&pa[begin], &pa[mid]);

	int key = pa[begin];
	int lefti = begin;
	int righti = end;
	int hole = begin;	//坑
	while (lefti < righti)
	{
		//一样的右边先走
		while (lefti < righti && pa[righti] >= key)
			righti--;
		pa[hole] = pa[righti];
		hole = righti;

		while (lefti < righti && pa[lefti] <= key)
			lefti++;
		pa[hole] = pa[lefti];
		hole = lefti;
	}
	pa[hole] = key;

	return hole;
}

//双指针
//int PartSort3(int* pa, int begin, int end)
//{
//	assert(a);
//
//	int* prev = a + 1;
//	int* cur = prev + 1;
//	int* pEnd = a + end;
//	while (cur <= pEnd)
//	{
//		if (*cur < a[begin] && ++prev < cur)
//			Swap(prev, cur);
//
//		cur++;
//	}
//
//	Swap(&a[begin], prev);
//	return prev - a;
//}

//双指针法
int PartSort3(int* pa, int begin, int end)
{
	//优化1，三数取中
	int mid = GetMid(pa, begin, end);
	Swap(&pa[begin], &pa[mid]);

	int* pKey = pa + begin;	//注意+begin，因为不是每次都是从0开始
	int* prev = pKey + 1;
	int* cur = prev + 1;
	int* pEnd = pa + end;
	while (cur <= pEnd)
	{
		if (*cur < *pKey)
			Swap(++prev, cur);
		cur++;
	}

	Swap(pKey, prev);

	return prev - pa;
}

//三路划分 + 随机key值法
//思路：通过 key 将数据划分为三块，大大提高多 key 值时的性能
void QuickSortByTWD(int* pa, int begin, int end)
{
	assert(pa);

	//此时数据已经全部分割排序完了
	if (begin >= end)
		return;


	//优化2，小区间优化
	if ((end - begin + 1) < 15)
		InsertSort(pa + begin, end - begin + 1);
	else
	{
		int mid = GetMid(pa, begin, end);
		Swap(&pa[begin], &pa[mid]);

		int key = pa[begin];
		int left = begin;
		int cur = left + 1;
		int right = end;

		//1.如果 cur < key ，将 cur 值交换至 key(left) 处，然后两者++
		//2.如果 cur = key ，不执行交换，cur++
		//3.如果 cur > key，将 cur 值交换至 right 处，right--
		//[begin, left - 1] [left, right] [right + 1, end]
		while (cur <= right)
		{
			if (pa[cur] < key)
			{
				Swap(&pa[cur], &pa[left]);
				cur++;
				left++;
			}

			if (pa[cur] == key)
			{
				cur++;
			}

			if (pa[cur] > key)
			{
				Swap(&pa[cur], &pa[right]);
				right--;
			}
		}

		QuickSortByTWD(pa, begin, left - 1);
		QuickSortByTWD(pa, right + 1, end);
	}
}

void HoareQuickSort(int* pa, int begin, int end)	//Hoare版的快速排序
{
	assert(pa);

	//此时数据已经全部分割排序完了
	if (begin >= end)
		return;


	//优化2，小区间优化
	if ((end - begin + 1) < 15)
		InsertSort(pa + begin, end - begin + 1);
	else
	{
		//int keyi = PartSort1(pa, begin, end);
		//int keyi = PartSort2(pa, begin, end);
		int keyi = PartSort3(pa, begin, end);

		//keyi 左右的两部分，需要递归解决
		HoareQuickSort(pa, begin, keyi - 1);
		HoareQuickSort(pa, keyi + 1, end);
	}
}

#include"Stack.h"

void QuickSortNonR(int* pa, int begin, int end)	//非递归版的快速排序
{
	assert(pa);

	//思路：借助栈，把将区间入栈，然后出栈，根据区间进行排序
	Stack S;
	StackInit(&S);

	//先把基础区间入栈
	StackPush(&S, begin);
	StackPush(&S, end);
	while (!StackEmpty(&S))
	{
		int right = StackTop(&S);
		StackPop(&S);
		int left = StackTop(&S);
		StackPop(&S);

		int keyi = PartSort1(pa, left, right);	//返回的值。就是区间中点值

		if (right > (keyi + 1))
		{
			StackPush(&S, keyi + 1);
			StackPush(&S, right);
		}

		if ((keyi - 1) > left)
		{
			StackPush(&S, left);
			StackPush(&S, keyi - 1);
		}
	}

	StackDestroy(&S);
}

void _MergeSort(int* pa, int begin, int end, int* tmp)	//归并主程序
{
	assert(pa && tmp);

	if (begin >= end)
		return;

	//目标两边经过递归后，变得有序
	//核心：后序遍历

	//[begin, mid] [mid+1, end]
	int mid = (begin + end) / 2;

	_MergeSort(pa, begin, mid, tmp);
	_MergeSort(pa, mid + 1, end, tmp);

	int begin1 = begin;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = end;
	int i = begin;

	//合并
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (pa[begin1] < pa[begin2])
			tmp[i++] = pa[begin1++];
		else
			tmp[i++] = pa[begin2++];
	}

	//确认没有遗漏的值
	while (begin1 <= end1)
		tmp[i++] = pa[begin1++];
	while (begin2 <= end2)
		tmp[i++] = pa[begin2++];

	//将数据拷贝到原数组中
	memcpy(pa + begin, tmp + begin, sizeof(int)*(end - begin + 1));
}

void MergeSort(int* pa, int n)	//归并排序
{
	assert(pa);

	//需要开辟一块同样大的空间进行归并
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (!tmp)
	{
		perror("malloc fail");
		exit(-1);
	}

	_MergeSort(pa, 0, n - 1, tmp);	//归并主程序

	free(tmp);
	tmp = NULL;
}

////优化方案一，直接跳出
//void MergeSortNonR(int* pa, int n)	//归并排序_迭代版
//{
//	assert(pa);
//
//	//需要开辟一块同样大的空间进行归并
//	int* tmp = (int*)malloc(sizeof(int) * n);
//	if (!tmp)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	//迭代思想：通过一个变量 rangeN 来控制合并的数据数
//	int rangeN = 1;	//从1开始
//
//	while (rangeN < n)
//	{
//		for (int i = 0; i < n; i += rangeN * 2)
//		{
//			int begin1 = i;
//			int end1 = i + rangeN - 1;
//			int begin2 = i + rangeN;
//			int end2 = i + rangeN * 2 - 1;
//
//			if (end1 >= n)
//				break;
//			else if (begin2 >= n)
//				break;
//			else if (end2 >= n)
//				end2 = n - 1;
//
//			int j = i;
//			合并
//			while (begin1 <= end1 && begin2 <= end2)
//			{
//				if (pa[begin1] < pa[begin2])
//					tmp[j++] = pa[begin1++];
//				else
//					tmp[j++] = pa[begin2++];
//			}
//
//			//确认没有遗漏的值
//			while (begin1 <= end1)
//				tmp[j++] = pa[begin1++];
//			while (begin2 <= end2)
//				tmp[j++] = pa[begin2++];
//
//			//将数据拷贝到原数组中
//			memcpy(pa + i, tmp + i, sizeof(int) * (end2 - i + 1));
//		}
//
//		rangeN *= 2;
//	}
//
//	free(tmp);
//	tmp = NULL;
//}

//优化方案二，修正范围
void MergeSortNonR(int* pa, int n)	//归并排序_迭代版
{
	assert(pa);

	//需要开辟一块同样大的空间进行归并
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (!tmp)
	{
		perror("malloc fail");
		exit(-1);
	}

	//迭代思想：通过一个变量 rangeN 来控制合并的数据数
	int rangeN = 1;	//从1开始

	while (rangeN < n)
	{
		for (int i = 0; i < n; i += rangeN * 2)
		{
			int begin1 = i;
			int end1 = i + rangeN - 1;
			int begin2 = i + rangeN;
			int end2 = i + rangeN * 2 - 1;

			if (end1 >= n)
			{
				end1 = n - 1;
				begin2 = n;
				end2 = n - 1;
			}
			else if (begin2 >= n)
			{
				begin2 = n;
				end2 = n - 1;
			}
			else if (end2 >= n)
				end2 = n - 1;

			int j = i;
			//合并
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (pa[begin1] < pa[begin2])
					tmp[j++] = pa[begin1++];
				else
					tmp[j++] = pa[begin2++];
			}

			//确认没有遗漏的值
			while (begin1 <= end1)
				tmp[j++] = pa[begin1++];
			while (begin2 <= end2)
				tmp[j++] = pa[begin2++];
		}

		//将数据拷贝到原数组中，整体拷贝
		memcpy(pa, tmp, sizeof(int) * n);

		rangeN *= 2;
	}

	free(tmp);
	tmp = NULL;
}

void ConutSort(int* pa, int n)	//计数排序
{
	//核心思想：映射
	assert(pa);

	int max = pa[0];
	int min = pa[n - 1];
	for (int i = 0; i < n; i++)
	{
		if (pa[i] > max)
			max = pa[i];
		if (pa[i] < min)
			min = pa[i];
	}

	int range = max - min + 1;
	range < n ? range = n : 1;

	int* mapSpace = (int*)calloc(range, sizeof(int));
	assert(mapSpace);

	for (int i = 0; i < n; i++)
	{
		mapSpace[pa[i] - min]++;
	}

	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (mapSpace[i]--)
		{
			pa[j++] = i + min;
		}
	}

	free(mapSpace);
	mapSpace = NULL;
}