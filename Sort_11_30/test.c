#define _CRT_SECURE_NO_WARNINGS 1	
#include"Sort.h"

//12_4日，因回家无法正常更新代码
//12_5，隔离
//12_6，常规更新，归并排序（递归版与迭代版）

int cmp(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}

void SortOP()
{
	srand((size_t)time(NULL));
	int n = 10000000;
	int* a1 = (int*)malloc(sizeof(int) * n);
	int* a2 = (int*)malloc(sizeof(int) * n);
	int* a3 = (int*)malloc(sizeof(int) * n);
	int* a4 = (int*)malloc(sizeof(int) * n);
	int* a5 = (int*)malloc(sizeof(int) * n);
	int* a6 = (int*)malloc(sizeof(int) * n);
	int* a7 = (int*)malloc(sizeof(int) * n);
	int* a8 = (int*)malloc(sizeof(int) * n);
	assert(a1 && a2 && a3 && a4 && a5 && a6 && a7 && a8);

	FILE* pf = fopen("test.txt", "w");
	if (!pf)
	{
		perror("open fail");
		return;
	}

	int i = 0;
	for (i = 0; i < n; i++)
	{
		a1[i] = i;
		fprintf(pf, "%d ", a1[i]);
		i % 100 == 0 ? fprintf(pf, "\n") : 1;
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		a5[i] = a1[i];
		a6[i] = a1[i];
		a7[i] = a1[i];
		a8[i] = a1[i];
	}

	int begin1 = clock();
	//InsertSort(a1, n);
	int end1 = clock();

	int begin2 = clock();
	ShellSort(a2, n);
	int end2 = clock();

	int begin3 = clock();
	//SelectSort(a3, n);
	int end3 = clock();

	int begin4 = clock();
	HeapSort(a4, n);
	int end4 = clock();

	int begin5 = clock();
	//BubbleSort(a5, n);
	int end5 = clock();

	int begin6 = clock();
	//HoareQuickSort(a6, 0, n - 1);
	qsort(a6, n, sizeof(int), cmp);
	//QuickSortNonR(a6, 0, n - 1);	//非递归版
	//QuickSortByTWD(a6, 0, n - 1);	//三路划分版
	int end6 = clock();

	int begin7 = clock();
	//MergeSort(a7, n);
	MergeSortNonR(a7, n);
	int end7 = clock();
	
	int begin8 = clock();
	ConutSort(a8, n);
	int end8 = clock();

	printf("InsertSort: %d\n", end1 - begin1);
	printf("ShellSort: %d\n", end2 - begin2);
	printf("SelectSort: %d\n", end3 - begin3);
	printf("HeapSort: %d\n", end4 - begin4);
	printf("BubbleSort: %d\n", end5 - begin5);
	printf("HoareQuickSort: %d\n", end6 - begin6);
	printf("MergeSort: %d\n", end7 - begin7);
	printf("CountSort: %d\n", end8 - begin8);

	free(a1);
	free(a2);
	free(a3);
	free(a4);
	free(a5);
	free(a6);
	free(a7);

	fclose(pf);
	pf = NULL;
}

int main()
{
	////int arr[] = { 8,5,6,7,3,1,2,9 };
	//int arr[] = { 6,1,2,7,9,3,4,5, 10, 8};
	//int len = sizeof(arr) / sizeof(arr[0]);

	//SortPrint(arr, len);

	////InsertSort(arr, len);	//直接插入排序
	////ShellSort(arr, len);	//希尔排序

	////SelectSort(arr, len);	//直接选择排序
	////HeapSort(arr, len);	//堆排序

	////BubbleSort(arr, len);	//冒泡排序
	////HoareQuickSort(arr, 0, len - 1);	//Hoare版的快速排序
	////QuickSortNonR(arr, 0, len - 1);	//非递归版快排
	////MergeSort(arr, len);	//归并排序
	////MergeSortNonR(arr, len);	//归并排序_迭代版
	////QuickSortByTWD(arr, 0, len - 1);	//三路划分版快排
	//ConutSort(arr, len);	//计数排序
	//SortPrint(arr, len);

	SortOP();
	return 0;
}