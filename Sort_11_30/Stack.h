#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<windows.h>

typedef int STDataType;	//栈的元素类型

typedef struct StackInfo
{
	STDataType* data;	//数据
	int top;	//栈顶
	int capacity;	//容量
}Stack;

void StackInit(Stack* ps);	//栈的初始化
void StackDestroy(Stack* ps);	//栈的销毁

void StackPrint(Stack* ps);	//打印栈

void StackPush(Stack* ps, STDataType x);	//入栈
void StackPop(Stack* ps);	//出栈

STDataType StackTop(Stack* ps);	//查看栈顶元素
int StackSize(Stack* ps);	//查看栈的有效元素个数
bool StackEmpty(Stack* ps);	//判断栈是否为空