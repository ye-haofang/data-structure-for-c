#define _CRT_SECURE_NO_WARNINGS 1	
#include"Stack.h"

void TestStack1()
{
	Stack S;
	StackInit(&S);

	int i = 0;
	while (i++ < 10)
	{
		StackPush(&S, i);
		StackPrint(&S);
	}

	i = 0;
	while (i++ < 10)
	{
		StackPop(&S);
		StackPrint(&S);
	}

	StackDestroy(&S);
}


void TestStack2()
{
	Stack S;
	StackInit(&S);

	int i = 0;
	while (i++ < 20)
	{
		StackPush(&S, i);
		StackPrint(&S);
	}

	printf("当前栈顶元素为:%d\n", StackTop(&S));
	printf("当前栈的有效元素个数为:%zu\n", StackSize(&S));
	printf("当前栈空情况:%d\n", StackEmpty(&S));

	i = 0;
	while (i++ < 10)
	{
		StackPop(&S);
		StackPrint(&S);
	}

	printf("当前栈顶元素为:%d\n", StackTop(&S));
	printf("当前栈的有效元素个数为:%zu\n", StackSize(&S));
	printf("当前栈空情况:%d\n", StackEmpty(&S));

	StackDestroy(&S);
}

void menu()
{
	printf("****************************\n");
	printf("***	0.退出	1.打印	 ***\n");
	printf("***	2.入栈	3.出栈	 ***\n");
	printf("***	4.栈顶	5.判空	 ***\n");
	printf("***	6.查看有效元素数 ***\n");
	printf("****************************\n");
}

int main()
{
	//TestStack2();
	Stack S;
	StackInit(&S);
	int input = 1;
	int val = 0;	//待入栈值
	while (input)
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case 0:
			StackDestroy(&S);
			break;
		case 1:
			printf("注意：打印栈需要弹出栈内所有元素\n是否确认(1/0)?");
			scanf("%d", &val);
			val == 0 ? printf("取消成功\n") : StackPrint(&S);
			break;
		case 2:
			val = 0;
			printf("请输入想入栈的元素:>");
			scanf("%d", &val);
			StackPush(&S, val);
			break;
		case 3:
			StackPop(&S);
			break;
		case 4:
			printf("当前栈顶元素为:%d\n", StackTop(&S));
			break;
		case 5:
			printf("当前栈空情况:%d\n",StackEmpty(&S));
			break;
		case 6:
			printf("当前栈内的有效元素为:%d\n", StackSize(&S));
			break;
		}
	}
	return 0;
}