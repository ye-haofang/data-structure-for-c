#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<string.h>
#include<time.h>

////strchr 与 clock
//int main()
//{
//	size_t begin = clock();
//	char* str = "aashiuhaibdsacic";
//	char* ch = strchr(str, 'b');
//	int i = 0;
//	for (; i < 100000000; i++)
//	{
//		;
//	}
//	size_t end = clock();
//	printf("运行时间是%zu毫秒,%s\n", end - begin, ch);
//	return 0;
//}

//////轮转数组
//void swap(int* nums, int left, int right)
//{
//    while (left < right)
//    {
//        int tmp = nums[left];
//        nums[left] = nums[right];
//        nums[right] = tmp;
//        left++;
//        right--;
//    }
//}
//void rotate(int* nums, int numsSize, int k)
//{
//    k %= numsSize;  //去除多余的次数
//    swap(nums, 0, numsSize - 1);   //整体翻转
//    swap(nums, 0, k - 1);   //左翻转
//    swap(nums, k, numsSize - 1);    //右翻转
//}

////189.轮转数组
////运行失败
//void rotate(int* nums, int numsSize, int k) {
//    if (0 == k)
//        return;
//    k %= numsSize;  //去除多余步骤
//    int ret = k - 1;
//    int arr[3] = { 0 };
//    int i = numsSize - 1;
//    int j = i;
//    while (ret >= 0)
//    {
//        arr[ret] = nums[i];
//        ret--;
//        i--;
//    }
//    ret = k - 1;
//    int z = numsSize - k;
//    for (; j >= 0; j--)
//    {
//        if (z)
//        {
//            nums[j] = nums[i];
//            z--;
//            i--;
//        }
//        else
//        {
//            nums[j] = arr[ret];
//            ret--;
//        }
//    }
//}
//int main()
//{
//    int arr[7] = { 1 };
//    rotate(arr, 1, 0);
//    int i = 0;
//    for (; i < 1; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}

////17.04 消失的数字，法一，相加相减法
//int missingNumber(int* nums, int numsSize)
//{
//    int n = numsSize;
//    int sum = n * (n + 1) / 2;
//    int tmp = 0;
//    int i = 0;
//    for (; i < n; i++)
//        tmp += nums[i];
//    return sum - tmp;
//}

////17.04 消失的数字，法二，异或求值法
//int missingNumber(int* nums, int numsSize)
//{
//    int i = 0;
//    int tmp = 0;
//    for (; i < numsSize; i++)
//        tmp ^= nums[i];
//
//    i = 0;
//    for (; i <= numsSize; i++)
//        tmp ^= i;
//
//    return tmp;
//}

//// 计算Func1的时间复杂度？
//void Func1(int N)
//{
//	int count = 0;
//	for (int k = 0; k < 2 * N; ++k)
//	{
//		++count;
//	}
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//}
////2*N + 10
////O(N)

//// 计算Func1的时间复杂度？
//void Func1(int N, int M)
//{
//    int count = 0;
//    for (int k = 0; k < M; ++k)
//    {
//        ++count;
//    }
//    for (int k = 0; k < N; ++k)
//    {
//        ++count;
//    }
//    printf("%d\n", count);
//}
//N + M
//O(N + M)

//// 计算Func2的时间复杂度？
//void Func2(int N)
//{
//    int count = 0;
//    for (int k = 0; k < 100; ++k)
//    {
//        ++count;
//    }
//    printf("%d\n", count);
//}
//100
//O(1)

////计算strchr的时间复杂度？
////Func4
//const char* strchr(const char* str, int character);
////good_1   bad_N   mid_N/2
////O(N)

////计算BubbleSort的时间复杂度？
////Func5
//void BubbleSort(int* a, int n)
//{
//    assert(a);
//    for (size_t end = n; end > 0; --end)
//    {
//        int exchange = 0;
//        for (size_t i = 1; i < end; ++i)
//        {
//            if (a[i - 1] > a[i])
//            {
//                Swap(&a[i - 1], &a[i]);
//                exchange = 1;
//            }
//        }
//        if (exchange == 0)
//            break;
//    }
//}
////N*(N - 1) / 2
////O(N^2)

////计算BinarySearch的时间复杂度？
//int BinarySearch(int* a, int n, int x)
//{
//    assert(a);
//    int begin = 0;
//    int end = n - 1;
//    // [begin, end]：begin和end是左闭右闭区间，因此有=号
//    while (begin <= end)
//    {
//        int mid = begin + ((end - begin) >> 1);
//        if (a[mid] < x)
//            begin = mid + 1;
//        else if (a[mid] > x)
//            end = mid - 1;
//        else
//            return mid;
//    }
//    return -1;
//}
//N = 2 ^ k   k = logN
//O(logN)

////计算阶乘递归Fac的时间复杂度？
////Func7
//long long Fac(size_t N)
//{
//    if (0 == N)
//        return 1;
//    return Fac(N - 1) * N;
//}
////N
////O(N)

////计算斐波那契递归Fib的时间复杂度？
//long long Fib(size_t N)
//{
//    if (N < 3)
//        return 1;
//    return Fib(N - 1) + Fib(N - 2);
//}
//1 + 2 + 4 + 8 + 16 + …… + 2^N
//O(2^N)

//空间复杂度

////计算BubbleSort的空间复杂度？
////Func1
//void BubbleSort(int* a, int n)
//{
//    for (size_t end = n; end > 0; --end)
//    {
//        int exchange = 0;
//        for (size_t i = 1; i < end; ++i)
//        {
//            if (a[i - 1] > a[i])
//            {
//                Swap(&a[i - 1], &a[i]);
//                exchange = 1;
//            }
//        }
//        if (exchange == 0)
//            break;
//    }
//}
////end、exchange、i，共三个变量
////O(1)

//计算Fibonacci的空间复杂度？
//返回斐波那契数列的前n项
//Func2
//long long* Fibonacci(size_t n)
//{
//    if (n == 0)
//        return NULL;
//    long long* fibArray = (long long*)malloc((n + 1) * sizeof(long long));
//    fibArray[0] = 0;
//    fibArray[1] = 1;
//    for (int i = 2; i <= n; ++i)
//    {
//        fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
//    }
//    return fibArray;
//}
////fibArray、(n+1)*sizeof(long long)、i
////O(N)

//计算阶乘递归Fac的空间复杂度？
long long Fac(size_t N)
{
    if (N == 0)
        return 1;
    return Fac(N - 1) * N;
}
////开辟了N次栈帧
////O(N)

////请问这段代码的时间复杂度是多少？
//int main()
//{
//    int N = 0;
//    scanf("%d", &N);
//    int count = 0;
//    int i = 0;
//    for (i = 0; i < N; i++)
//    {
//        count++;
//    }
//    return 0;
//}

//void* memsot(void* ptr, int value, size_t num)
//{
//	char* ptrs = (char*)ptr;
//	size_t i = 0;
//	while (i < num)
//	{
//		ptrs[i] = value;
//		i++;
//	}
//	return ptr;
//}
//int main()
//{
//	char arr[10] = "abcdefg";
//	printf("%s\n", (char*)memsot(arr, '&', 3));
//	return 0;
//}

////空间复杂度
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = 3;
//	printf("%d\n", a + b + c);
//	return 0;
//}