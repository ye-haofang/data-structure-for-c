#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

////LCP 50. 宝石补给
//#include<stdlib.h>
//int cmp(const void* e1, const void* e2)
//{
//    return (*(int*)e1 - *(int*)e2);
//}
//
//int giveGem(int* gem, int gemSize, int** operations, int operationsSize, int* operationsColSize)
//{
//    int times = 0;  //需要交换的次数
//    while (times < operationsSize)
//    {
//        int pos1 = operations[times][0];
//        int pos2 = operations[times][1];
//
//        gem[pos2] += (gem[pos1] / 2); //宝石给勇士2
//        gem[pos1] -= (gem[pos1] / 2); //勇士1会失去对应的宝石数
//
//        times++;
//    }
//
//    qsort(gem, gemSize, sizeof(int), cmp);
//    return gem[gemSize - 1] - gem[0];
//}
///*
//[3,1,2], operations = [[0,2],[2,1],[2,0]]
//*/
//int main()
//{
//    int arr1[] = { 3,1,2 };
//    int arr2[] = { 0,2,2,1,2,0 };
//    giveGem(arr1, 3, &arr2, 3, arr1);
//    return 0;
//}

////Fizz Buzz
//#include<stdlib.h>
//#include<assert.h>
//#include<string.h>
//char** fizzBuzz(int n, int* returnSize) {
//    *returnSize = n; //多此一举
//    char** arr = (char**)malloc(sizeof(char*) * n);
//    assert(arr);
//
//    int i = 1;
//    for (i = 1; i <= n; i++)
//    {
//        arr[i - 1] = (char*)malloc(sizeof(char) * 9);
//        assert(arr[i - 1]);
//
//        if (i % 3 == 0 && i % 5 == 0)
//            strcpy(arr[i - 1], "FizzBuzz");
//        else if (i % 3 == 0)
//            strcpy(arr[i - 1], "Fizz");
//        else if (i % 5 == 0)
//            strcpy(arr[i - 1], "Buzz");
//        else
//            sprintf(arr[i - 1], "%d", i);
//    }
//    return arr;
//}
//int main()
//{
//    int len = 0;
//    fizzBuzz(3, &len);
//    return 0;
//}
//
////链表的中间节点
////快慢指针
//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* pfast, * pslow;
//    pfast = pslow = head;
//    while (pfast && pfast->next)
//    {
//        pfast = pfast->next->next;
//        pslow = pslow->next;
//    }
//
//    return pslow;
//}

////变成0
////移位操作符，右移->缩小两倍
//int numberOfSteps(int num) {
//    int step = 0;   //步数
//    while (num)
//    {
//        if (num % 2 == 0)
//            num >>= 1;
//        else
//            num -= 1;
//        step++; //步数+1
//    }
//
//    return step;
//}

////最富有客户的资产总量
//int maximumWealth(int** accounts, int accountsSize, int* accountsColSize) {
//    int moneyMax = 0;   //最有钱的
//    int i = 0;
//    while (i < accountsSize)
//    {
//        int tmp = 0;
//        int j = 0;
//        while (j < *accountsColSize)
//            tmp += accounts[i][j++];
//        moneyMax < tmp ? moneyMax = tmp : 0;    //三目赋值符
//        i++;
//    }
//
//    return moneyMax;
//}

////空间换时间
//#include<stdlib.h>
//#include<assert.h>
//void moveZeroes(int* nums, int numsSize) {
//    int* pa = (int*)calloc(sizeof(int), numsSize);
//    int* tmp = pa;
//    assert(tmp);
//
//    int i = 0;
//    while (i < numsSize)
//    {
//        if (nums[i] != 0)
//            *pa++ = nums[i];
//        i++;
//    }
//    i = 0;
//    while (i < numsSize)
//        nums[i++] = tmp[i];
//}

////移除元素，双指针
//int removeElement(int* nums, int numsSize, int val) {
//    int* L = nums;
//    int* R = nums + numsSize - 1;
//    int step = 0;
//    while (L < R)
//    {
//        while (L < R && *L != val)
//            L++;
//        while (L < R && *R == val)
//            R--;
//
//        if (L < R)
//        {
//            int tmp = *L;
//            *L = *R;
//            *R = tmp;
//        }
//    }
//    int i = 0;
//    while (i < numsSize)
//    {
//        if (nums[i++] != val)
//            step++;
//    }
//    return step;
//}

////去重算法
////双指针
//int removeDuplicates(int* nums, int numsSize) {
//    int* pfast, * pslow;
//    pfast = pslow = nums;
//    while (pfast < (nums + numsSize))
//    {
//        if (*pfast != *pslow)
//            *++pslow = *pfast;
//        pfast++;
//    }
//
//    return pslow - nums + 1;
//}

//去重算法2
#include<string.h>
int removeDuplicates(int* nums, int numsSize) {
    int* pfast, * pslow, * pend;
    pfast = pslow = nums;
    pend = nums + numsSize;
    while (pfast < nums + numsSize)
    {
        if (pfast - pslow == 2)
        {
            memmove(pslow + 1, pfast, sizeof(int) * (pend - pfast));
        }
        if (*pfast != *pslow)
        {
            pslow = pfast;  //开始新阶段
        }
        pfast++;
    }

    return pfast - nums + 1;
}
int main()
{
    int arr[] = { 1,1,1,2,2,3 };
    removeDuplicates(arr, 6);
    return 0;
}