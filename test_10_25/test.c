#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

////轮转数组
////思路一
////逐个轮转_不行，运行超时
//void rotate(int* nums, int numsSize, int k) {
//	k %= numsSize;
//	while (k--)
//	{
//		int tmp = nums[numsSize - 1];
//		int i = numsSize - 2;
//		for (; i >= 0; i--)
//			nums[i + 1] = nums[i];
//		nums[0] = tmp;
//	}
//}

////思路二
////以空间换时间
//#include<assert.h>
//#include<stdlib.h>
//
//void rotate(int* nums, int numsSize, int k) {
//	k %= numsSize;
//	int* pa = (int*)malloc(sizeof(int) * numsSize);
//	assert(pa);	//断言防止空指针
//
//	//取出
//	int i = 0;
//	int j = numsSize - k;
//	int ret = k;	//取出次数
//	while (ret--)
//	{
//		pa[i] = nums[j];
//		i++;
//		j++;
//	}
//
//	//存入
//	ret = numsSize - k;
//	j = 0;
//	while (ret--)
//	{
//		pa[i] = nums[j];
//		i++;
//		j++;
//	}
//
//	//覆盖
//	for (i = 0; i < numsSize; i++)
//		nums[i] = pa[i];
//
//	free(pa);	//释放
//	pa = NULL;
//}
//
//int main()
//{
//	int arr[] = { -1 };
//	rotate(arr, 1, 2);
//	return 0;
//}

////27.移除元素
////左右指针
//int removeElement(int* nums, int numsSize, int val) {
//    int left = 0;
//    int right = numsSize - 1;
//    int len = numsSize;
//    while (left < right)
//    {
//        while (nums[left] != val && left < right)
//            left++;
//        while (nums[right] == val && right > left)
//            right--;
//        if (left < right)
//        {
//            int tmp = nums[left];
//            nums[left] = nums[right];
//            nums[right] = tmp;
//        }
//    }
//    int i = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        if (nums[i] == val)
//            len--;
//    }
//    return len;
//}

#include"SeqList.h"

void TestSeqList1()
{
	SL s1;
	SLInit(&s1);

	SLPushBack(&s1, 1);
	SLPushBack(&s1, 2);
	SLPushBack(&s1, 3);
	SLPushBack(&s1, 4);
	SLPushBack(&s1, 5);

	//SLPrint(&s1);

	SLPopBack(&s1);
	SLPopBack(&s1);
	SLPopBack(&s1);
	SLPopBack(&s1);
	SLPopBack(&s1);

	SLPrint(&s1);

	SLPopBack(&s1);

	SLDestroy(&s1);
}
int main()
{
	TestSeqList1();
	return 0;
}