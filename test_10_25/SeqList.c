#define _CRT_SECURE_NO_WARNINGS 1	
#include"SeqList.h"

void SLInit(SL* ps)
{
	assert(ps);
	ps->data = NULL;
	ps->size = ps->capacity = 0;
}

void SLDestroy(SL* ps)
{
	assert(ps);
	free(ps->data);
	ps->data = NULL;
	ps->size = ps->capacity = 0;
}

void SLPrint(SL* ps)
{
	assert(ps);
	int i = 0;
	for (i = 0; i < ps->size; i++)
		printf("%d ", ps->data[i]);
	printf("\n");
}

void SLPushBack(SL* ps, SLDataType x)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		size_t newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->data, sizeof(SLDataType) * newCapacity);
		assert(tmp);
		ps->data = tmp;
		ps->capacity = newCapacity;
	}
	ps->data[ps->size++] = x;
}

void SLPopBack(SL* ps)
{
	assert(ps->size > 0);
	ps->size--;
}