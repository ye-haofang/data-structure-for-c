#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>

typedef struct ListNode {
    int val;
    struct ListNode *next;
}SLTNode;
 
extern void SLTPushBack(SLTNode** pphead, int* arr, int len);

int getDecimalValue(struct ListNode* head) {
    //先反转单链表
    struct ListNode* newHead = NULL;
    struct ListNode* tmp = head;
    struct ListNode* newTail = NULL;
    //取节点尾插
    while (tmp)
    {
        if (newHead == NULL)
        {
            newHead = newTail = tmp;
            tmp = tmp->next;
            newTail->next = NULL;
        }
        else
        {
            newHead = tmp;
            tmp = tmp->next;
            newHead->next = newTail;
            newTail = newHead;  //链接
        }

    }

    tmp = newHead;
    int num = 0;
    int weight = 0;
    while (tmp)
    {
        num += tmp->val * (int)pow(2.0, (double)weight);
        tmp = tmp->next;
        weight++;
    }

    return num;
}

int main()
{
    int arr[] = { 1,0,1 };
    SLTNode* L1 = NULL;
    SLTPushBack(&L1, arr, sizeof(arr) / sizeof(arr[0]));
    int n = getDecimalValue(L1);
    return 0;
}