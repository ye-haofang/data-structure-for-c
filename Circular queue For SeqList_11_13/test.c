#define _CRT_SECURE_NO_WARNINGS 1	
//力扣 622. 设计循环队列
//用顺序结构实现
// https://leetcode.cn/problems/design-circular-queue/


#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>
//顺序结构实现循环队列

typedef int SLDataType;

typedef struct {
    SLDataType* data;
    int head;
    int tail;
    int k;
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* Q = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    assert(Q);

    //多申请了一个空间
    SLDataType* tmp = (SLDataType*)malloc(sizeof(SLDataType) * (k + 1));
    assert(tmp);
    Q->data = tmp;
    Q->head = Q->tail = 0;
    Q->k = k;

    return Q;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj);
bool myCircularQueueIsFull(MyCircularQueue* obj);

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    assert(obj);
    //如果满了，就入不了队
    if (myCircularQueueIsFull(obj))
        return false;
    int k = obj->k;

    obj->data[obj->tail] = value;
    obj->tail = (obj->tail + 1) % (k + 1);  //回环操作

    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    assert(obj);
    //如果为空，是出不了队的
    if (myCircularQueueIsEmpty(obj))
        return false;

    int k = obj->k;
    obj->head = (obj->head + 1) % (k + 1);  //这就完成了删除(出队)
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    return obj->data[obj->head];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    int k = obj->k;
    int pos = (obj->tail + k) % (k + 1);  //copy一下
    return obj->data[pos];
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    assert(obj);
    return obj->head == obj->tail;  //两个相等，表就为空
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    assert(obj);

    int k = obj->k;
    int pos = (obj->tail + 1) % (k + 1);
    return pos == obj->head;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    assert(obj);

    free(obj->data);
    obj->data = NULL;
    obj->head = obj->tail = 0;
}


/*
["MyCircularQueue","enQueue","Rear","Rear","deQueue","enQueue","Rear","deQueue","Front","deQueue","deQueue","deQueue"]
[[6],[6],[],[],[],[5],[],[],[],[],[],[]]
*/
int main()
{
    int a = 0;
    bool b = true;
    MyCircularQueue* Q = myCircularQueueCreate(6);

    b = myCircularQueueEnQueue(Q, 6);

    a = myCircularQueueRear(Q);
    a = myCircularQueueRear(Q);

    b = myCircularQueueDeQueue(Q);

    b = myCircularQueueEnQueue(Q, 6);

    a = myCircularQueueRear(Q);

    b = myCircularQueueDeQueue(Q);

    a = myCircularQueueFront(Q);

    b = myCircularQueueDeQueue(Q);
    b = myCircularQueueDeQueue(Q);
    b = myCircularQueueDeQueue(Q);

    myCircularQueueEnQueue(Q, 4);

    a = myCircularQueueRear(Q);

    myCircularQueueFree(Q);
    return 0;
}