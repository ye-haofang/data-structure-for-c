#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

typedef int LTDataType;

typedef struct ListNode
{
	struct ListNode* prev;
	struct ListNode* next;
	LTDataType data;
}LT;

LT* LTInit(void);	//初始化
void LTDestroy(LT* plist);	//销毁
void LTPrint(LT* plist);	//打印

void LTPushBack(LT* plist, LTDataType x);	//尾插
void LTPopBack(LT* plist);	//尾删

void LTPushFront(LT* plist, LTDataType x);	//头插
void LTPopFront(LT* plist);	//头删

void LTInsert(LT* plist, LTDataType x);	//任意位置插入_前插
void LTErase(LT* plist);	//任意位置删除

LT* LTFind(LT* plist, LTDataType x);	//查找元素x

bool LTNULL(LT* plist);	//判断链表是否为空
size_t LTSize(LT* plist);	//统计链表的长度