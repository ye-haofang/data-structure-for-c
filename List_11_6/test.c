#define _CRT_SECURE_NO_WARNINGS 1	
#include"List.h"

void TestList1()
{
	LT* phead = LTInit();	//直接买一个节点当作头(哨兵位)
	LTPrint(phead);

	LTPushBack(phead, 1);
	LTPushBack(phead, 2);
	LTPushBack(phead, 3);
	LTPushBack(phead, 4);
	LTPushBack(phead, 5);
	LTPrint(phead);

	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);

	//LTPopFront(phead);	//成功报错
	//LTPrint(phead);

	LTDestroy(phead);
	phead = NULL;
}

void TestList2()
{
	LT* phead = LTInit();	//直接买一个节点当作头(哨兵位)
	LTPrint(phead);

	LTPushFront(phead, 1);
	LTPushFront(phead, 2);
	LTPushFront(phead, 3);
	LTPushFront(phead, 4);
	LTPushFront(phead, 5);
	LTPrint(phead);

	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);
	LTPopFront(phead);
	LTPrint(phead);

	//LTPopFront(phead);	//成功报错
	//LTPrint(phead);

	LTDestroy(phead);
}

void TestList3()
{
	LT* phead = LTInit();	//直接买一个节点当作头(哨兵位)
	LTPrint(phead);

	LTPushFront(phead, 1);
	LTPushFront(phead, 2);
	LTPushFront(phead, 3);
	LTPushFront(phead, 4);
	LTPushFront(phead, 5);
	LTPrint(phead);

	int i = 0;
	for (; i < 5; i++)
	{
		LT* tmp = LTFind(phead, i);
		printf("%p\n", tmp);
		if(tmp)
			tmp->data = i + 10;	//测试修改和查找
	}

	//测试判断表空
	if (LTNULL(phead))
		printf("NULL\n");
	else
		printf("NO NULL\n");

	//测试链表元素
	printf("当前链表长度为:%d\n", LTSize(phead));

	LTDestroy(phead);
}

int main()
{
	TestList3();
	return 0;
}