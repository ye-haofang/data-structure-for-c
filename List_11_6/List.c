#define _CRT_SECURE_NO_WARNINGS 1	
#include"List.h"

static LT* buyNewNode(int x)	//买新节点
{
	LT* newnode = (LT*)malloc(sizeof(LT));
	assert(newnode);	//检查是否开辟成功

	newnode->prev = NULL;
	newnode->next = NULL;
	newnode->data = x;

	return newnode;	//直接返回新节点
}
LT* LTInit(void)	//初始化
{
	LT* phead = buyNewNode(-1);	//购买一个新节点作为哨兵位
	phead->next = phead;
	phead->prev = phead;
	return phead;
}

void LTDestroy(LT* plist)	//销毁
{
	assert(plist);
	LT* cur = plist->next;
	while (cur != plist)
	{
		LT* tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	free(cur);	//释放最后的哨兵位
}

void LTPrint(LT* plist)	//打印
{
	assert(plist);
	LT* cur = plist->next;
	while (cur != plist)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}

	printf("打印结束\n");
}

void LTPushBack(LT* plist, LTDataType x)	//尾插
{
	assert(plist);

	//LT* newnode = buyNewNode(x);

	//LT* pend = plist->prev;	//找到尾节点

	//pend->next = newnode;	//改变指向
	//newnode->prev = pend;
	//newnode->next = plist;
	//plist->prev = newnode;

	LTInsert(plist, x);	//代码复用
}

void LTPopBack(LT* plist)	//尾删
{
	assert(plist);
	assert(plist->next != plist);

	//LT* pend = plist->prev;	//待删除的节点
	//LT* ptail = pend->prev;	//新的尾节点

	//free(pend);
	//pend = NULL;

	//ptail->next = plist;	//改变指向
	//plist->prev = ptail;

	LTErase(plist->prev);	//代码复用
}


void LTPushFront(LT* plist, LTDataType x)	//头插
{
	assert(plist);

	//LT* newnode = buyNewNode(x);	//买节点

	//newnode->next = plist->next;	//要先链接后面
	//(plist->next)->prev = newnode;
	//plist->next = newnode;
	//newnode->prev = plist;

	LTInsert(plist->next, x);	//代码复用
}

void LTPopFront(LT* plist)	//头删
{
	assert(plist);
	assert(plist->next != plist);

	//LT* head = plist->next;	//记录头节点
	//plist->next = head->next;	//改变链接关系
	//(head->next)->prev = plist;

	//free(head);
	//head = NULL;

	LTErase(plist->next);	//代码复用
}


void LTInsert(LT* plist, LTDataType x)	//任意位置插入_前插
{
	assert(plist);

	LT* newnode = buyNewNode(x);

	LT* left = plist->prev;	//直接找左右两个，逻辑清晰一些

	left->next = newnode;	//重新链接
	newnode->prev = left;
	newnode->next = plist;
	plist->prev = newnode;
}

void LTErase(LT* plist)	//任意位置删除
{
	assert(plist);
	assert(plist->next != plist);

	LT* left = plist->prev;
	LT* right = plist->next;

	free(plist);
	plist = NULL;

	left->next = right;	//轻松改变链接
	right->prev = left;
}

LT* LTFind(LT* plist, LTDataType x)	//查找元素x
{
	assert(plist);

	LT* left = plist->next;	//左与右
	LT* right = plist->prev;	//双向查找

	//当左与右碰面时，查找就结束了
	while (left != right)
	{
		if (left->data == x)
			return left;	//找到返回对应指针就行了
		if (right->data == x)
			return right;
		left = left->next;
		right = right->prev;
	}

	//需要再单独处理一次，防止遗漏
	if (left->data == x)
		return left;
	return NULL;
}

bool LTNULL(LT* plist)	//判断链表是否为空
{
	assert(plist);

	return plist->next == plist;	//巧妙利用判断式布尔值
}
size_t LTSize(LT* plist)	//统计链表的长度
{
	assert(plist);

	size_t count = 0;
	LT* cur = plist->next;
	while (cur != plist)
	{
		count++;
		cur = cur->next;
	}

	return count;
}