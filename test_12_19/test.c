#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//力扣刷题

//1. 两数之和
//暴力解决

int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
    *returnSize = 2;
    int* tmp = (int*)malloc(sizeof(int) * 2);
    assert(tmp);
    int i = 0;
    for (int i = 0; i < numsSize; i++)
    {
        int j = i + 1;
        for (; j < numsSize; j++)
        {
            if (nums[i] + nums[j] == target)
            {
                tmp[0] = i;
                tmp[1] = j;
                return tmp;
            }
        }
    }

    return NULL;
}


typedef struct ListNode 
{
    int val;
    struct ListNode* next;
}SLTNode;


struct ListNode* getNode(int x)
{
    struct ListNode* newnode = (struct ListNode*)malloc(sizeof(struct ListNode));
    assert(newnode);

    newnode->next = NULL;
    newnode->val = x;

    return newnode;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    //遍历相加，创建链表
    struct ListNode* gard = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* tail = (struct ListNode*)malloc(sizeof(struct ListNode));
    assert(gard && tail);

    tail = NULL;
    gard->next = tail;
    struct ListNode* tmp1 = l1;
    struct ListNode* tmp2 = l2;

    int base = 0;
    while (tmp1 && tmp2)
    {
        int num1 = tmp1->val;
        int num2 = tmp2->val;
        int ListNum = num1 + num2;
        ListNum += base;
        base = 0;

        if (ListNum > 9)
        {
            ListNum %= 10;
            base = 1;
        }

        struct ListNode* newnode = getNode(ListNum);

        if (tail == NULL)
        {
            gard->next = newnode;
            tail = newnode;
        }
        else
        {
            tail->next = newnode;
            tail = newnode;
        }

        tmp1 = tmp1->next;
        tmp2 = tmp2->next;
    }

    while (tmp1)
    {
        int num = tmp1->val;
        int ListNum = num + base;
        base = 0;

        if (ListNum > 9)
        {
            ListNum %= 10;
            base = 1;
        }

        struct ListNode* newnode = getNode(ListNum);

        if (tail == NULL)
        {
            gard->next = newnode;
            tail = newnode;
        }
        else
        {
            tail->next = newnode;
            tail = newnode;
        }

        tmp1 = tmp1->next;
    }

    while (tmp2)
    {
        int num = tmp2->val;
        int ListNum = num + base;
        base = 0;

        if (ListNum > 9)
        {
            ListNum %= 10;
            base = 1;
        }

        struct ListNode* newnode = getNode(ListNum);

        if (tail == NULL)
        {
            gard->next = newnode;
            tail = newnode;
        }
        else
        {
            tail->next = newnode;
            tail = newnode;
        }

        tmp2 = tmp2->next;
    }

    if (base)
    {
        struct ListNode* newnode = getNode(base);

        tail->next = newnode;
        tail = newnode;
    }

    return gard->next;
}


//创建链表

static SLTNode* BuyNewNode(int x)
{
    SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode) * 1);
    assert(newnode);

    newnode->val = x;
    newnode->next = NULL;

    return newnode;
}

//尾插
void SLTPushBack(SLTNode** pphead, int* arr, int len)
{
    int i = 0;
    for (i = 0; i < len; i++)
    {
        SLTNode* newnode = BuyNewNode(arr[i]);
        if (*pphead == NULL)
            (*pphead) = newnode;
        else
        {
            SLTNode* tail = *pphead;
            while (tail->next)
                tail = tail->next;

            tail->next = newnode;
        }
    }
}

int main()
{
    SLTNode* l1 = NULL;
    SLTNode* l2 = NULL;
    int arr1[] = { 9,9,9,9,9,9,9 };
    int arr2[] = { 9,9,9,9 };

    SLTPushBack(&l1, arr1, sizeof(arr1) / sizeof(arr1[0]));
    SLTPushBack(&l2, arr2, sizeof(arr2) / sizeof(arr2[0]));

    SLTNode* list = addTwoNumbers(l1, l2);

    while (list)
    {
        printf("%d ", list->val);
        list = list->next;
    }

    return 0;
}