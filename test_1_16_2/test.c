#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

////废弃方案
////594. 最长和谐子序列
////https://leetcode.cn/problems/longest-harmonious-subsequence/
//
//int findLHS(int* nums, int numsSize) {
//    //尝试两层循环
//    int theLongest = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        int tmp = nums[i];
//        int len = 1;
//        int max = nums[i];
//        int min = nums[i];
//        for (int j = i + 1; j < numsSize; j++)
//        {
//            //考虑颠覆性问题
//            if (nums[j] > max)
//            {
//                if (nums[j] - min == 1)
//                {
//                    len++;
//                    max = nums[j];
//                }
//                else if (nums[j] - max == 1)
//                {
//                    len++;
//                    for (int k = 0; k < j; k++)
//                    {
//                        if (nums[k] == min)
//                            len--;
//                    }
//
//                    //重新更新信息
//                    min = max;
//                    max = nums[j];
//                }
//            }
//            else if (nums[j] < min)
//            {
//                if (max - nums[j] == 1)
//                {
//                    len++;
//                    min = nums[j];
//                }
//                else if (min - nums[j] == 1)
//                {
//                    len++;
//                    for (int k = 0; k < j; k++)
//                    {
//                        if (nums[k] == max)
//                            len--;
//                    }
//
//                    //重新更新信息
//                    max = min;
//                    min = nums[j];
//                }
//
//            }
//            else
//                len++;
//
//        }
//
//        //需要进行合法性检验，即不能全是一个数
//        if (len > theLongest && max != min)
//            theLongest = len;
//    }
//
//    return theLongest;
//}

//594. 最长和谐子序列
//https://leetcode.cn/problems/longest-harmonious-subsequence/

//尝试滑动窗口
int cmp(const void* e1, const void* e2)
{
    return *(int*)e1 - *(int*)e2;
}

int findLHS(int* nums, int numsSize) {
    qsort(nums, numsSize, sizeof(nums[0]), cmp);    //排序

    int prev = 0;
    int cur = 1;
    int tenLongest = 0;
    int mid = 0;
    while (cur < numsSize)
    {
        if (nums[cur] - nums[prev] <= 1)
        {
            while (cur < numsSize && nums[cur] - nums[prev] <= 1)
            {
                if (nums[cur - 1] == nums[prev])
                    mid = cur;
                cur++;
            }

            int len = cur - prev;
            if (len > tenLongest && nums[prev] != nums[cur - 1])
                tenLongest = len;

            //调制，继续滑动
            prev = mid;
        }
        else
        {
            prev = cur;
            cur++;
        }
    }

    return tenLongest;
}
int main()
{
    int arr[] = { 1,1,1,1 };
    findLHS(arr, sizeof(arr) / sizeof(arr[0]));
    return 0;
}