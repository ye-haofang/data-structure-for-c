#define _CRT_SECURE_NO_WARNINGS 1	

 //直接插入排序
int* sortArray(int* nums, int numsSize, int* returnSize) {
	//可以从第二个数据开始排序
	*returnSize = numsSize;
	for (int i = 1; i < numsSize; i++)
	{
		int end = i;
		int tmp = nums[end];	//临时存储待插入的值
		while (end)
		{
			if (tmp < nums[end - 1])
			{
				nums[end] = nums[end - 1];
				end--;	//此时需要将数据往后移动
			}
			else
				break;	//此时的 tmp 已经找到属于自己的位置了
		}
		nums[end] = tmp;	//插入数据
	}

	return nums;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */

 //希尔排序
int* sortArray(int* nums, int numsSize, int* returnSize) {
	*returnSize = numsSize;
	int gap = numsSize;	//默认gap为n
	while (gap > 1)
	{
		gap /= 2;	//要确保至少出现一次直接插入排序
		for (int i = 0; i < numsSize - gap; i++)
		{
			int end = i;
			int tmp = nums[end + gap];
			while (end >= 0)
			{
				if (tmp < nums[end])
				{
					nums[end + gap] = nums[end];
					end -= gap;
				}
				else
					break;
			}
			nums[end + gap] = tmp;
		}
	}
	return nums;
}