#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//匹配
//#include<assert.h>
//int numMatchingSubseq(char* s, char** words, int wordsSize) {
//    if (*s == '\0')
//        return 0;
//    int seq = 0;    //返回的单词个数
//    int i = 0;
//    char* star = s;
//    while (i < wordsSize)
//    {
//        s = star;
//        while (*s && *(words[i]))
//        {
//            if (*s == *(words[i]))
//                words[i]++;
//            s++;
//        }
//        if (!*(words[i]))
//            seq++;
//        i++;
//    }
//
//    return seq;
//}
//strchr
//寻找字符
//找到返回指针，没找到返回空指针
#include<assert.h>
#include<string.h>
int numMatchingSubseq(char* s, char** words, int wordsSize) {
    int seq = 0;    //返回的单词个数
    int i = 0;
    while (i < wordsSize)
    {
        char* src = s;    //重头再来
        while (src && *(words[i]))
        {
            src = strchr(src, *(words[i])); //在tmp中查找目标字符
            if (src)
            {
                src++;  //向后走一步，避免重复匹配
                (words[i])++;  //向后寻找下一个字符
            }
        }
        if (!*(words[i]))
            seq++;
        i++;
    }
    return seq;
}
int main()
{
    char* s = "abcde";
    char* words[4] = { "a","bb","acd","ace" };
    int len = numMatchingSubseq(s, words, 4);
    printf("%d\n",len);
    return 0;
}

////LCP 50. 宝石补给
//int giveGem(int* gem, int gemSize, int** operations, int operationsSize, int* operationsColSize)
//{
//    int max = gem[0];
//    int min = gem[0];
//
//    int times = 0;  //需要交换的次数
//    while (times < operationsSize)
//    {
//        int pos1 = operations[times][0];
//        int pos2 = operations[times][1];
//
//        gem[pos2] += gem[pos1] / 2; //宝石给勇士2
//        gem[pos1] %= 2; //勇士1会失去对应的宝石数
//
//        max < gem[pos2] ? max = gem[pos2] : max;
//        min > gem[pos1] ? min = gem[pos1] : min;
//
//        times++;
//    }
//
//    return max - min;
//}