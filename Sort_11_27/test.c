#define _CRT_SECURE_NO_WARNINGS 1	
#include"Sort.h"

void TestInsertSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3,5 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	InsertSort(arr, len);

	SortPrint(arr, len);
}

void TestShellSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3,5 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	ShellSort(arr, len);

	SortPrint(arr, len);
}

void TestSelectSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3,5 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	SelectSort(arr, len);	//ѡ������
	
	SortPrint(arr, len);
}

void TestBubbleSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3,5 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	BubbleSort(arr, len);

	SortPrint(arr, len);
}

void TestHeapSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3,5 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	HeapSort(arr, len);

	SortPrint(arr, len);
}

void TestQuickSort()
{
	int arr[] = { 9,1,2,5,7,4,8,6,3 };
	int len = sizeof(arr) / sizeof(arr[0]);

	SortPrint(arr, len);

	QuickSort(arr, 0, len - 1);

	SortPrint(arr, len);
}

int cmp(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}

void SortOP()
{
	srand((size_t)time(NULL));
	int n = 10000000;
	int* a1 = (int*)malloc(sizeof(int) * n);
	int* a2 = (int*)malloc(sizeof(int) * n);
	int* a3 = (int*)malloc(sizeof(int) * n);
	int* a4 = (int*)malloc(sizeof(int) * n);
	int* a5 = (int*)malloc(sizeof(int) * n);
	int* a6 = (int*)malloc(sizeof(int) * n);
	assert(a1 && a2 && a3 && a4 && a5 && a6);

	int i = 0;
	for (i = 0; i < n; i++)
	{
		a1[i] = rand() + i;
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		a5[i] = a1[i];
		a6[i] = a1[i];
	}

	int begin1 = clock();
	//InsertSort(a1, n);
	int end1 = clock();

	int begin2 = clock();
	ShellSort(a2, n);
	int end2 = clock();

	int begin3 = clock();
	//SelectSort(a3, n);
	int end3 = clock();

	int begin4 = clock();
	HeapSort(a4, n);
	int end4 = clock();

	int begin5 = clock();
	BubbleSort(a5, n);
	int end5 = clock();

	int begin6 = clock();
	QuickSort(a6, 0, n - 1);
	//qsort(a5, n, sizeof(int), cmp);
	int end6 = clock();

	printf("InsertSort: %d\n", end1 - begin1);
	printf("ShellSort: %d\n", end2 - begin2);
	printf("SelectSort: %d\n", end3 - begin3);
	printf("HeapSort: %d\n", end4 - begin4);
	printf("BubbleSort: %d\n", end5 - begin5);
	printf("QuickSort: %d\n", end6 - begin6);

	free(a1);
	free(a2);
	free(a3);
	free(a4);
	free(a5);
}

int main()
{
	//TestInsertSort();
	TestShellSort();
	//TestSelectSort();
	//TestBubbleSort();
	//TestHeapSort();
	//TestQuickSort();
	//SortOP();
	return 0;
}