#define _CRT_SECURE_NO_WARNINGS 1	
#include"Sort.h"

void SortPrint(int* pa, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
		printf("%d ", pa[i]);

	printf("\n");
}


//思路：从第二个数据开始，与它之前的数据比对，找到合适位置插入其中
void InsertSort(int* pa, int n)	//插入排序
{
	int i = 0;
	for (i = 0; i < n - 1; i++)
	{

		int end = i;
		int tmp = pa[end + 1];
		while (end >= 0)
		{
			if (tmp < pa[end])
			{
				pa[end + 1] = pa[end];
				end--;
			}
			else
				break;
		}
		pa[end + 1] = tmp;	//将原 end+1 处的值插入到合适位置
	}
}

////有点差的版本
////思路：借助增量 gap ，分块排序，最后在进行步长 1 的直接插入排序
//void ShellSort(int* pa, int n)	//希尔排序
//{
//	//假设gap为3
//	int gap = n / 2;
//	int i = 0;
//	for (i = 0; i < n - gap; i++)
//	{
//
//		int end = i;
//		int tmp = pa[end + gap];
//		while (end >= 0)
//		{
//			if (tmp < pa[end])
//			{
//				pa[end + gap] = pa[end];
//				end -= gap;
//			}
//			else
//				break;
//		}
//		pa[end + gap] = tmp;	//将原 end+1 处的值插入到合适位置
//	}
//
//	InsertSort(pa, n);	//直接插入排序
//}


////思想：借助增量 gap ，一般为 n / 2 或 n / 3 + 1
////确保 gap 最后为 1，自适应式排序
//void ShellSort(int* pa, int n)	//希尔排序
//{
//	int gap = n;
//	while (gap > 1)
//	{
//		//int gap = n / 2;
//		gap = gap / 3 + 1;
//		int i = 0;
//		for (i = 0; i < n - gap; i++)
//		{
//
//			int end = i;
//			int tmp = pa[end + gap];
//			while (end >= 0)
//			{
//				if (tmp < pa[end])
//				{
//					pa[end + gap] = pa[end];
//					end -= gap;
//				}
//				else
//					break;
//			}
//			pa[end + gap] = tmp;	//将原 end+1 处的值插入到合适位置
//		}
//	}
//}

//自写版
void ShellSort(int* pa, int n)	//希尔排序
{
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		int i = 0;
		for (; i < n - gap; i++)
		{
			int end = i;
			int tmp = pa[end + gap];
			while (end >= 0)
			{
				if (tmp < pa[end])
				{
					pa[end + gap] = pa[end];
					end -= gap;
				}
				else
					break;
			}
			pa[end + gap] = tmp;
		}

	}
}

void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

//思路：找出最小的，放到前面，找到最大的放到后面（升序）
void SelectSort(int* pa, int n)	//选择排序
{
	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		int maxi = begin;
		int mini = begin;
		for (int i = begin + 1; i <= end; i++)
		{
			if (pa[maxi] < pa[i])
				maxi = i;
			if (pa[mini] > pa[i])
				mini = i;
		}
		if (maxi == begin)
			maxi = mini;
		Swap(&pa[mini], &pa[begin]);
		Swap(&pa[maxi], &pa[end]);
		begin++;
		end--;
	}
}

void AdjustDown(int* pa, int len, int parent)
{

	//向下调整，主要是把大孩子(小孩子)往上面交换

	//假设法，假设左孩子为大孩子(小孩子)
	int child = (parent * 2) + 1;
	while (child < len)
	{
		if (child + 1 < len && pa[child + 1] > pa[child])
			child += 1;	//变成右孩子，child+1 < len，防止越界

		//和向上调整一样的逻辑，推荐用符号判别大小堆
		if (pa[child] > pa[parent])
		{
			Swap(&pa[child], &pa[parent]);
			parent = child;	//父亲变成孩子
			child = (parent * 2) + 1;	//更新孩子
		}
		else
			break;
	}
}
void HeapSort(int* pa, int n)	//堆排序
{
	//升序，建小堆
	/*
	* 步骤
	* 1.建堆
	* 2.把堆顶的元素，交换到堆底
	* 3.交换完成后，堆的长度缩减1
	* 4.如此重复，直到交换完成
	*/

	int i = (n - 2) / 2;
	for (; i >= 0; i--)
	{
		AdjustDown(pa, n, i);	//建堆
	}

	i = n - 1;
	while (i > 0)
	{
		Swap(&pa[0], &pa[i]);
		AdjustDown(pa, i, 0);
		i--;
	}
}


//思路：两层for循环，遍历冒泡比较
void BubbleSort(int* pa, int n)	//冒泡排序
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		int j = 0;
		int flag = 1;
		for (j = 1; j < n - i; j++)
		{
			if (pa[j - 1] > pa[j])
			{
				int tmp = pa[j - 1];
				pa[j - 1] = pa[j];
				pa[j] = tmp;
				flag = 0;
			} 
		}
		if (flag)
			break;
	}
}

void QuickSort(int* pa, int begin, int end)	//快速排序
{
	//如果起点大于等于终点，说明已经排完了
	if (begin >= end)
		return;
	int left = begin;
	int key = left;	//key关键值
	int right = end;
	while (left < right)
	{
		//因为 key 在左边，所以右先走
		//右边要找到比 key 小
		while (left < right && pa[right] >= pa[key])
			right--;

		while (left < right && pa[left] <= pa[key])
			left++;

		Swap(&pa[left], &pa[right]);
	}
	//此时left和right相遇，谁交换都行
	Swap(&pa[left], &pa[key]);

	//现在可以分成三截
	//begin、left、end，其中 left 这个点不需要参与运算了
	QuickSort(pa, begin, left - 1);
	QuickSort(pa, left + 1, end);
}