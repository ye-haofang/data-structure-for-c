#define _CRT_SECURE_NO_WARNINGS 1	
#include"SeqList.h"


//void TestSeqList1()
//{
//	SL s;
//	SeqListInit(&s);
//
//	SeqListPushBack(&s, 1);
//	SeqListPushBack(&s, 2);
//	SeqListPushBack(&s, 3);
//	SeqListPrint(&s);
//
//	SeqListPushFront(&s, 4);
//	SeqListPushFront(&s, 5);
//	SeqListPushFront(&s, 6);
//	SeqListPrint(&s);
//
//	SeqListInsert(&s, 0, 200);	//实现头插
//	SeqListInsert(&s, s.size, 100);	//实现尾插
//	SeqListPrint(&s);
//
//	SeqListErase(&s, 0);	//实现头删
//	SeqListErase(&s, s.size - 1);	//实现尾删
//	SeqListPrint(&s);
//
//	SLDatatype x = 0;
//	printf("请输入你想删除的元素:>");
//	scanf("%d", &x);
//	SeqListErase(&s, SeqListFind(&s, x));
//	SeqListPrint(&s);
//
//	SeqListDestroy(&s);
//}

void menu()
{
	printf("*******************************\n");
	printf("******  0.退出   1.打印  ******\n");
	printf("******  2.尾插   3.尾删  ******\n");
	printf("******  4.头插   5.头删  ******\n");
	printf("******  6.任意插 7.任意删******\n");
	printf("******  8.按元素值插入   ******\n");
	printf("******  9.按元素值删除   ******\n");
	printf("*******************************\n");
}

int main()
{
	int input = 1;
	SL s;
	SeqListInit(&s);	//创建一个顺序表
	while (input)
	{
		int pos = 0;
		SLDatatype x = 0;
		SLDatatype y = 0;
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 0:
			printf("退出顺序表!\n");
			SeqListDestroy(&s);
			break;
		case 1:
			SeqListPrint(&s);
			break;
		case 2:
			printf("请输入一个值:>");
			scanf("%d", &x);
			SeqListPushBack(&s, x);
			break;
		case 3:
			SeqListPopBack(&s);
			break;
		case 4:
			printf("请输入一个值:>");
			scanf("%d", &x);
			SeqListPushFront(&s, x);
			break;
		case 5:
			SeqListPopFront(&s);
			break;
		case 6:
			printf("请输入下标和目标值:>");
			scanf("%d %d", &pos, &x);
			SeqListInsert(&s, pos, x);
			break;
		case 7:
			printf("请输入下标:>");
			scanf("%d", &pos);
			SeqListErase(&s, pos);
			break;
		case 8:
			printf("请输入待插入元素值和目标值:>");
			scanf("%d %d", &y, &x);
			SeqListInsert(&s, SeqListFind(&s, y), x);
			break;
		case 9:
			printf("请输入待删除元素值:>");
			scanf("%d", &y);
			SeqListErase(&s, SeqListFind(&s, y));
			break;
		default :
			printf("选择错误，请重新选择!\n");
			break;
		}
	}
	return 0;
}