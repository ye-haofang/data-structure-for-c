#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

typedef int SLDatatype;	//顺序表类型
typedef struct SeqListInfo	//基本结构
{
	SLDatatype* data;	//数据
	size_t size;	//实际有效数据数
	size_t capacity;	//容量
}SL;

void SeqListInit(SL* ps);	//顺序表初始化
void SeqListDestroy(SL* ps);	//销毁顺序表

void SeqListPrint(SL* ps);	//打印顺序表

void SeqListPushBack(SL* ps, SLDatatype x);		//尾插
void SeqListPopBack(SL* ps);	//尾删

void SeqListPushFront(SL* ps, SLDatatype x);		//头插
void SeqListPopFront(SL* ps);	//头删

void SeqListInsert(SL* ps, int pos, SLDatatype x);	//任意插
void SeqListErase(SL* ps, int pos);	//任意位置删除

int SeqListFind(SL* ps, SLDatatype x);	//查找元素