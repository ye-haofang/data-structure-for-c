#define _CRT_SECURE_NO_WARNINGS 1	

//HJ99 自守数

#include <stdio.h>

int main() {
    //暴力枚举
    int n = 0;
    scanf("%d", &n);

    int digits = 10;
    int count = 0;  //计数器
    for (int i = 0; i <= n; i++)
    {
        if (i >= 10 && i % 10 == 0 && (i / digits) == 1)
        {
            //逢十更新count
            digits *= 10;    //位数+1
        }

        int tmp = i * i;
        if (i == (tmp % digits))
            count++;
    }

    printf("%d\n", count);

    return 0;
}


//OR86 返回小于N的质数个数

#include <stdio.h>
#include<math.h>
#include<stdbool.h>

//大于2的偶数肯定不是质数，可以通过开平方减少循环次数
int getPrimeNumber(const int n)
{
    int count = 0;
    if (n > 2)
        count++;    //特殊处理

    for (int i = 3; i < n; i += 2)
    {
        bool flag = true;
        for (int j = 3; j <= (int)sqrt(i); j++)
        {
            if (i % j == 0)
            {
                flag = false;
                break;
            }
        }

        if (flag)
            count++;
    }

    return count;
}

int main() {
    int n = 0;
    scanf("%d", &n);
    printf("%d\n", getPrimeNumber(n));
    return 0;
}