#define _CRT_SECURE_NO_WARNINGS 1	
//力扣 232. 用栈实现队列
// https://leetcode.cn/problems/implement-queue-using-stacks/

#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>

//先写一个顺序栈
typedef int STDataType;

typedef struct StackInfo
{
    STDataType* data;   //数据域
    int top;    //栈顶
    int capacity;
}STNode;

void STNodeInit(STNode* ps)
{
    assert(ps);
    ps->data = (STDataType*)malloc(sizeof(STDataType) * 4); //默认空间为4
    ps->top = 0;    //栈顶为0，说明栈空
    ps->capacity = 4;
}

void STNodeDestroy(STNode* ps)
{
    assert(ps);

    free(ps->data); //顺序栈是连续的，可以一次性释放
    ps->data = NULL;
    ps->top = ps->capacity = 0;
}

bool STNodeEmpty(STNode* ps)
{
    assert(ps);

    return ps->top == 0;
}

void STNodePush(STNode* ps, STDataType x)
{
    assert(ps);

    if (ps->top == ps->capacity)
    {
        STDataType* tmp = realloc(ps->data, sizeof(STDataType) * 2);
        if (!tmp)
        {
            perror("malloc file");
            exit(-1);
        }
        ps->data = tmp;
        ps->capacity *= 2;  //二倍扩容法
    }

    ps->data[ps->top++] = x;
}

void STNodePop(STNode* ps)
{
    assert(ps);
    assert(!STNodeEmpty(ps));    //至少有一个元素

    ps->top--;  //-1即删除
}

STDataType STNodeTop(STNode* ps)
{
    assert(ps);
    assert(!STNodeEmpty(ps));

    return ps->data[ps->top - 1];
}

int STNodeSize(STNode* ps)
{
    assert(ps);

    int i = ps->top;
    int size = 0;
    while (i--)
    {
        size++;
    }

    return size;
}

typedef struct {
    STNode pushst;
    STNode popst;
} MyQueue;


MyQueue* myQueueCreate() {
    MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
    assert(obj);

    STNodeInit(&obj->pushst);
    STNodeInit(&obj->popst);

    return obj;
}

bool myQueueEmpty(MyQueue* obj);

void myQueuePush(MyQueue* obj, int x) {
    assert(obj);

    STNodePush(&obj->pushst, x);
}

int myQueuePeek(MyQueue* obj);

int myQueuePop(MyQueue* obj) {
    assert(obj);
    assert(!myQueueEmpty(obj));

    STDataType x = myQueuePeek(obj);    //经过peek处理，出队栈是一定有数据的

    STNodePop(&obj->popst);

    return x;

}

int myQueuePeek(MyQueue* obj) {
    assert(obj);
    assert(!myQueueEmpty(obj));

    if (STNodeEmpty(&obj->popst))
    {
        //如果出队栈为空，就去隔壁取数据来进行出队
        while (!STNodeEmpty(&obj->pushst))
        {
            STDataType x = STNodeTop(&obj->pushst);
            STNodePop(&obj->pushst);
            STNodePush(&obj->popst, x); //将数据压入出队栈
        }
    }

    STDataType x = STNodeTop(&obj->popst);

    return x;
}

bool myQueueEmpty(MyQueue* obj) {
    assert(obj);

    return STNodeEmpty(&obj->pushst) && STNodeEmpty(&obj->popst);
}

void myQueueFree(MyQueue* obj) {
    assert(obj);

    STNodeDestroy(&obj->pushst);
    STNodeDestroy(&obj->popst);

    free(obj);
    obj = NULL;
}

int main()
{
    MyQueue* obj = myQueueCreate();
    

    int a = 0;

    myQueuePush(obj, 1);
    myQueuePush(obj, 2);
    myQueuePush(obj, 3);
    myQueuePush(obj, 4);

    a = myQueuePop(obj);

    myQueuePush(obj, 5);

    a = myQueuePop(obj);
    a = myQueuePop(obj);
    a = myQueuePop(obj);
    a = myQueuePop(obj);

    myQueueEmpty(obj);

    return 0;
}