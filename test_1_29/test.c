#define _CRT_SECURE_NO_WARNINGS 1	

//13. 罗马数字转整数
//https://leetcode.cn/problems/roman-to-integer/

int getNum(char x)
{
    switch (x)
    {
    case 'I':
        return 1;
        break;
    case 'V':
        return 5;
        break;
    case 'X':
        return 10;
        break;
    case 'L':
        return 50;
        break;
    case 'C':
        return 100;
        break;
    case 'D':
        return 500;
        break;
    case 'M':
        return 1000;
        break;
    default:
        return 0;
        break;
    }
}

int romanToInt(char* s) {
    int val = 0;
    char* ps = s;
    while (*s)
    {
        if (*(s + 1))
        {
            int n1 = getNum(*s);
            int n2 = getNum(*(s + 1));
            if (n2 > n1)
            {
                val += (n2 - n1);
                s += 2;
            }
            else
            {
                val += n1;
                s++;
            }
        }
        else
        {
            int n = getNum(*s);
            val += n;
            s++;
        }
    }

    return val;
}