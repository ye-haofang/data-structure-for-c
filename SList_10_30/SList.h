#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLTDatatype;

typedef struct SListNode
{
	SLTDatatype data;	//数据域
	struct SListNode* next;	//指针域
}SLTNode;

//打印
void SLTPrint(SLTNode* phead);

//尾插与尾删
void SLTPushBack(SLTNode** pphead, SLTDatatype x);
void SLTPopBack(SLTNode** pphead);

//头插与头删
void SLTPushFront(SLTNode** pphead, SLTDatatype x);
void SLTPopFront(SLTNode** pphead);

//查找
SLTNode* SLTFind(SLTNode* plist, SLTDatatype x);

//任意位置插入与删除
//后插
void SLTInsertAfter(SLTNode* pos, SLTDatatype x);
void SLTEraseAfter(SLTNode* pos);

//单链表销毁
void SListDestroy(SLTNode* plist);