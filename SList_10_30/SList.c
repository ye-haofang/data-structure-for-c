#define _CRT_SECURE_NO_WARNINGS 1	
#include"SList.h"

//打印
void SLTPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

static SLTNode* BuyNewNode(SLTDatatype x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode) * 1);
	assert(newnode);

	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

//尾插与尾删
void SLTPushBack(SLTNode** pphead, SLTDatatype x)
{
	SLTNode* newnode = BuyNewNode(x);
	if (*pphead == NULL)
		(*pphead) = newnode;
	else
	{
		SLTNode* tail = *pphead;
		while (tail->next)
			tail = tail->next;

		tail->next = newnode;
	}
}

void SLTPopBack(SLTNode** pphead)
{
	assert(*pphead);

	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* prev = NULL;
		SLTNode* tail = *pphead;
		while (tail->next)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		prev->next = NULL;
	}
}

//头插与头删
void SLTPushFront(SLTNode** pphead, SLTDatatype x)
{
	SLTNode* newnode = BuyNewNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SLTPopFront(SLTNode** pphead)
{
	assert(*pphead);
	SLTNode* next = (*pphead)->next;
	free(*pphead);
	*pphead = next;
}

//查找
SLTNode* SLTFind(SLTNode* plist, SLTDatatype x);

//任意位置插入与删除
//后插
void SLTInsertAfter(SLTNode* pos, SLTDatatype x);
void SLTEraseAfter(SLTNode* pos);

//单链表销毁
void SListDestroy(SLTNode* plist)
{
	SLTNode* cur = plist;
	while (plist)
	{
		cur = plist;
		plist = plist->next;
		free(cur);
	}
}