#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>
#include<string.h>

typedef struct ListNode
{
    int val;
    struct ListNode* next;
}SLTNode;

//废弃方案
////1171. 从链表中删去总和值为零的连续节点
//
//struct ListNode* removeZeroSumSublists(struct ListNode* head) {
//    //三步走：转数组、判断、转链表
//    int* pa = (int*)calloc(1000, sizeof(int));
//    assert(pa);
//    memset(pa, 1, sizeof(int)*1000);
//
//    int pos = 0;
//    struct ListNode* tmp = head;
//    while (tmp)
//    {
//        pa[pos++] = tmp->val;
//        tmp = tmp->next;
//    }
//
//    int accVal = 0; //累加值
//    int acci = 0;   //起始下标
//    int comVal = 0; //比较值
//    int comi = 0;   //结束下标
//    for (int i = 0; i < pos; i++)
//    {
//        if (pa[i] < 0)
//        {
//            if (comVal < 0)
//            {
//                comi = i + 1;
//                accVal = 0;
//            }
//            comVal += pa[i];
//        }
//        else
//        {
//            if (accVal == 0)
//                acci = i;   //记录此时的下标
//            accVal += pa[i];
//        }
//
//        //判断环节
//        //理想情况
//        if (accVal + comVal == 0)
//        {
//            for (int j = acci; j <= i; j++)
//                pa[j] = 16843009;   //消除节点
//            //需要把累加值和比较值清零
//            accVal = comVal = 0;
//        }
//        else if (comVal < 0)
//        {
//            //此时可能存在有效配对组，需要进一步调查
//            for (int j = comi; j < i; j++)
//            {
//                acci = j;   //修正下标
//                accVal = 0;
//                for (int k = j; k < i; k++)
//                {
//                    if(pa[k] != 16843009 && pa[k] > 0)
//                        accVal += pa[k];    //重新计算累加值
//                }
//
//                //判断是否符合条件
//                if (accVal + comVal == 0)
//                {
//                    for (int jj = acci; jj <= i; jj++)
//                        pa[jj] = 16843009;
//                    accVal = comVal = 0;
//                    comi = 0;
//                }
//            }
//        }
//    }
//
//    tmp = head; //转链表
//    struct ListNode* cur = NULL;
//    for (int i = 0; i < pos; i++)
//    {
//        //合法数据
//        if (pa[i] != 16843009)
//        {
//            //合法数据可以入链表
//            tmp->val = pa[i];
//            cur = tmp;
//            tmp = tmp->next;
//        }
//    }
//
//    if (tmp != head)
//    {
//        cur->next = NULL; //断尾操作
//        return head;
//    }
//
//    return cur;
//}

////废弃方案二
////1171. 从链表中删去总和值为零的连续节点
//
//struct ListNode* removeZeroSumSublists(struct ListNode* head) {
//    //三步走：转数组、判断、转链表
//    int* pa = (int*)calloc(1000, sizeof(int));
//    assert(pa);
//    memset(pa, 1, sizeof(int) * 1000);
//
//    int pos = 0;
//    struct ListNode* tmp = head;
//    while (tmp)
//    {
//        pa[pos++] = tmp->val;
//        tmp = tmp->next;
//    }
//
//    int accVal = 0; //累加值
//    int begi = 0;   //起始下标
//    int comVal = 0; //比较值
//    int endi = 0;   //结束下标
//    bool flag = true;
//
//    //维护空间
//    for (int i = 0; i < pos; i++)
//    {
//        if (pa[i] > 0)
//        {
//            if (pa[i] != 16843009)
//                accVal += pa[i];
//            endi++;
//        }
//        else if(pa[i] < 0)
//        {
//            if (comVal < 0 && flag)
//                begi = i - 1;   //矫正起始下标
//
//            if (pa[i] != 16843009)
//                comVal += pa[i];
//        }
//
//        if (accVal + comVal == 0)
//        {
//            for (int k = begi; k <= endi; k++)
//                pa[k] = 16843009;
//        }
//        else if (accVal + comVal > 0 && comVal < 0)
//        {
//            for (int j = begi + 1; j < endi; j++)
//            {
//                accVal = 0;
//                for (int x = j; x < endi; x++)
//                {
//                    if (pa[x] != 16843009 && pa[x] > 0)
//                        accVal += pa[x];
//                }
//
//                if (accVal + comVal == 0)
//                {
//                    for (int k = j; k <= endi; k++)
//                        pa[k] = 16843009;
//                    begi = 0;
//                    endi++;
//                }
//            }
//        }
//    }
//
//    tmp = head; //转链表
//    struct ListNode* cur = NULL;
//    for (int i = 0; i < pos; i++)
//    {
//        //合法数据
//        if (pa[i] != 16843009)
//        {
//            //合法数据可以入链表
//            tmp->val = pa[i];
//            cur = tmp;
//            tmp = tmp->next;
//        }
//    }
//
//    if (tmp != head)
//    {
//        cur->next = NULL; //断尾操作
//        return head;
//    }
//
//    return cur;
//}
//

////已过，但效率很低
////1171. 从链表中删去总和值为零的连续节点
//
//struct ListNode* removeZeroSumSublists(struct ListNode* head) {
//    //三步走：转数组、判断、转链表
//    int* pa = (int*)calloc(1000, sizeof(int));
//    assert(pa);
//    memset(pa, 1, sizeof(int) * 1000);
//
//    int pos = 0;
//    struct ListNode* tmp = head;
//    while (tmp)
//    {
//        pa[pos++] = tmp->val;
//        tmp = tmp->next;
//    }
//
//    int accVal = 0;
//    int begi = 0;
//    int endi = 0;
//    int spePos = 0;
//
//    for (int i = 0; i < pos; i++)
//    {
//        bool flag = true;
//        accVal = 0;
//
//        if (1)
//        {
//            spePos = i;
//            //向左找
//            while (spePos >= 0)
//            {
//                if(pa[spePos] != 16843009)
//                    accVal += pa[spePos];
//
//                if (accVal == 0)
//                {
//                    flag = false;
//                    for (int k = spePos; k <= i; k++)
//                        pa[k] = 16843009;
//
//                    break;
//                }
//                spePos--;
//            }
//            accVal = 0;
//            //向右找
//            spePos = i;
//            while (spePos < pos && flag)
//            {
//                if (pa[spePos] != 16843009)
//                    accVal += pa[spePos];
//
//                if (accVal == 0)
//                {
//                    flag = false;
//                    for (int k = i; k <= spePos; k++)
//                        pa[k] = 16843009;
//                    break;
//                }
//                spePos++;
//            }
//            accVal = pa[i];
//            //全局找，两边扩散
//            int leftPos = i - 1;
//            int rightPos = i + 1;
//            while ((leftPos >= 0 || rightPos < pos) && flag)
//            {
//                if (leftPos >= 0 &&  pa[leftPos] != 16843009)
//                    accVal += pa[leftPos];
//                if (rightPos < pos &&  pa[rightPos] != 16843009)
//                    accVal += pa[rightPos];
//
//                leftPos--, rightPos++;
//
//                if (accVal == 0)
//                {
//                    for (int k = i; k < rightPos; k++)
//                        pa[k] = 16843009;
//                    for (int k = leftPos + 1; k < i; k++)
//                        pa[k] = 16843009;
//                    break;
//                }
//            }
//        }
//    }
//
//    tmp = head; //转链表
//    struct ListNode* cur = NULL;
//    for (int i = 0; i < pos; i++)
//    {
//        //合法数据
//        if (pa[i] != 16843009)
//        {
//            //合法数据可以入链表
//            tmp->val = pa[i];
//            cur = tmp;
//            tmp = tmp->next;
//        }
//    }
//
//    if (tmp != head)
//    {
//        cur->next = NULL; //断尾操作
//        return head;
//    }
//
//    free(pa);
//    pa = NULL;
//
//    return cur;
//}


//优化版，删除不必要的循环，空间采取动态申请的方式
//1171. 从链表中删去总和值为零的连续节点

struct ListNode* removeZeroSumSublists(struct ListNode* head) {
    //三步走：转数组、判断、转链表
    struct ListNode* tmp = head;

    int len = 0;
    while (tmp)
    {
        tmp = tmp->next;
        len++;
    }

    int* pa = (int*)calloc(1, sizeof(int) * len);
    assert(pa);
    memset(pa, 1, sizeof(int) * len);

    tmp = head;
    int pos = 0;
    while (tmp)
    {
        pa[pos++] = tmp->val;
        tmp = tmp->next;
    }

    int accVal = 0;
    int begi = 0;
    int endi = 0;
    int spePos = 0;

    for (int i = 0; i < pos; i++)
    {
        bool flag = true;
        accVal = 0;

        spePos = i;
        //向左找
        while (spePos >= 0)
        {
            if (pa[spePos] != 16843009)
                accVal += pa[spePos];

            if (accVal == 0)
            {
                flag = false;
                for (int k = spePos; k <= i; k++)
                    pa[k] = 16843009;

                break;
            }
            spePos--;
        }
        accVal = 0;
        //向右找
        spePos = i;
        while (spePos < pos && flag)
        {
            if (pa[spePos] != 16843009)
                accVal += pa[spePos];

            if (accVal == 0)
            {
                flag = false;
                for (int k = i; k <= spePos; k++)
                    pa[k] = 16843009;
                break;
            }
            spePos++;
        }
    }

    tmp = head; //转链表
    struct ListNode* cur = NULL;
    for (int i = 0; i < pos; i++)
    {
        //合法数据
        if (pa[i] != 16843009)
        {
            //合法数据可以入链表
            tmp->val = pa[i];
            cur = tmp;
            tmp = tmp->next;
        }
    }

    if (tmp != head)
    {
        cur->next = NULL; //断尾操作
        return head;
    }

    free(pa);
    pa = NULL;

    return cur;
}

extern void SLTPushBack(SLTNode** pphead, int* arr, int len);

int main()
{
    int arr[] = { -3,-1,1,-1,-4,1,-3,-1,3,3 };
    SLTNode* L = NULL;
    SLTPushBack(&L, arr, sizeof(arr) / sizeof(arr[0]));
    removeZeroSumSublists(L);
    return 0;
}