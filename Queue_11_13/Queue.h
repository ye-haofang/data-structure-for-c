#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

typedef int QDataType;

typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QNode;

typedef struct Queue
{
	QNode* head;
	QNode* tail;
	size_t size;
}Queue;

void QueueInit(Queue* pq);	//初始化
void QueueDestroy(Queue* pq);	//销毁

void QueuePush(Queue* pq, QDataType x);	//入队
void QueuePop(Queue* pq);	//出队

QDataType QueueBack(Queue* pq);	//返回队尾元素
QDataType QueueFront(Queue* pq);	//返回队头元素

bool QueueEmpty(Queue* pq);	//判断队空
size_t QueueSize(Queue* pq);	//求队的大小
