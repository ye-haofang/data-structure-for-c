#define _CRT_SECURE_NO_WARNINGS 1	
#include"Queue.h"

void QueueInit(Queue* pq)	//初始化
{
	assert(pq);

	pq->head = pq->tail = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)	//销毁
{
	assert(pq);
	QNode* cur = pq->head;
	while (cur)
	{
		QNode* tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	pq->head = pq->tail = NULL;
	pq->size = 0;
}

void QueuePush(Queue* pq, QDataType x)	//入队
{
	assert(pq);

	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	assert(newnode);
	newnode->data = x;
	newnode->next = NULL;

	if (pq->head && pq->tail)
	{
		pq->tail->next = newnode;
		pq->tail = newnode;
	}
	else
	{
		pq->head = pq->tail = newnode;
	}

	pq->size++;
}

void QueuePop(Queue* pq)	//出队
{
	assert(pq);
	assert(!QueueEmpty(pq));

	QNode* cur = pq->head;
	pq->head = pq->head->next;
	free(cur);
	cur = NULL;

	if (pq->head == NULL)
		pq->tail = NULL;

	pq->size--;
}

QDataType QueueBack(Queue* pq)	//返回队尾元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->tail->data;
}

QDataType QueueFront(Queue* pq)	//返回队头元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->head->data;
}

bool QueueEmpty(Queue* pq)	//判断队空
{
	assert(pq);

	return pq->head == NULL && pq->tail == NULL;
}

size_t QueueSize(Queue* pq)	//求队的大小
{
	assert(pq);

	return pq->size;
}