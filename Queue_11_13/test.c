#define _CRT_SECURE_NO_WARNINGS 1	
#include"Queue.h"

void TestDemo(void)
{
	Queue Q;
	QueueInit(&Q);

	int i = 0;
	for (i = 0; i < 100; i++)
	{
		QueuePush(&Q, i);
	}

	printf("%zu\n", QueueSize(&Q));
	printf("%d\n", QueueFront(&Q));
	printf("%d\n", QueueBack(&Q));

	printf("队列空情况:%d\n", QueueEmpty(&Q));

	for (i = 0; i < 99; i++)
	{
		QueuePop(&Q);
	}

	printf("%zu\n", QueueSize(&Q));
	printf("%d\n", QueueFront(&Q));
	printf("%d\n", QueueBack(&Q));

	printf("队列空情况:%d\n", QueueEmpty(&Q));

	QueueDestroy(&Q);
}

int main()
{
	TestDemo();
	return 0;
}