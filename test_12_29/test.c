#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>
#include<string.h>

//面试题 01.02. 判定是否互为字符重排---未过

bool CheckPermutation(char* s1, char* s2) {
    //直接strstr伺候
    int len1 = strlen(s1) * 2 + 1;
    int len2 = strlen(s2) * 2 + 1;

    char* str1 = (char*)calloc(len1, sizeof(int));
    char* str2 = (char*)calloc(len2, sizeof(int));
    assert(str1 && str2);

    for (int i = 0; i < 2; i++)
    {
        strcat(str1, s1);
        strcat(str2, s2);
    }

    char* tmp1 = strstr(str1, s2);
    char* tmp2 = strstr(str2, s1);

    free(str1);
    free(str2);
    str1 = str2 = NULL;

    return tmp1 && tmp2;
}

//面试题 01.02. 判定是否互为字符重排

bool CheckPermutation(char* s1, char* s2) {
    //映射

    int* tmp = (int*)malloc(sizeof(int) * 26);
    assert(tmp);
    memset(tmp, 0, sizeof(int) * 26);

    char* str1 = s1;
    char* str2 = s2;
    int i = 0;
    while (*(str1 + i))
        tmp[*(str1 + i++) - 'a']++;

    i = 0;
    while (*(str2 + i))
        tmp[*(str2 + i++) - 'a']--;

    i = 0;
    while (i < 26)
    {
        if (tmp[i++] != 0)
            return false;
    }

    free(tmp);
    tmp = NULL;

    return true;;
}

//面试题 01.04. 回文排列

bool canPermutePalindrome(char* s) {
    //两种情况可以构成回文数：所有字符呈偶数对、除一个字符外，所有字符呈偶数对
    //跟上一题很像。全字符对待
    int* tmp = (int*)calloc(128, sizeof(int));
    assert(tmp);

    char* str = s;
    while (*str)
    {
        int pos = (int)*str;    //下标

        //尝试配平
        if (tmp[pos] == 0)
            tmp[pos]++;
        else
            tmp[pos]--;

        str++;
    }

    int sum = 0;
    for (int i = 0; i < 128; i++)
        sum += tmp[i];

    free(tmp);
    tmp = NULL;

    return sum == 0 || sum == 1;    //两种情况
}

int main()
{
    char s1[] = "abc";
    char s2[] = "bca";
    printf("%d\n", CheckPermutation(s1, s2));
    return 0;
}