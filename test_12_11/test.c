#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

//归并排序与计数排序

void _MergeSort(int* pa, int begin, int end, int* tmp)	//归并主程序
{
	assert(pa);

	if (begin >= end)
		return;	//非必要情况 + 非法情况

	int mid = (begin + end) / 2;	//取中间数，分成两部分
	
	_MergeSort(pa, begin, mid, tmp);
	_MergeSort(pa, mid + 1, end, tmp);

	int i = begin;
	int begin1 = begin;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = end;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (pa[begin1] <= pa[begin2])
			tmp[i++] = pa[begin1++];
		else
			tmp[i++] = pa[begin2++];
	}

	while (begin1 <= end1)
		tmp[i++] = pa[begin1++];
	while (begin2 <= end2)
		tmp[i++] = pa[begin2++];

	memcpy(pa + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

void MergeSort(int* pa, int n)	//归并排序递归版
{
	assert(pa);

	int* tmp = (int*)malloc(sizeof(int) * n);	//辅助空间
	assert(tmp);

	_MergeSort(pa, 0, n - 1, tmp);	//归并主程序

	free(tmp);
	tmp = NULL;
}

void MergeSortNonR(int* pa, int n)	//归并排序非递归版
{
	assert(pa);

	int* tmp = (int*)malloc(sizeof(int) * n);
	assert(tmp);

	int rangeN = 1;	//步长、间隔，从1开始，0无意义

	while (rangeN < n)
	{
		for (int i = 0; i < n; i += (rangeN * 2))
		{
			int begin1 = i;
			int end1 = i + rangeN - 1;
			int begin2 = i + rangeN;
			int end2 = i + rangeN * 2 - 1;

			//考虑边界问题
			if (end1 >= n)
				break;
			else if (begin2 >= n)
				break;
			else if (end2 >= n)
				end2 = n - 1;

			int j = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (pa[begin1] <= pa[begin2])
					tmp[j++] = pa[begin1++];
				else
					tmp[j++] = pa[begin2++];
			}

			while (begin1 <= end1)
				tmp[j++] = pa[begin1++];
			while (begin2 <= end2)
				tmp[j++] = pa[begin2++];

			//拷贝
			memcpy(pa + i, tmp + i, sizeof(int) * (end2 - i + 1));
		}

		rangeN *= 2;
	}

	free(tmp);
	tmp = NULL;
}

void CountSort(int* pa, int n)	//计数排序
{
	assert(pa);

	//核心思想：映射
	int max = pa[0];
	int min = pa[0];
	for (int i = 0; i < n; i++)
	{
		max < pa[i] ? max = pa[i] : 1;
		min > pa[i] ? min = pa[i] : 1;
	}

	//开空间
	//如果出现 1 2 2 2 2 2 3，这种情况，空间就需要大一点
	int size = (max - min + 1) < n ? n : (max - min + 1);

	int* mapSpace = (int*)malloc(sizeof(int) * size);
	assert(mapSpace);

	memset(mapSpace, 0, sizeof(int) * size);

	//将原数组中值的出现次数，映射到新空间中（相对映射）
	for (int i = 0; i < n; i++)
		mapSpace[pa[i] - min] += 1;

	//取值，放至原数组中
	int j = 0;
	for (int i = 0; i < size; i++)
	{
		while (mapSpace[i]--)
		{
			pa[j++] = i + min;
		}
	}

	free(mapSpace);
	mapSpace = NULL;
}

int main()
{
	int arr[] = { 1,3,5,3,7,5,8,2,99,12,4 };
	int len = sizeof(arr) / sizeof(arr[0]);

	for (int i = 0; i < len; i++)
		printf("%d ", arr[i]);

	//MergeSort(arr, len);
	//MergeSortNonR(arr, len);
	CountSort(arr, len);

	//printf("\n这是归并递归版\n");
	//printf("\n这是归并非递归版\n");
	printf("\n这是计数排序\n");

	for (int i = 0; i < len; i++)
		printf("%d ", arr[i]);

	return 0;
}