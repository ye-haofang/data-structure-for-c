#define _CRT_SECURE_NO_WARNINGS 1	
#include"Heap.h"

void AdjustDown(HPDataType* pa, int len, int parent);
void HeapCreate(HP* php, HPDataType* a, int n)	//create 初始化
{
	assert(php);

	//先为堆申请一块足够大的空间
	HPDataType* tmp = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (!tmp)
	{
		perror("malloc fail");
		exit(-1);
	}

	php->a = tmp;

	//把数组a中的数据拷贝到堆中
	memcpy(php->a, a, sizeof(HPDataType) * n);
	php->size = php->capacity = n;

	//建堆算法
	//从下面开始往上建堆
	int i = (n - 1) / 2;
	for (; i >= 0; i--)
	{
		AdjustDown(php->a, n, i);
	}
}

void HeapInit(HP* php)
{
	assert(php);

	php->a = NULL;
	php->size = php->capacity = 0;	//直接初始化
}

void HeapDestroy(HP* php)
{
	assert(php);

	free(php->a);
	php->a = NULL;
	php->size = php->capacity = 0;
}

void HeapPrint(HP* php)
{
	assert(php);

	int i = 0;
	while (i < php->size)
	{
		printf("%d ", php->a[i++]);
	}

	printf("\n");
}

void Swap(HPDataType* n1, HPDataType* n2)
{
	assert(n1 && n2);

	HPDataType tmp = *n1;
	*n1 = *n2;
	*n2 = tmp;
}

void AdjustUp(HPDataType* pa, int child)	//向上调整_小堆版
{
	assert(pa);

	//向上比较调整
	//如果当前孩子小于父亲，就变换
	while (child > 0)
	{
		int parent = (child - 1) / 2;	//找到父亲位
		if (pa[child] < pa[parent])
		{
			Swap(&pa[child], &pa[parent]);	//交换
			child = parent;	//孩子跑到父亲位
		}
		else
		{
			break;
		}
	}
}

//void AdjustUp(HPDataType* pa, int child)	//向上调整_大堆版
//{
//	assert(pa);
//
//	//向上比较调整
//	//如果当前孩子大于父亲，就变换
//	while (child > 0)
//	{
//		int parent = (child - 1) / 2;	//找到父亲位
//		if (pa[child] < pa[parent])
//		{
//			Swap(&pa[child], &pa[parent]);	//交换
//			child = parent;	//孩子跑到父亲位
//		}
//		else
//		{
//			break;
//		}
//	}
//}

void HeadPush(HP* php, HPDataType x)
{
	assert(php);

	if (php->size == php->capacity)
	{
		int newCapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->a, sizeof(HPDataType) * newCapacity);
		if (!tmp)
		{
			perror("realloc faie");
			exit(-1);
		}

		php->a = tmp;
		php->capacity = newCapacity;
	}

	php->a[php->size++] = x;

	AdjustUp(php->a, php->size - 1);	//向上调整
}

void AdjustDown(HPDataType* pa, int len, int parent)	//向下调整_小堆版
{

	assert(pa);

	//如果父亲大于小孩子，就变换
	int child = parent * 2 + 1;	//默认小孩子为左孩子
	while (child < len)
	{
		child = child + 1 < len && pa[child] < pa[child + 1] ? child : child + 1;	//确认小孩子
		if (pa[child] < pa[parent])
		{
			Swap(&pa[parent], &pa[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

//void AdjustDown(HPDataType* pa, int len, int parent)	//向下调整_大堆版
//{
//
//	assert(pa);
//
//	//如果父亲小于大孩子，就变换
//	int child = parent * 2 + 1;	//默认大孩子为左孩子
//	while (child < len)
//	{
//		child = pa[child] < pa[child + 1] && child + 1 < len ? child + 1 : child;	//确认大孩子
//		if (pa[parent] < pa[child])
//		{
//			Swap(&pa[parent], &pa[child]);
//			parent = child;
//			child = parent * 2 + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}

void HeadPop(HP* php)
{
	assert(php);
	assert(!HeadEmpty(php));	//堆空，不删除

	//交换根和最后一个节点
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;	//删除

	AdjustDown(php->a, php->size, 0);	//向下调整
}

HPDataType HeapTop(HP* php)
{
	assert(php);
	assert(!HeadEmpty(php));

	return php->a[0];
}

size_t HeapSize(HP* php)
{
	assert(php);

	return php->size;
}

bool HeadEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}

//void PrintTopK(int* a, int len, int k)	//TopK问题_小堆版
//{
//	//传入数组，构建堆，根据参数k，打印前k个数
//	HP TopK;	//属于TopK的堆
//	HeapCreate(&TopK, a, len);
//
//	//现在是小堆，因此取的是最小的k个数
//	while (k--)
//	{
//		printf("%d ", HeapTop(&TopK));
//		HeadPop(&TopK);
//	}
//
//	//销毁堆
//	HeapDestroy(&TopK);
//}

//void PrintTopK(int* a, int len, int k)	//TopK问题_大堆版
//{
//	//传入数组，构建堆，根据参数k，打印前k个数
//	HP TopK;	//属于TopK的堆
//	HeapCreate(&TopK, a, len);
//	
//	//这里是大根堆，因此取的是最大的k个数
//	while (k--)
//	{
//		printf("%d ", HeapTop(&TopK));
//		HeadPop(&TopK);
//	}
//
//	//销毁堆
//	HeapDestroy(&TopK);
//}

/*
* TopK 问题，解法二，空间有保障
*/

//求最大的k个，建小堆（前k个数的小堆）
//遍历数据，将比小堆大的数据入堆，沉底
//遍历结束后，小堆中得到的就是最大的前k个数
//优点：消耗空间很小
void PrintTopK(int* a, int len, int k)
{
	//建大小为k的小堆
	HP min;
	HeapCreate(&min, a, k);

	int i = 0;
	while (i < len)
	{
		int val = HeapTop(&min);
		if (a[i] > val)
		{
			min.a[0] = a[i];
			AdjustDown(min.a, k, 0);	//向下调整
		}
		i++;
	}

	HeapPrint(&min);

	HeapDestroy(&min);
}