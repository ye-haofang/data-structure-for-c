#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

typedef int HPDataType;

typedef struct HeapInfo
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HeapInit(HP* php);	
void HeapCreate(HP* php, HPDataType* a, int n);	//create ��ʼ��
void HeapDestroy(HP* php);
void HeapPrint(HP* php);

void HeadPush(HP* php, HPDataType x);
void HeadPop(HP* php);

HPDataType HeapTop(HP* php);

size_t HeapSize(HP* php);
bool HeadEmpty(HP* php);

void PrintTopK(int* a, int len, int k);	//TopK����