#define _CRT_SECURE_NO_WARNINGS 1	
#include"Heap.h"

void TestDemo1()
{
	HP H1;
	//HeapInit(&H1);

	int array[] = { 27,15,19,18,28,34,65,49,25,37 };
	int len = sizeof(array) / sizeof(array[0]);

	HeapCreate(&H1, array, len);

	//int i = 0;
	//while (i < len)
	//{
	//	HeadPush(&H1, array[i]);
	//	i++;
	//}

	HeapPrint(&H1);

	while (!HeadEmpty(&H1))
	{
		printf("%d ", HeapTop(&H1));
		HeadPop(&H1);
	}

	//int k = 5;
	//while (k--)
	//{
	//	printf("%d ", HeapTop(&H1));
	//	HeadPop(&H1);
	//}

	HeapDestroy(&H1);
}

void TestDemo2()
{
	//这是大堆
	int arr[] = { 1,2,3,34,43,5,346,5,64,52,34,22,31 };
	printf("数组arr的前5个数依次为: ");
	PrintTopK(arr, sizeof(arr) / sizeof(arr[0]), 5);
}

void TestDemo3()
{
	//这是小堆
	int arr[] = { 1,2,3,34,43,5,346,5,64,52,34,22,31 };
	printf("数组arr的后5个数依次为: ");
	PrintTopK(arr, sizeof(arr) / sizeof(arr[0]), 5);
}

int main()
{
	TestDemo3();
	return 0;
}