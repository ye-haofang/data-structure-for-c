#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

//面试题 01.03. URL化

char* replaceSpaces(char* S, int length) {
    //倒着走，避免过多重复操作

    //先统计空格数
    int spCount = 0;
    char* str = S;
    int n = length--;
    while (n--)
    {
        if (isspace(*str))
            spCount++;
        str++;
    }

    //一个空格多占两位
    spCount *= 2;
    char* pend = S + length + spCount;  //移动至新字符串的最后一个位置
    *(pend + 1) = '\0'; //防止题目搞事情

    //开始判断，双指针安排
    char* begin = S;    //起始位置
    char* prev = S + length;    //真实字符串的最后一个位置
    while (prev >= begin)
    {
        //如果指针 prev 指向空白字符
        if (isspace(*prev))
        {
            *pend-- = '0';
            *pend-- = '2';
            *pend-- = '%';
        }
        else
            *pend-- = *prev;    //正常替换

        prev--;
    }

    return pend + 1;
}

int main()
{
    char pa[] = "               ";
    replaceSpaces(pa, 5);
    return 0;
}