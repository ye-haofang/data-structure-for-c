#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//1480. 一维数组的动态和

//解法一，重新开辟数组进行计算
int* runningSum(int* nums, int numsSize, int* returnSize) {
    int* tmp = (int*)calloc(numsSize, sizeof(int));
    assert(tmp);

    int k = 0;
    int sum = 0;
    for (int i = 0; i < numsSize; i++)
    {
        sum += nums[i];
        tmp[k++] = sum;
    }

    *returnSize = numsSize;

    return tmp;
}

//1480. 一维数组的动态和

//解法二，侵入式编程
int* runningSum(int* nums, int numsSize, int* returnSize) {
    int k = 0;
    int sum = 0;
    for (int i = 0; i < numsSize; i++)
    {
        sum += nums[i];
        nums[i] = sum;
    }

    *returnSize = numsSize;

    return nums;
}

int main()
{
    int arr[] = { 1,1,1,1,1 };
    int pos = 0;
    runningSum(arr, 5, &pos);
    return 0;
}