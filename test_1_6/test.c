#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//33. 搜索旋转排序数组

int search(int* nums, int numsSize, int target) {
    int left = 0;
    int right = numsSize - 1;
    //int prev = (left + right) / 2;    //本来还想优化，结果题目搞事情
    int prev = left;
    int cur = prev;
    if (prev + 1 <= right)
        cur = prev + 1;

    //获取旋转点
    while (prev < right && nums[prev] < nums[cur])
    {
        prev++;
        cur++;
    }

    int left1 = left;
    int right1 = prev;
    int left2 = cur;
    int right2 = right;
    int mid1, mid2;

    //判断 target 属于哪个有序区间
    if (target >= nums[left1])
    {
        while (left1 <= right1)
        {
            mid1 = (left1 + right1) / 2;
            if (nums[mid1] > target)
                right1 = mid1 - 1;
            else if (nums[mid1] < target)
                left1 = mid1 + 1;
            else
                return mid1;
        }
    }
    else
    {
        while (left2 <= right2)
        {
            mid2 = (left2 + right2) / 2;
            if (nums[mid2] > target)
                right2 = mid2 - 1;
            else if (nums[mid2] < target)
                left2 = mid2 + 1;
            else
                return mid2;
        }
    }

    return -1;
}
int main()
{
    int arr[] = { 7,8,1,2,3,4,5,6 };
    search(arr, sizeof(arr) / sizeof(arr[0]), 2);
    return 0;
}