#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

////法一，未过
//long long getFacVal(int n)
//{
//	if (n == 1)
//		return 1;
//
//	return n * getFacVal(n - 1);
//}
//
////面试题 16.05. 阶乘尾数
//int trailingZeroes(int n) {
//    //统计2的倍数和5的倍数，进行拆分
//    int numTwo = 0; //统计二的个数
//    int numFive = 0;    //统计五的个数
//    for (int i = 2; i <= n; i += 2)
//    {
//        int tmp = i;
//        while (tmp % 2 == 0)
//        {
//            numTwo++;
//            tmp /= 2;
//        }
//    }
//
//    for (int i = 5; i <= n; i += 5)
//    {
//        int tmp = i;
//        while (tmp % 5 == 0)
//        {
//            numFive++;
//            tmp /= 5;
//        }
//    }
//
//    int min = numTwo;
//    if (numFive < min)
//        min = numFive;
//
//    return min;
//}

////法二，没过，复杂了
////面试题 16.05. 阶乘尾数
//
//int trailingZeroes(int n) {
//    //统计五出现的个数
//    int facNum = 1;
//    int numFive = 0;
//    int mul_5 = 2;
//    int mul_10 = 1;
//    for (int i = 5; i <= n; i = facNum * 5)
//    {
//        if (facNum / (int)(pow(10.0, (double)mul_10)) == 1)
//            mul_10++;
//
//        if (facNum % 10 == 5)
//            numFive += mul_5++;  //5的倍数
//        else if (facNum % 10 == 0)
//            numFive += mul_10;  //10的倍数
//        else
//            numFive++;   //常规加1
//
//        facNum++;
//    }
//    return numFive;
//}

//面试题 16.05. 阶乘尾数

int trailingZeroes(int n) {
    //统计五出现的次数
    int numFive = 0;
    while (n)
    {
        n /= 5;
        numFive += n;
    }

    return numFive;
}

int main()
{
	//for (int i = 30; i > 0; i--)
	//	printf("%-5d | %lld\n", i, getFacVal(i));
    printf("%d\n", trailingZeroes(2133344234));
	return 0;
}