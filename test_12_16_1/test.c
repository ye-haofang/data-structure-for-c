#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>

//1805. 字符串中不同整数的数目
int numDifferentIntegers(char* word) {
    //满足条件：前一个不为空格，当前为空格，计数+1
    int count = 0;
    char* str = word;
    while (*str)
    {
        if (isalpha(*str))
            *str = ' ';
        str++;
    }

    str = word; //这是前一个
    char* tmp = str + 1;    //这是后一个
    while (*tmp)
    {
        if (isdigit(*str) && *tmp == ' ')
            count++;
        str++;
        tmp++;
    }

    if (isdigit(*str))
        count++;

    return count;
}

int main()
{
    char arr[] = "a123bc34d8ef34";
    int n = numDifferentIntegers(arr);
    printf("%d\n", n);
    return 0;
}

//1796. 字符串中第二大的数字

void getMid(int* pMin, int* pMid, int* pMax)   //调整
{
    if (*pMin > *pMid)
    {
        int tmp = *pMin;
        *pMin = *pMid;
        *pMid = tmp;
        if (*pMid > *pMax)
        {
            int tmp = *pMid;
            *pMid = *pMax;
            *pMax = tmp;
        }
    }
    else if (*pMin > *pMax)
    {
        int tmp = *pMin;
        *pMin = *pMax;
        *pMax = tmp;
        if (*pMid > *pMax)
        {
            int tmp = *pMid;
            *pMid = *pMax;
            *pMax = tmp;
        }
    }
}

int secondHighest(char* s) {
    int min = -1;
    int mid = -1;
    int max = -1;
    char* str = s;

    while (*str)
    {
        if (isdigit(*str))
        {
            int num = *str - '0';
            if (num != min && num != mid && num != max)
            {
                min = num;
                getMid(&min, &mid, &max);
            }
        }
        str++;
    }

    if (min < mid && mid < max)
        return mid;
    else
        return -1;
}