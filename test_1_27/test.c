#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

////法一，排序比较，失败
////2309. 兼具大小写的最好英文字母
////https://leetcode.cn/problems/greatest-english-letter-in-upper-and-lower-case/
//
//int cmp(const void* e1, const void* e2)
//{
//    return *(char*)e1 - *(char*)e2;
//}
//
//char* greatestLetter(char* s) {
//    char* ps = s;
//    qsort(s, strlen(s), sizeof(char), cmp);
//
//    char* pChar = (char*)calloc(2, sizeof(2));
//    assert(pChar);
//
//    while (*(ps + 1))
//    {
//        if (*(ps + 1) - *ps == 32)
//            pChar[0] = *ps;
//
//        ps++;
//    }
//
//    return pChar;
//}

////法二，映射比较，通过
//2309. 兼具大小写的最好英文字母
//https://leetcode.cn/problems/greatest-english-letter-in-upper-and-lower-case/

char* greatestLetter(char* s) {
    //映射比较
    //26个大写字母+26个小写字母+6个中间
    int* tmp = (int*)calloc(58, sizeof(int));
    assert(tmp);

    char* ps = s;
    while (*ps)
    {
        tmp[*ps - 'A']++;
        ps++;
    }

    char* pChar = (char*)calloc(2, sizeof(char));
    assert(pChar);

    int i = 0;
    while (i < 26)
    {
        //如果两者和大于1，说明小写大写都出现过，需要加判断，防止失衡的情况
        if ((tmp[i] + tmp[i + 32]) > 1 && tmp[i] != 0 && tmp[i + 32] != 0)
            pChar[0] = (char)(i + 'A');
        i++;
    }

    free(tmp);
    tmp = NULL;

    return pChar;
}

int main()
{
    char arr[] = "lEeTcOdE";
    greatestLetter(arr);
    return 0;
}