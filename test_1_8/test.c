#define _CRT_SECURE_NO_WARNINGS 1

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

//一遍过
 //面试题 02.05. 链表求和

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    //常规思路，直接两个链表对应位相加
    struct ListNode* tmp1 = l1;
    struct ListNode* tmp2 = l2;
    struct ListNode* cur = NULL;    //用来保存链表1的前一个节点

    //进位保存即可
    int rouVal = 0; //进位值
    while (tmp1 && tmp2)
    {
        int num = tmp1->val + tmp2->val;
        num += rouVal;
        rouVal = 0;
        if (num > 9)
        {
            num %= 10;
            rouVal = 1;
        }
        tmp1->val = num;

        cur = tmp1;
        tmp1 = tmp1->next;
        tmp2 = tmp2->next;
    }
    if (tmp1 == NULL)
        cur->next = tmp2;   //直接改链

    //继续判断是否需要进位
    tmp1 = cur->next;
    while (tmp1)
    {
        int num = tmp1->val + rouVal;
        rouVal = 0; //用完就置0
        if (num > 9)
        {
            num %= 10;
            rouVal = 1;
        }
        tmp1->val = num;

        cur = tmp1;
        tmp1 = tmp1->next;
    }

    //判断是否需要补充节点
    if (rouVal)
    {
        struct ListNode* node = (struct ListNode*)malloc(sizeof(struct ListNode) * 1);
        assert(node);

        node->next = NULL;
        node->val = rouVal;

        cur->next = node;   //链接上补充节点
    }

    return l1;
}