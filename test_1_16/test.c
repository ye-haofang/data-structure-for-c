#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

 //1122. 数组的相对排序
 //https://leetcode.cn/problems/relative-sort-array/

int* relativeSortArray(int* arr1, int arr1Size, int* arr2, int arr2Size, int* returnSize) {
    //映射
    int max = arr1[0];
    int min = arr1[0];
    for (int i = 0; i < arr1Size; i++)
    {
        if (max < arr1[i])
            max = arr1[i];

        if (min > arr1[i])
            min = arr1[i];
    }

    int len = max - min + 1;
    int* map = (int*)calloc(len, sizeof(int));
    assert(map);

    int* mapForArr2 = (int*)calloc(arr2Size, sizeof(int));
    assert(mapForArr2);

    for (int i = 0; i < arr1Size; i++)
    {
        bool flag = true;
        //相对位置数累计
        for (int j = 0; j < arr2Size; j++)
        {
            if (arr1[i] == arr2[j])
            {
                mapForArr2[j]++;
                flag = false;
                break;
            }
        }

        //当arr1[i]未出现在arr2中时
        if (flag)
            map[arr1[i] - min]++;
    }

    //先把相对顺序的值植入arr1中
    int pos = 0;
    for (int i = 0; i < arr2Size; i++)
    {
        while (mapForArr2[i])
        {
            arr1[pos++] = arr2[i];
            mapForArr2[i]--;
        }
    }

    //再把映射的顺序植入arr1
    for (int i = 0; i < len; i++)
    {
        while (map[i])
        {
            arr1[pos++] = i + min;
            map[i]--;
        }
    }

    *returnSize = arr1Size;
    free(map);
    map = NULL;
    free(mapForArr2);
    mapForArr2 = NULL;

    return arr1;
}

int main()
{
    int arr1[] = { 28, 6, 22, 8, 44, 17 };
    int arr2[] = { 22, 28, 8, 6 };
    int len = 0;
    relativeSortArray(arr1, sizeof(arr1) / sizeof(arr1[0]), arr2, sizeof(arr2) / sizeof(arr2[0]), &len);
    return 0;
}