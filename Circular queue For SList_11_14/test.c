#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdbool.h>
#include<assert.h>
#include<stdlib.h>
//双向循环链表实现循环队列
typedef int SListDataType;

typedef struct SListNode
{
    SListDataType data;
    struct SListNode* next; //指向下一个节点
    struct SListNode* prev; //指向上一个节点
}SLNode;

typedef struct {
    SLNode* head;
    SLNode* tail;
    int len;    //长度
} MyCircularQueue;

SLNode* buyNode(void)
{
    SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));
    assert(newnode);

    newnode->next = newnode->prev = NULL;
    return newnode;
}
void SListPush(MyCircularQueue* obj, int k)
{
    obj->len = k;   //真实可用长度
    while (k--)
    {
        SLNode* newnode = buyNode();
        if (obj->tail == NULL)
        {
            //如果尾为空，为第一次插入，需要把头尾指针的指向处理一下
            obj->head = obj->tail = newnode;
            obj->head->next = obj->tail->next = newnode;
            obj->head->prev = obj->tail->prev = newnode;
        }
        else
        {
            obj->tail->next = newnode;
            newnode->prev = obj->tail;
            obj->tail = newnode;
        }
    }

    obj->tail->next = obj->head;
    obj->head->prev = obj->tail;    //闭环

    obj->tail = obj->head;  //头尾相遇
}

MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    assert(obj);

    obj->head = obj->tail = NULL;
    SListPush(obj, k + 1);    //多申请一个

    return obj;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj);
bool myCircularQueueIsFull(MyCircularQueue* obj);

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    assert(obj);
    //满了，是入不了队的
    if (myCircularQueueIsFull(obj))
        return false;

    //关于入队：即链表的尾插
    obj->tail->data = value;
    obj->tail = obj->tail->next;    //尾节点向后移动
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    assert(obj);
    //空了，是出不了队的
    if (myCircularQueueIsEmpty(obj))
        return false;

    //关于出队: 头节点向后移动，就行了，有效数据为[头，尾]
    obj->head = obj->head->next;
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    //队头元素，就是头节点的数据域
    return obj->head->data;
}

int myCircularQueueRear(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    //队尾元素，就是尾节点的上一个节点数据域
    return (obj->tail->prev)->data;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    assert(obj);
    //头尾相遇，链表为空

    return obj->head == obj->tail;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    assert(obj);
    //如果尾的下一个节点等于头节点，就说明满了

    SLNode* tmp = obj->tail->next;
    return tmp == obj->head;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    assert(obj);

    //关于释放：此时需要用到长度，控制，释放完整个链表
    int len = obj->len;
    while (len--)
    {
        SLNode* cur = obj->tail;
        obj->tail = obj->tail->next;

        free(cur);
    }

    obj->head = obj->tail = NULL;
    obj->len = 0;
    free(obj);
    obj = NULL;
}
/*
["MyCircularQueue","enQueue","enQueue","enQueue","enQueue","Rear","isFull","deQueue","enQueue","Rear"]
[[3],[1],[2],[3],[4],[],[],[],[4],[]]
*/
int main()
{
    int a = 0;
    bool b = true;
    MyCircularQueue* M1 = myCircularQueueCreate(3);

    b = myCircularQueueEnQueue(M1, 1);
    b = myCircularQueueEnQueue(M1, 2);
    b = myCircularQueueEnQueue(M1, 3);
    b = myCircularQueueEnQueue(M1, 4);

    a = myCircularQueueRear(M1);
    
    b = myCircularQueueIsFull(M1);

    b = myCircularQueueDeQueue(M1);

    b = myCircularQueueEnQueue(M1, 4);

    a = myCircularQueueRear(M1);
    return 0;
}