#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

int maxDepth(char* s) {
    //思路:直接数左括号出现的最大数，就行了，出现右括号会把左括号抵消
    int leftPar = 0;    //统计左括号
    int maxDepth = 0;   //最大深度
    int depth = 0;  //深度
    char* ps = s;
    while (*ps)
    {
        if (*ps == '(')
            leftPar++;

        if (*ps == ')' && leftPar != 0)
            //出现右括号，抵消
            leftPar--;

        depth = leftPar;  //赋值

        depth > maxDepth ? maxDepth = depth : 1;    //取大的值

        ps++;
    }

    return maxDepth;
}

int main()
{
    char arr[] = "(1+(2*3)+((8)/4))+1";
    printf("%d\n", maxDepth(arr));
    return 0;
}