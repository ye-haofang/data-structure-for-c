#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

////26.删除有序数组中的重复项
////去重算法_快慢指针
//int removeDuplicates(int* nums, int numsSize)
//{
//    int* pfast = nums;  //快指针
//    int* pslow = nums;  //慢指针
//    //通过源下标，将数组中的元素遍历一遍
//    while (numsSize--)
//    {
//        //如果快指针不等与慢指针，就执行赋值
//        if (*pfast != *pslow)
//        {
//            pslow++;    //需要先将慢指针往后走一步
//            *pslow = *pfast;    //赋值
//        }
//        pfast++;    //快指针始终在移动
//    }
//
//    return pslow - nums + 1;    //指针-指针，+1的原因是会漏掉一开始的元素
//}

//88.合并两个有序数组
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n)
{
    int i1 = m - 1; //nums1的有效尾元素下标
    int i2 = n - 1; //nums2的有效尾元素下标
    int j = m + n - 1;  //nums1的尾元素下标
    //将两个数组中的较小值，从后往前存入nums1
    while (i1 >= 0 && i2 >= 0)
    {
        //情况一
        if (nums2[i2] > nums1[i1])
        {
            nums1[j] = nums2[i2];
            j--;
            i2--;
        }
        else
        {
            //情况二
            nums1[j] = nums1[i1];
            j--;
            i1--;
        }
    }
    //如果nums2中还有数据，就需要单独处理一下
    if (i2 >= 0)
    {
        //如果nums2中还有元素没被合并，就需要单独处理一下
        while (i2 >= 0)
        {
            nums1[j] = nums2[i2];
            j--;
            i2--;
        }
    }
}