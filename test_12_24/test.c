#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);    //读取划拳轮数
    int arr[4] = { 0 };   //记录数据
    int garA = 0;
    int garB = 0;   //A和B的分数
    while (n--)
    {
        scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);
        int sum = arr[0] + arr[2];
        sum == arr[1] && sum != arr[3] ? garB++ : 1;
        sum == arr[3] && sum != arr[1] ? garA++ : 1;
    }

    printf("%d %d", garA, garB);
    return 0;
}


#include<stdio.h>

int main()
{
    int garA = 0;
    int garB = 0;   //A和B的分数
    scanf("%d %d", &garA, &garB);

    int A = garA;
    int B = garB;   //回溯

    int n = 0;
    scanf("%d", &n);    //读取划拳轮数
    int arr[4] = { 0 };   //记录数据
    while (n--)
    {
        scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);
        int sum = arr[0] + arr[2];

        if (garA >= 0 && garB >= 0)
        {
            //让题目输入完全
            sum == arr[1] ? garA-- : 1;    //A喝
            sum == arr[3] ? garB-- : 1;
        }
    }

    if (garA < 0)
    {
        printf("A\n");
        printf("%d\n", B - garB);
    }
    else
    {
        printf("B\n");
        printf("%d\n", A - garA);
    }
    return 0;
}