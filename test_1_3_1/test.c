#define _CRT_SECURE_NO_WARNINGS 1	

//HJ7 取近似值

#include <stdio.h>

int main() {
    double fnum = 0.0;
    scanf("%lf", &fnum);
    int tmp = ((int)(fnum * 10) % 10);  //取小数部分
    if (tmp >= 5)
        printf("%d\n", (int)fnum + 1);
    else
        printf("%d\n", (int)fnum);
    return 0;
}