#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

typedef struct ListNode {
	int val;
	struct ListNode* next;
}SLTNode;

////876.链表的中间节点
////暴力遍历
//struct ListNode* middleNode(struct ListNode* head)
//{
//    int count = 0;  //记录总节点数
//    struct ListNode* cur = head;    //节点委托
//    while (cur)
//    {
//        count++;
//        cur = cur->next;    //向后移动
//    }
//    int mid = count / 2;    //获取中间位
//    cur = head; //拨正
//    while (mid--)
//        cur = cur->next;    //移动到合适位置
//
//    return cur; //返回此节点
//}


////876.链表的中间节点
////快慢指针
//struct ListNode* middleNode(struct ListNode* head)
//{
//    //思路：快指针和慢指针呈两倍关系
//    struct ListNode* pfast = head;  //快指针
//    struct ListNode* plow = head;   //慢指针
//    while (pfast && (pfast->next))
//    {
//        pfast = pfast->next->next;  //快指针每次走两步
//        plow = plow->next;  //慢指针每次走一步
//    }
//
//    return plow;    //当快指针走到头后，返回慢指针
//}

////剑指Offer 链表中倒数第k个节点
////双指针
//int countVal(struct ListNode* p1, struct ListNode* p2)
//{
//    int step = 0;  //计数器
//    while (p2 != p1)
//    {
//        step++;
//        p2 = p2->next;  //如果两个指针没有遇见，就往后走
//    }
//    return step;    //返回步数
//}
//
//struct ListNode* FindKthToTail(struct ListNode* pListHead, int k)
//{
//    struct ListNode* p1 = pListHead;
//    struct ListNode* p2 = pListHead;
//    int judge = 0;  //法官，判断k的合法性
//    struct ListNode* cur = pListHead;
//    while (cur)
//    {
//        judge++;    //法官得到的是真实的节点数
//        cur = cur->next;
//    }
//    if (k > judge)
//        return NULL;    //如果k大于真实节点数，就是非法的
//    //循环思想：同k步移动
//    //首先要先让 p1 和 p2 相差 k 步
//    while (p1)
//    {
//        //p1 会向后走，直到 p1 和 p2 之间为 k 步(即两者间元素数等于k)
//        if (countVal(p1, p2) < k)
//        {
//            p1 = p1->next;
//            continue;
//        }
//        p1 = p1->next;  //现在可以一起带着 p2 往后走了
//        p2 = p2->next;
//    }
//    return p2;  //返回p2就行了
//}
//extern void SLTPushBack(SLTNode**, int*, int);

//剑指Offer 链表中倒数第k个节点
//快慢指针
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k)
{
    struct ListNode* pfast = pListHead; //快慢指针
    struct ListNode* pslow = pListHead; //原理和思路1差不多，不过简化了很多步骤
    //不得不佩服前人的智慧
    //可以通过k解决两个指针间的k步问题
    while (k--)
    {
        //如果pfast都为空了，就说k肯定是非法的
        if (!pfast)
            return NULL;    //直接返回NULL
        pfast = pfast->next;    //向后移动
    }
    //此时两个指针可以一起往后走
    while (pfast)
    {
        pfast = pfast->next;
        pslow = pslow->next;
    }

    return pslow;   //当快指针走完时，慢指针指向的就是目标节点
}

extern void SLTPushBack(SLTNode**, int*, int);

int main()
{
    int arr[] = { 1,2,3,4,5 };
    SLTNode* s = NULL;
    SLTPushBack(&s, arr, 5);
    s = FindKthToTail(s, 1);
    printf("%d\n", s->val);
    return 0;
}