#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>

//typedef struct ListNode
//{
//    int val;
//    struct ListNode* next;
//}LN;
// 
////203.移除链表元素
//struct ListNode* removeElements(struct ListNode* head, int val)
//{
//    struct ListNode* cur = head;    //记录下起始位置
//    struct ListNode* prev = NULL; //万能节点，避免头节点就是val的情况
//    while (cur)
//    {
//        struct ListNode* tail = cur->next;  //记录下一个节点信息
//        //情况1，头节点是val，且prev为空，此时需要释放头节点，然后更新头节点信息
//        if (cur->val == val && !prev)
//        {
//            free(cur);  //释放头节点
//            head = cur = tail;  //注意，head也要更新
//        }
//        else if (cur->val == val)   //情况2，节点cur是val，且prev不为空，说明prev已经指向cur的上一个节点了，直接更新
//        {
//            free(cur);  //释放中节点
//            prev->next = cur = tail;    //前节点链接后节点
//        }
//        else  //情况3，cur不是val，此时可以更新prev信息，让cur向后移动
//        {
//            prev = cur;
//            cur = tail; //tail就是cur的下一个节点，直接更新信息
//        }
//    }
//    return head;  //直接返回头节点信息，即使链表中全是val，移除后head也被置空了
//}
//
//int main()
//{
//    LN* n1 = (LN*)malloc(sizeof(LN));
//    n1->val = 7;
//    LN* n2 = (LN*)malloc(sizeof(LN));
//    n2->val = 7;
//    LN* n3 = (LN*)malloc(sizeof(LN));
//    n3->val = 7;
//    LN* n4 = (LN*)malloc(sizeof(LN));
//    n4->val = 7;
//    //LN* n5 = (LN*)malloc(sizeof(LN));
//    //n5->val = 4;
//    //LN* n6 = (LN*)malloc(sizeof(LN));
//    //n6->val = 5;
//    //LN* n7 = (LN*)malloc(sizeof(LN));
//    //n7->val = 6;
//
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    //n4->next = n5;
//    //n5->next = n6;
//    //n6->next = n7;
//    n4->next = NULL;
//
//    LN* cur = removeElements(n1, 7);
//    while (cur)
//    {
//        printf("%d->", cur->val);
//        cur = cur->next;
//    }
//    printf("NULL\n");
//
//    return 0;
//}



typedef struct ListNode 
{
    int val;
    struct ListNode* next;
}LN;

//206.翻转单链表
//思路：遍历单链表，然后挨个头插
struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* cur = head;
    struct ListNode* prev = NULL;
    struct ListNode* tail = NULL;
    while (cur)
    {
        prev = cur->next;   //翻转前要先记录下一个节点信息，不然待会就找不到了
        cur->next = tail;   //将节点链接至新链表
        tail = cur; //新链表信息更新
        cur = prev; //老链表信息更新，指向下一个节点
    }
    return tail;    //此时的tail就是新链表的头节点
}
int main()
{
    LN* n1 = (LN*)malloc(sizeof(LN));
    n1->val = 1;
    LN* n2 = (LN*)malloc(sizeof(LN));
    n2->val = 2;
    LN* n3 = (LN*)malloc(sizeof(LN));
    n3->val = 3;
    LN* n4 = (LN*)malloc(sizeof(LN));
    n4->val = 4;
    LN* n5 = (LN*)malloc(sizeof(LN));
    n5->val = 5;

    n1->next = n2;
    n2->next = n3;
    n3->next = n4;
    n4->next = n5;
    n5->next = NULL;

    printf("翻转前:\n");
    LN* cur = n1;
    while (cur)
    {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");

    cur = reverseList(n1);
    while (cur)
    {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");

    return 0;
}