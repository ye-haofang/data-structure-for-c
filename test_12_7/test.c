#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>

//力扣OJ
//912. 排序数组
//https://leetcode.cn/problems/sort-an-array/

////归并，递归版
//void MergeSort(int* pa, int begin, int end, int* tmp)
//{
//    if (begin >= end)
//        return;
//
//    int mid = (begin + end) / 2;
//    //[begin, mid] [mid + 1, end]
//
//    MergeSort(pa, begin, mid, tmp);
//    MergeSort(pa, mid + 1, end, tmp);
//
//    //核心思想为合并，即将两个有序数组合并
//    //利用后序遍历，从两个数开始进行有序合并，逐层往上，直到所有数据都有序
//    int i = begin;
//    int begin1 = begin, end1 = mid;
//    int begin2 = mid + 1, end2 = end;
//    while (begin1 <= end1 && begin2 <= end2)
//    {
//        if (pa[begin1] < pa[begin2])
//            tmp[i++] = pa[begin1++];
//        else
//            tmp[i++] = pa[begin2++];
//    }
//
//    //排除数据没有合并完的情况
//    while (begin1 <= end1)
//        tmp[i++] = pa[begin1++];
//    while (begin2 <= end2)
//        tmp[i++] = pa[begin2++];
//
//    //需要把临时数组中的值拷贝到原数组中，这里 + begin，可以提高效率
//    memcpy(pa + begin, tmp + begin, sizeof(int) * (end - begin + 1));
//}
//
//int* sortArray(int* nums, int numsSize, int* returnSize) {
//    *returnSize = numsSize;
//
//    int* tmp = (int*)malloc(sizeof(int) * numsSize);
//    assert(tmp);
//
//    MergeSort(nums, 0, numsSize - 1, tmp);
//
//    return nums;
//}

////归并，迭代版
////优化方式一，区间调整
//int* sortArray(int* nums, int numsSize, int* returnSize) {
//    *returnSize = numsSize;
//
//    int* tmp = (int*)malloc(sizeof(int) * numsSize);
//    assert(tmp);
//
//    int rangeN = 1;  //控制步长的关键变量
//
//    while (rangeN < numsSize)
//    {
//        for (int i = 0; i < numsSize; i += rangeN * 2)
//        {
//            int j = i;   //临时数组 tmp 的下标
//            int begin1 = i, end1 = i + rangeN - 1;
//            int begin2 = i + rangeN, end2 = i + rangeN * 2 - 1;
//
//            if (end1 >= numsSize)
//            {
//                end1 = numsSize - 1;
//                begin2 = numsSize;
//                end2 = numsSize - 1;
//            }
//            else if (begin2 >= numsSize)
//            {
//                begin2 = numsSize;
//                end2 = numsSize - 1;
//            }
//            else if (end2 >= numsSize)
//            {
//                end2 = numsSize - 1;
//            }
//
//            while (begin1 <= end1 && begin2 <= end2)
//            {
//                if (nums[begin1] <= nums[begin2])
//                    tmp[j++] = nums[begin1++];
//                else
//                    tmp[j++] = nums[begin2++];
//            }
//
//            while (begin1 <= end1)
//                tmp[j++] = nums[begin1++];
//            while (begin2 <= end2)
//                tmp[j++] = nums[begin2++];
//        }
//
//        memcpy(nums, tmp, sizeof(int) * numsSize);
//        rangeN *= 2; //步长扩大两倍
//    }
//
//    free(tmp);
//    tmp = NULL;
//
//    return nums;
//}

//归并，迭代版
//优化方式二，直接跳出
int* sortArray(int* nums, int numsSize, int* returnSize) {
    *returnSize = numsSize;

    int* tmp = (int*)malloc(sizeof(int) * numsSize);
    assert(tmp);

    int rangeN = 1;  //控制步长的关键变量

    while (rangeN < numsSize)
    {
        for (int i = 0; i < numsSize; i += rangeN * 2)
        {
            int j = i;   //临时数组 tmp 的下标
            int begin1 = i, end1 = i + rangeN - 1;
            int begin2 = i + rangeN, end2 = i + rangeN * 2 - 1;

            if (end1 >= numsSize)
                break;
            else if (begin2 >= numsSize)
                break;
            else if (end2 >= numsSize)
                end2 = numsSize - 1;

            while (begin1 <= end1 && begin2 <= end2)
            {
                if (nums[begin1] <= nums[begin2])
                    tmp[j++] = nums[begin1++];
                else
                    tmp[j++] = nums[begin2++];
            }

            while (begin1 <= end1)
                tmp[j++] = nums[begin1++];
            while (begin2 <= end2)
                tmp[j++] = nums[begin2++];

            memcpy(nums + i, tmp + i, sizeof(int) * (end2 - i + 1));
        }
        rangeN *= 2; //步长扩大两倍
    }

    free(tmp);
    tmp = NULL;

    return nums;
}


int main()
{
    int arr[] = { 5,3,2,1 };
    int i = 0;
    sortArray(arr, 4, &i);

    return 0;
}