#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<windows.h>

////KMP算法
//
//void getNext(int* next, char* str, int len)
//{
//	assert(str);
//
//	next[0] = -1;	//next数组中的前两个元素可以确定
//	next[1] = 0;
//
//	int i = 2;
//	int k = 0;
//	//循环 len-2 次
//	while (i < len)
//	{
//		if (k == -1 || str[i] == str[k])
//		{
//			next[i] = k + 1;	//如果k=-1，+1后为0，说明不匹配
//			i++;	//确定next值后，才能向后移动
//          k++:
//		}
//		else
//		{
//			k = next[k];	//不相等，k就要回退
//		}
//	}
//}
//
//int KMP(char* str1, char* str2, int pos)
//{
//	assert(str1 && str2);
//
//	if (str1 && str2)
//		return -1;	//其中一个是空串，直接返回-1
//
//	int len1 = strlen(str1);
//	int len2 = strlen(str2);
//	int* next = (int*)malloc(sizeof(int) * len2);
//	assert(next);
//	
//	if (pos < 0 || pos >= len2)
//		return -1;	//如果pos非法，返回-1
//
//	getNext(next, str2, len2);
//
//	int i = 0;
//	int j = 0;
//	while(i < len1 && j < len2)
//	{
//		if (str1[i] != str2[j])
//		{
//			j = next[j];	//j 回退到特定位置
//		}
//		j++;
//		i++;
//		if (j == len2)
//			return i - j;
//	}
//	return -1;
//}
//
//int main()
//{
//	double s1, e1, s2, e2;
//	printf("%d\n", KMP("abcdefg", "def", 0));	//3
//	printf("%d\n", KMP("abcdefg", "bac", 0));	//-1
//	printf("%d\n", KMP("abcdefg", "a", 0));	//0
//	s1 = (double)clock();
//	Sleep(1000);
//	printf("%d\n", KMP("abcdeaskjhacooushaicoacbaciacibIZxiXNaosihcsdaojcoacnosajxjxdacnioanabcaxfg", "aojcoacnosajxj", 0));	
//	e1 = (double)clock();
//
//	s2 = (double)clock();
//	Sleep(1000);
//	printf("%s\n", strstr("abcdeaskjhacooushaicoacbaciacibIZxiXNaosihcsdaojcoacnosajxjxdacnioanabcaxfg", "aojcoacnosajxj"));
//	e2 = (double)clock();
//
//	printf("KMP运行时间为:%.5f\n", e1 - s1);
//	printf("strstr运行时间为:%.5f\n", e2 - s2);
//
//	return 0;
//}


//KMP算法
//next数组优化版
//多处细节优化版
void getNext(int* next, char* str, int len)
{
	assert(str);

	next[0] = -1;	//next数组中的前两个元素可以确定
	next[1] = 0;

	int i = 2;
	int k = 0;
	//循环 len-2 次
	while (i < len)
	{
		if (k == -1 || str[i-1] == str[k])
		{
			next[i] = k + 1;	//如果k=-1，+1后为0，说明不匹配
			i++;	//确定next值后，才能向后移动
			k++;
		}
		else
		{
			k = next[k];	//不相等，k就要回退
		}
	}
	i = 0;
	while (i < len)
	{
		if (str[i] == str[next[i]])
			next[i] = next[next[i]];
		i++;
	}
}

int KMP(char* str1, char* str2, int pos)
{
	assert(str1 && str2);

	if (str1 == NULL || str2 == NULL)
		return -1;	//其中一个是空串，直接返回-1

	int len1 = strlen(str1);
	int len2 = strlen(str2);
	int* next = (int*)malloc(sizeof(int) * len2);
	assert(next);

	if (pos < 0 || pos >= len2)
		return -1;	//如果pos非法，返回-1

	getNext(next, str2, len2);

	int i = 0;
	int j = 0;
	while (i < len1 && j < len2)
	{
		if (str1[i] != str2[j])
		{
			j = next[j];	//j 回退到特定位置
		}
		j++;
		i++;
		if (j == len2)
			return i - j;
	}
	return -1;
}

int main()
{
	printf("%d\n", KMP("adioascnaoincodbaoabcaabbcabcaabdabbcvodadfa", "abcaabbcabcaabdab", 0));

	return 0;
}