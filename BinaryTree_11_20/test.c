#define _CRT_SECURE_NO_WARNINGS 1	
#include"BinaryTree.h"


//二叉树更新
//提交作业
void TestBinaryTree1()
{
	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
	BTNode* F = (BTNode*)malloc(sizeof(BTNode));

	A->data = 'A';
	B->data = 'B';
	C->data = 'C';
	D->data = 'D';
	E->data = 'E';
	F->data = 'F';


	A->left = B, A->right = D;
	B->left = C, B->right = NULL;
	C->left = C->right = NULL;
	D->left = E, D->right = F;
	E->left = E->right = NULL;
	F->left = F->right = NULL;

	BTNode* root = A;

	//测试三种基本遍历
	BinaryTreePrevOrder(root);
	printf("\n");
	BinaryTreeInOrder(root);
	printf("\n");
	BinaryTreePostOrder(root);

	//printf("\n%d\n", BinaryTreeComplete(root));	//此二叉树不是完全二叉树

	BinaryTreeDestory(&root);
	A = root = NULL;
}

void TestBinaryTree2()
{
	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
	BTNode* F = (BTNode*)malloc(sizeof(BTNode));
	BTNode* X = (BTNode*)malloc(sizeof(BTNode));

	A->data = 'A';
	B->data = 'B';
	C->data = 'C';
	D->data = 'D';
	E->data = 'E';
	F->data = 'F';
	X->data = 'X';

	A->left = B, A->right = D;
	B->left = C, B->right = NULL;
	C->left = X;
	X->left = X->right = NULL;
	C->right = NULL;
	D->left = E, D->right = F;
	E->left = E->right = NULL;
	F->left = F->right = NULL;

	BTNode* root = A;

	//测试计算节点树、叶子节点树、k层节点数、查找值为k的节点
	printf("节点数：%d\n",BinaryTreeSize(root));
	printf("叶子节点数：%d\n", BinaryTreeLeafSize(root));
	printf("树的深度为：%d\n", BinaryTreeDepth(root));
	printf("k层节点数：%d\n", BinaryTreeLevelKSize(root, 3));
	printf("目标节点为：%p\n参考值：%p\n", BinaryTreeFind(root, 'E'), E);

	printf("\n是否为完全二叉树: %d\n", BinaryTreeComplete(root));	//此二叉树不是完全二叉树

	BinaryTreeDestory(&root);
	A = root = NULL;
}

void TestBinaryTree3()
{
	BTDataType arr[] = "ABD##E#H##CF##G##";
	int n = strlen(arr);	//长度
	int i = 0;

	BTNode* root = BinaryTreeCreate(arr, n, &i);	//通过前序遍历创建树


	//测试三种基本遍历
	BinaryTreePrevOrder(root);
	printf("\n");
	BinaryTreeInOrder(root);
	printf("\n");
	BinaryTreePostOrder(root);
	printf("\n");

	//测试计算节点树、叶子节点树、k层节点数、查找值为k的节点
	printf("节点数：%d\n", BinaryTreeSize(root));
	printf("叶子节点数：%d\n", BinaryTreeLeafSize(root));
	printf("树的深度为：%d\n", BinaryTreeDepth(root));
	printf("k层节点数：%d\n", BinaryTreeLevelKSize(root, 3));
	printf("目标节点为：%p\n", BinaryTreeFind(root, 'E'));

	BinaryTreeDestory(&root);
}

void TestBinaryTree4()
{
	//BTDataType arr[] = "ABD##E##CF##G##";	//完全二叉树，过
	//BTDataType arr[] = "ABD##E#H##CF##G##";	//非完全二叉树，过
	BTDataType arr[] = "ABD##E#H##C##";	//非完全二叉树
	int n = strlen(arr);	//长度
	int i = 0;

	BTNode* root = BinaryTreeCreate(arr, n, &i);	//通过前序遍历创建树

	BinaryTreeLevelOrder(root);

	printf("\n是否为完全二叉树: %d\n", BinaryTreeComplete(root));

	BinaryTreeDestory(&root);
}


//bool isUnivalTree(BTNode* root) {
//	if (root == NULL)
//		return true;
//
//	int val = root->data;
//
//	int valLeft = isUnivalTree(root->left);
//	int valRight = isUnivalTree(root->right);
//
//	return valLeft == valRight;
//}
//void TestOJ()	//没过
//{
//	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
//	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
//	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
//	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
//	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
//
//	A->data = 2;
//	B->data = 2;
//	C->data = 2;
//	D->data = 5;
//	E->data = 2;
//
//	A->left = B, A->right = D;
//	B->left = C, B->right = NULL;
//	C->left = C->right = NULL;
//	C->right = NULL;
//	D->left = E, D->right = NULL;
//	E->left = E->right = NULL;
//
//	printf("%d\n", isUnivalTree(A));
//}

int main()
{
	TestBinaryTree4();
	//TestOJ();
	return 0;
}