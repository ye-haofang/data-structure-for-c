#define _CRT_SECURE_NO_WARNINGS 1
#include"BinaryTree.h"
#include"Queue.h"

// 通过前序遍历的数组" A B D # # E # H # # C F # # G # # " 构建二叉树
BTNode* BinaryTreeCreate(BTDataType* pc, int n, int* pi)
{
    assert(pc);

    if (pc[*pi] == '#')
    {
        (*pi)++;
        return NULL;
    }

    BTNode* newnode = (BTNode*)malloc(sizeof(BTNode));
    if (!newnode)
    {
        perror("malloc::fail");
        exit(-1);
    }
    newnode->data = pc[(*pi)++]; //可能有bug
	n--;	//这个n好像没啥用，假设是数组的大小，那么每成功创建一个就-1

    newnode->left = BinaryTreeCreate(pc, n, pi);
    newnode->right = BinaryTreeCreate(pc, n, pi);

    return newnode;
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	assert(root);

	//二叉树销毁：后序遍历递归销毁
	if (*root == NULL)
		return;

	BinaryTreeDestory(&(*root)->left);
	BinaryTreeDestory(&(*root)->right);

	free((*root));
	*root = NULL;
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	//根节点+左右节点
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	//只有左右孩子都为空的节点，才是叶子节点
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}

	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

//二叉树的深度
int BinaryTreeDepth(BTNode* root)
{
	if (root == NULL)
		return 0;

	int leftLen = BinaryTreeDepth(root->left);
	int rightLen = BinaryTreeDepth(root->right);

	return 1 + (leftLen > rightLen ? leftLen : rightLen);
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}

	if (k == 1)
	{
		return 1;
	}

	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}

	if (root->data == x)
	{
		return root;	//找到了
	}

	BTNode* pleft = BinaryTreeFind(root->left, x);
	BTNode* pright = BinaryTreeFind(root->right, x);

	if (pleft)
		return pleft;
	else if (pright)
		return pright;
	else
		return NULL;
}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	 //前序：根左右
	printf("%c ", root->data);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}

// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	//中序：左根右
	BinaryTreeInOrder(root->left);
	printf("%c ", root->data);
	BinaryTreeInOrder(root->right);
}

// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	//后序：左右根
	BinaryTreePostOrder(root->left);
	BinaryTreePostOrder(root->right);
	printf("%c ", root->data);
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	//需要用到队列
	//思想：根入队，出队后带下一层入队，如此重复，直到队空
	Queue Q;
	QueueInit(&Q);

	if (root)
		QueuePush(&Q, root);	//首先把根节点入队
	
	while (!QueueEmpty(&Q))
	{
		BTNode* tmp = QueueFront(&Q);
		printf("%c ", tmp->data);
		QueuePop(&Q);

		if (tmp->left)
			QueuePush(&Q, tmp->left);

		if (tmp->right)
			QueuePush(&Q, tmp->right);
	}

	QueueDestroy(&Q);
}

//判断是否为完全二叉树
bool BinaryTreeComplete(BTNode* root) 
{
	Queue Q;
	QueueInit(&Q);

	if (root)
		QueuePush(&Q, root);	//首先把根节点入队
	else
		return true;
	int treeSize = BinaryTreeSize(root);

	BTDataType* pc = (BTDataType*)calloc(sizeof(BTDataType), treeSize + 1);
	if (!pc)
	{
		perror("calloc::fail");
		exit(-1);
	}

	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	assert(node);
	node->data = '\0';

	if (treeSize == 1)
		return true;

	if (!root->right)
		return false;

	int i = 0;
	while (i < treeSize)
	{
		BTNode* tmp = QueueFront(&Q);
		QueuePop(&Q);
		pc[i++] = tmp->data;

		if (tmp->left)
			QueuePush(&Q, tmp->left);
		else
			QueuePush(&Q, node);
		if (tmp->right)
			QueuePush(&Q, tmp->right);
		else
			QueuePush(&Q, node);
	}

	QueueDestroy(&Q);

	int size = strlen(pc);
	return size == treeSize;
}