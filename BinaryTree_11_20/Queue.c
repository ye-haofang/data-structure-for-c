
#define _CRT_SECURE_NO_WARNINGS 1	
#include"Queue.h"

void QueueInit(Queue* pq)	//队列的初始化
{
	assert(pq);

	//初始化状态：队头、队尾指针都指向空，队列大小为0
	pq->front = pq->rear = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)	//队列的销毁
{
	assert(pq);

	//销毁思路：利用临时指针进行销毁
	QNode* cur = pq->front;
	while (cur)
	{
		QNode* tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	pq->front = pq->rear = NULL;
	pq->size = 0;
}

//void QueuePrint(Queue* pq)	//队列的遍历打印_测试用
//{
//	assert(pq);
//
//	//遍历思想和销毁时一致，不过将销毁变成了打印
//	QNode* cur = pq->front;
//	while (cur)
//	{
//		printf("%d ", cur->data);
//		cur = cur->next;
//	}
//	printf("\n");
//}

void QueuePrint(Queue* pq)	//队列的打印_用户用
{
	assert(pq);

	//只能通过调用接口实现
	while (!QueueEmpty(pq))
	{
		printf("%d ", QueueFront(pq));
		QueuePop(pq);
	}
	printf("\n");
}

static QNode* buyNode(QListDataType x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc :: fail");
		exit(-1);
	}

	newnode->data = x;	//赋值就是新节点
	newnode->next = NULL;

	return newnode;
}

void QueuePush(Queue* pq, QListDataType x)	//入队
{
	assert(pq);

	//先买一个节点
	QNode* newnode = buyNode(x);

	//分情况：如果队头为空，说明队空，此时直接将新节点赋值给队头、队尾
	if (pq->front == NULL)
	{
		pq->front = pq->rear = newnode;
		pq->size++;
	}
	else
	{
		//否则就是将新节点，链接到队尾，然后更新队尾
		pq->rear->next = newnode;	//链接
		pq->rear = newnode;	//更新队尾
		pq->size++;
	}
}

void QueuePop(Queue* pq)	//出队
{
	assert(pq);
	assert(!QueueEmpty(pq));	//如果队空，是不能出队的

	//出队思想：有元素才能出队，更新队头，销毁原队头
	QNode* cur = pq->front;
	pq->front = pq->front->next;	//更新队头指针
	free(cur);
	cur = NULL;
	pq->size--;
}

QListDataType QueueFront(Queue* pq)	//查看队头元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->front->data;
}

QListDataType QueueRear(Queue* pq)	//查看队尾元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->rear->data;
}

int QueueSize(Queue* pq)	//当前队列的有效元素数
{
	assert(pq);

	return pq->size;
}

bool QueueEmpty(Queue* pq)	//判断当前队空情况
{
	assert(pq);

	return pq->front == NULL;
}