#define _CRT_SECURE_NO_WARNINGS 1	

//2315. 统计星号
//https://leetcode.cn/problems/count-asterisks/

int countAsterisks(char* s) {
    //只有在开关 > 0 的时候，才统计 * 号
    int button = 1;

    int n = 0;
    char* ps = s;
    while (*ps)
    {
        if (*ps == '|')
            button *= -1;   //改变状态

        if (button > 0 && *ps == '*')
            n++;    //合法统计

        ps++;
    }

    return n;
}