#pragma once
#include <iostream>
#include <vector>
#include <stack>
#include <cassert>

using namespace std;

namespace Yohifo
{

	class Sort
	{
	public:
		// 直接插入排序（扑克牌整理）
		void Insert(vector<int> arr)
		{
			cout << "void Insert(): ";
			for (size_t i = 0; i < arr.size(); i++)
			{
				int end = i - 1;
				int tmp = arr[i];
				while (end >= 0 && arr[end] > tmp)
					arr[end + 1] = arr[end], end--;
				arr[end + 1] = tmp;
			}
			Print(arr);
		}

		// 希尔排序（区间划分，减少不必要的循环）
		void Shell(vector<int> arr)
		{
			cout << "void Shell(): ";
			int gap = arr.size();
			while (gap > 1)
			{
				gap = (gap / 3) + 1;
				for (size_t i = 0; i < arr.size() - gap; i++)
				{
					int end = i;
					int tmp = arr[i + gap];

					while (end >= 0 && arr[end] > tmp)
						arr[end + gap] = arr[end], end -= gap;
					arr[end + gap] = tmp;
				}
			}
			Print(arr);
		}

		// 选择排序（选择最大值放至末尾）
		void Select(vector<int> arr)
		{
			cout << "void Select(): ";
			for (size_t i = 0; i < arr.size(); i++)
			{
				size_t n = arr.size() - i;
				int tmp = n - 1;
				for (size_t j = 0; j < n; j++)
					arr[j] > arr[tmp] ? tmp = j : 1;
				swap(arr[tmp], arr[n - 1]);
			}

			Print(arr);
		}

		// 堆排序（建大堆，取堆顶元素放至末尾）
		void AdjustDown(vector<int>& arr, int n, int parent)
		{
			int child = parent * 2 + 1;

			while (child < n)
			{
				if (child + 1 < n && arr[child] < arr[child + 1])
					child++;

				if (arr[parent] < arr[child])
				{
					swap(arr[parent], arr[child]);
					parent = child;
					child = parent * 2 + 1; // 堆排序快速的核心
				}
				else
					return;
			}
		}

		void Heap(vector<int> arr)
		{
			cout << "void Heap(): ";
			for (int i = (arr.size() - 2) / 2; i >= 0; i--)
				AdjustDown(arr, arr.size(), i);
			
			for (size_t i = 0; i < arr.size(); i++)
			{
				int n = arr.size() - i;
				swap(arr[0], arr[n - 1]);
				AdjustDown(arr, n - 1, 0);
			}
			Print(arr);
		}

		// 冒泡排序（两两交换，将最大值交换至末尾）
		void Bubble(vector<int> arr)
		{
			cout << "void Bubble(): ";
			for (size_t i = 0; i < arr.size(); i++)
			{
				for (size_t j = 0; j < arr.size() - i - 1; j++)
				{
					if (arr[j] > arr[j + 1])
						swap(arr[j], arr[j + 1]);
				}
			}
			Print(arr);
		}

		// 霍尔版，选定最左值为基准值，右边先走（找小于等于基准值的元素）
		int Hoare(vector<int>& arr, int left, int right)
		{
			// 取 arr[left] 作为基准值
			int idx = left;

			while (left < right)
			{
				// right 需要找到比 val 小的位置
				while (right > left && arr[right] > arr[idx])
					right--;

				// left 需要找到比 val 大的位置
				while (left < right && arr[left] <= arr[idx])
					left++;

				if (left < right)
					swap(arr[left], arr[right]);
			}
			swap(arr[idx], arr[left]);

			return left;
		}

		// 挖坑法，选取最左值为基准值，挖坑，右边先走，找到值后填入坑中，同时形成新的坑，左边再走
		int DigPit(vector<int>& arr, int left, int right)
		{
			int pit = left;
			int val = arr[pit];

			while (left < right)
			{
				while (right > left && arr[right] > val)
					right--;
				arr[pit] = arr[right], pit = right;

				while (left < right && arr[left] <= val)
					left++;
				arr[pit] = arr[left], pit = left;
			}
			arr[pit] = val;
			return left;
		}

		// 双指针
		int DoublePoint(vector<int>& arr, int left, int right)
		{
			int idx = left;
			int slow = left, fast = left + 1;

			while (fast <= right)
			{
				if (arr[fast] <= arr[idx])
					swap(arr[++slow], arr[fast]);
				fast++;
			}
			swap(arr[slow], arr[idx]);
			return slow;
		}

		void _Quick(vector<int>& arr, int left, int right)
		{
			if (left >= right - 1)
				return;

			// 小区间优化
			if (right - left <= 20)
			{
				Intervals(arr, left, right);
				return;
			}

			//// 随机取中
			//GetMid(arr, left, right - 1);
			////int midIdx = Hoare(arr, left, right - 1);
			////int midIdx = DigPit(arr, left, right - 1);
			//int midIdx = DoublePoint(arr, left, right - 1);
			//_Quick(arr, left, midIdx);
			//_Quick(arr, midIdx, right);

			// 三路划分版
			auto ret = DivThreeRoad(arr, left, right - 1);
			int a = ret.first, b = ret.second;
			_Quick(arr, left, a);
			_Quick(arr, b + 1, right);
		}

		void Quick(vector<int> arr)
		{
			cout << "void Quick(): ";
			_Quick(arr, 0, arr.size());
			Print(arr);
		}

		// 迭代版快排（利用栈模拟递归，将左右区间入栈）
		void QuickNonR(vector<int> arr)
		{
			cout << "void QuickNonR(): ";
			if (arr.size() < 2)
				return;

			stack<pair<int, int>> st;
			st.push({ 0, arr.size() });

			while (st.size())
			{
				auto topVal = st.top();
				st.pop();

				int left = topVal.first;
				int right = topVal.second;

				GetMid(arr, left, right - 1); // 三数取中

				// 2.小区间优化
				if (right - left + 1 < 20)
				{
					Intervals(arr, left, right);
					continue;
				}

				// 普通迭代
				//int midIdx = DoublePoint(arr, left, right - 1);
				//if (left < midIdx)
				//	st.push({ left, midIdx });
				//if (midIdx + 1 < right)
				//	st.push({ midIdx + 1, right });

				// 三路划分优化版
				auto ret = DivThreeRoad(arr, left, right - 1);
				if (left + 1 < ret.first)
					st.push({ left, ret.first });
				if (ret.second + 1 < right)
					st.push({ ret.second + 1, right });
			}
			Print(arr);
		}

		// 1.三数取中
		void GetMid(vector<int>& arr, int left, int right)
		{
			if(left >= right)
				return;

			//int randMid = right >> 1;
			int randMid = left + rand() % (right - left); // 取随机数

			vector<int> tmp = { left, randMid, right };
			for (size_t i = 0; i < tmp.size(); i++)
				for (size_t j = 0; j < tmp.size() - i - 1; j++)
					if (arr[tmp[j]] > arr[tmp[j + 1]])
						swap(tmp[j], tmp[j + 1]);

			swap(arr[left], arr[tmp[1]]);
		}

		// 2.小区间优化（希尔排序）
		void Intervals(vector<int>& arr, int left, int right)
		{
			int gap = right - left + 1;
			while (gap > 1)
			{
				gap = gap / 3 + 1;
				for (int i = left; i < right - gap; i++)
				{
					int end = i;
					int tmp = arr[i + gap];
					while (end >= 0 && arr[end] > tmp)
						arr[end + gap] = arr[end], end -= gap;
					arr[end + gap] = tmp;
				}
			}
		}

		// 3.三路划分
		pair<int, int> DivThreeRoad(vector<int>& arr, int left, int right)
		{
			if (left >= right)
				return { left,right };

			int val = arr[left];
			int cur = left;
			while (cur <= right)
			{
				if (arr[cur] > val)
					swap(arr[cur], arr[right--]);
				else if (arr[cur] < val)
					swap(arr[cur++], arr[left++]);
				else
					cur++;
			}

			return { left, right };
		}

		// 归并排序（合并两个有序数组思想 + DFS）
		void _Merge(vector<int>& arr, int left, int right, vector<int>& tmp)
		{
			if (left >= right - 1)
				return;

			int mid = (left + right) >> 1;
			_Merge(arr, left, mid, tmp);
			_Merge(arr, mid, right, tmp);

			int i = left, j = mid, pos = left;
			while (i < mid || j < right)
			{
				if (i < mid && j < right)
				{
					if (arr[i] <= arr[j])
						tmp[pos++] = arr[i++];
					else
						tmp[pos++] = arr[j++];
				}
				else if (i < mid)
					tmp[pos++] = arr[i++];
				else
					tmp[pos++] = arr[j++];
			}

			for (int i = left; i < right; i++)
				arr[i] = tmp[i];
		}
		
		void Merge(vector<int> arr)
		{
			cout << "void Merge(): ";
			vector<int> tmp(arr.size()); // 辅助空间
			_Merge(arr, 0, arr.size(), tmp);
			Print(arr);
		}

		// 迭代版归并（利用区间模拟递归，区间值从小值大，在两个区间之间进行合并）
		void MergeNonR(vector<int> arr)
		{
			cout << "void MergeNonR(): ";
			size_t randN = 1; // 分组范围
			vector<int> tmp(arr.size());
			while (randN < arr.size())
			{
				for (size_t i = 0; i < arr.size(); i += randN * 2)
				{
					size_t bL = i; 
					size_t eL = i + randN;

					size_t bR = i + randN;
					size_t eR = i + 2 * randN;

					// 分析越界情况
					if (eL >= arr.size() || bR >= arr.size())
						break;
					else if (eR >= arr.size())
						eR = arr.size();

					// 合并数组
					int pos = i;
					while (bL < eL || bR < eR)
					{
						if (bL < eL && bR < eR)
						{
							if (arr[bL] <= arr[bR])
								tmp[pos++] = arr[bL++];
							else
								tmp[pos++] = arr[bR++];
						}
						else if (bL < eL)
							tmp[pos++] = arr[bL++];
						else
							tmp[pos++] = arr[bR++];
					}

					for (size_t j = i; j < eR; j++)
						arr[j] = tmp[j];
				}

				randN *= 2;
			}
			Print(arr);
		}

		// 计数排序（哈希映射思想）
		void Count(vector<int> arr)
		{
			int maxVal = INT_MIN, minVal = INT_MAX;
			for (auto e : arr)
				maxVal = max(maxVal, e), minVal = min(minVal, e);

			int n = maxVal - minVal + 1; // 容量
			vector<int> countArr(n);

			for (auto e : arr)
				countArr[e - minVal]++;

			int pos = 0;
			for (int i = 0; i < n; i++)
			{
				int cnt = countArr[i];
				while (cnt--)
					arr[pos++] = i + minVal;
			}

			Print(arr);
		}


		void Print(vector<int>& arr)
		{
			for (auto e : arr)
				cout << e << " ";
			cout << endl;
		}
	};

	void Test(vector<int>& arr)
	{
		Sort s;
		//s.Insert(arr);
		//s.Shell(arr);
		//s.Select(arr);
		//s.Heap(arr);
		//s.Bubble(arr);
		//s.Quick(arr);
		//s.QuickNonR(arr);
		//s.Merge(arr);
		//s.MergeNonR(arr);
		s.Count(arr);
	}
}