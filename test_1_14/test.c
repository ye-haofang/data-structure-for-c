#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

 //563. 二叉树的坡度

int getSlope(struct TreeNode* root, struct TreeNode* tmp)
{
    if (!root)
        return 0;

    int leftVal = getSlope(root->left, tmp);
    int rightVal = getSlope(root->right, tmp);

    int num = leftVal - rightVal;
    if (num < 0)
        num *= -1;

    if (root == tmp)
        root->val = num;

    return root->val + leftVal + rightVal;
}

void comSlope(struct TreeNode* root)
{
    if (!root)
        return;

    getSlope(root, root);

    comSlope(root->left);
    comSlope(root->right);
}

int slopSumVal(struct TreeNode* root)
{
    if (!root)
        return 0;

    int val = root->val;

    return val + slopSumVal(root->left) + slopSumVal(root->right);
}

int findTilt(struct TreeNode* root) {
    //先计算出每个节点的梯坡，然后遍历求和
    comSlope(root);
    int slop = slopSumVal(root);

    return slop;
}

// 通过前序遍历的数组"A B D # # E # H # # C F # # G # #"构建二叉树
struct TreeNode* BinaryTreeCreate(char* a, int n, int* pi)
{
    assert(a);

    //如果一开始就为 # 就没必要创建了
    if (a[*pi] == '#')
    {
        (*pi)++;	//向后移动，找到下一个值
        return NULL;
    }

    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    assert(node);

    node->val = a[(*pi)++] - '0';	//赋值，并向后移动

    node->left = BinaryTreeCreate(a, n, pi);	//左右链接
    node->right = BinaryTreeCreate(a, n, pi);

    return node;	//最开始的节点就是根节点
}

int main()
{
    char arr[] = "12##3##";
    int pos = 0;
    struct TreeNode* root = BinaryTreeCreate(arr, 0, &pos);
    int n = findTilt(root);
    return 0;
}