#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>



struct Node {
    int val;
    struct Node* next;
    struct Node* random;
};

 //138.复制带随机指针的链表
 //插入-链接-分离
#include<stdlib.h>
#include<assert.h>

struct Node* copyRandomList(struct Node* head)
{
    struct Node* cur = head;
    //1.创建一个新链表	
    struct Node* newhead, * newtail;
    newhead = newtail = NULL;

    //先获取一个新链表
    while (cur)
    {
        //不带哨兵位的新链表
        struct Node* newnode = (struct Node*)malloc(sizeof(struct Node));
        assert(newnode);
        newnode->val = cur->val;

        if (newhead == NULL)
        {
            newhead = newtail = newnode;
        }
        else
        {
            newtail->next = newnode;
            newtail = newnode;
        }

        cur = cur->next;
    }

    //将新链表插入到原链表中
    struct Node* prev, * mid, * next; //前、中、后三个点位
    prev = mid = next = head;   //可以都指向原表头
    cur = newhead;  //需要记录新链表
    while (prev)
    {
        mid = cur;  //待插入的节点
        cur = cur->next;
        next = prev->next;   //点位就绪

        //插入
        prev->next = mid;
        mid->next = next;

        prev = next;    //向后移动
    }

    //改变random的指向关系
    prev = mid = next = head;   //复用上面的移动段
    while (prev && prev->next)
    {
        mid = prev->next;
        next = mid->next;

        //链接
        if (prev->random == NULL)
        {
            mid->random = NULL;
        }
        else
        {
            mid->random = prev->random->next;   //巧妙链接
        }

        prev = next;    //向后移动
    }

    //分离出原链表
    //删除+尾插
    prev = mid = next = head;   //复用上面的移动段
    newhead = newtail = NULL;
    while (prev && prev->next)
    {
        mid = prev->next;
        next = mid->next;

        //删除
        prev->next = next;
        //尾插
        prev = next;    //向后移动
        if (newhead == NULL)
        {
            newhead = newtail = mid;
        }
        else
        {
            newtail->next = mid;
            newtail = mid;
        }

        prev = next;
    }

    return newhead;
}

typedef struct Node SLTNode;

extern void SLTPushBack(SLTNode**, int*, int);

int main()
{
    SLTNode* n1 = (SLTNode*)malloc(sizeof(SLTNode));
    SLTNode* n2 = (SLTNode*)malloc(sizeof(SLTNode));
    SLTNode* n3 = (SLTNode*)malloc(sizeof(SLTNode));
    SLTNode* n4 = (SLTNode*)malloc(sizeof(SLTNode));
    SLTNode* n5 = (SLTNode*)malloc(sizeof(SLTNode));

    n1->val = 7;
    n1->next = n2;
    n1->random = NULL;

    n2->val = 13;
    n2->next = n3;
    n2->random = n1;

    n3->val = 11;
    n3->next = n4;
    n3->random = n5;

    n4->val = 10;
    n4->next = n5;
    n4->random = n3;

    n5->val = 1;
    n5->next = NULL;
    n5->random = n1;

    n1 = copyRandomList(n1);

    return 0;
}