#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>

//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	memset(arr, 1, 4);
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//typedef int(*pf_t)(int);
//int main()
//{
//	int (*(*p)(int, int))(int);
//
//	pf_t(*ps)(int, int);
//
//	return 0;
//}

//int main()
//{
//	printf("%d\n", printf("abcde\n"));
//	return 0;
//}

//extern add(int, int);
//
//int main()
//{
//	int a = add(2, 3);
//	printf("%d", a);
//	return 0;
//}

//int main()
//{
//	int* p = (int*)malloc(sizeof(int) * 4);
//	int* ptr = realloc(p, sizeof(int) * 10);
//	free(p);
//	return 0;
//}

////CC3 编写函数实现两数交换（指针方式）
//#include <iostream>
//using namespace std;
//
//void swap(int* x, int* y)
//{
//    int tmp = *x;
//    *x = *y;
//    *y = tmp;
//}
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%d\n%d", &a, &b);
//    swap(&a, &b);
//    printf("%d %d\n", a, b);
//}

////CC4 利用指针遍历数组
//#include <iostream>
//#include<stdlib.h>
//#include<assert.h>
//using namespace std;
//
//int main()
//{
//    int* pn = (int*)malloc(sizeof(int) * 6);
//    assert(pn);
//    int i = 0;
//    while (i++ < 6)
//        scanf("%d", (pn + i));
//    i = 0;
//    while (i++ < 6)
//        printf("%d ", *(pn + i));
//
//    free(pn);
//    pn = NULL;
//}

////CC5 牛牛的新数组求和
//#include <stdio.h>
//#include<assert.h>
//
//int cal(int* pa, const int len)
//{
//    assert(pa);
//    int i = 0;
//    int sum = 0;
//    while (i < (int)len)
//        sum += *(pa + i++);
//    return sum;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int i = 0;
//    while (i < n)
//        scanf("%d", &arr[i++]);
//    int sum = cal(arr, n);
//    printf("%d\n", sum);
//    return 0;
//}

////CC6 牛牛的排序
//#include <stdio.h>
//#include<assert.h>
//#include<stdlib.h>
//
//int cmp(const void* e1, const void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//
//void sort(int* pa, const int len)
//{
//    assert(pa);
//    qsort(pa, len, sizeof(int), cmp);
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int i = 0;
//    while (i < n)
//        scanf("%d", &arr[i++]);
//    sort(arr, n);
//    i = 0;
//    while (i < n)
//        printf("%d ", arr[i++]);
//    return 0;
//}

////CC7 牛牛的单向链表
//#include<stdio.h>
//#include<assert.h>
//#include<stdlib.h>
//
//typedef struct SList
//{
//	int data;
//	struct SList* next;
//}SL;
//
//
//void SLPushBack(SL** ps)
//{
//	int x = 0;
//	scanf("%d", &x);
//	SL* newNode = (SL*)malloc(sizeof(SL) * 1);
//	assert(newNode);
//	newNode->data = x;
//	newNode->next = NULL;
//
//	if ((*ps) == NULL)
//		*ps = newNode;
//	else
//	{
//		SL* tail = *ps;
//		while (tail->next)
//			tail = tail->next;
//		tail->next = newNode;
//	}
//}
//
//void SLPrint(SL* ps)
//{
//	SL* cur = ps;
//	while (cur)
//	{
//		printf("%d ", cur->data);
//		cur = cur->next;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	SL* s1 = NULL;
//	while (n--)
//		SLPushBack(&s1);
//	SLPrint(s1);
//	return 0;
//}