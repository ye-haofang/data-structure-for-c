#define _CRT_SECURE_NO_WARNINGS 1

//当static修饰外部函数时，会抵消掉extern的外部链接属性，此时函数调用失败
static int add(int x, int y)
{
	return x + y;
}