#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>

////法一，想复杂了，没过
////面试题 08.05. 递归乘法
//
//void recMul(int* mul, int mulof2)
//{
//    if (mulof2 == 0)
//        return;
//
//    (*mul) <<= 1;
//    recMul(mul, mulof2 / 2);
//}
//
//void findEven(int A, int* B)
//{
//    int mulof2 = 2; //2的倍数
//    while (A == 0)
//    {
//        //取最后一位判断
//        if ((A & 1) == 1)
//        {
//            //找到了其中一个偶数
//            recMul(B, mulof2); //递归
//        }
//
//        mulof2 <<= 1;
//        A >>= 2;
//    }
//}
//
//int multiply(int A, int B) {
//    //左移1位，扩大两倍
//    //情况1、有2的倍数和奇数，利用2的倍数控制移位次数
//    //情况2、没有2的倍数，想办法拆出2的倍数
//
//    //假设A为2的倍数
//    if ((A & (A - 1)) == 0 && A != 1)
//    {
//        //如果成立，说明A是2的倍数，直接进行递归程序
//        recMul(&B, A);
//        return B;
//    }
//    else if ((B & (B - 1)) == 0 && B != 1)
//    {
//        //现在是B为2的倍数，执行递归
//        recMul(&A, B);
//        return A;
//    }
//    else
//    {
//        //既然没有2的倍数，那就造一个出来
//        if (A % 2 == 0)
//        {
//            //A是偶数，但不是2的倍数，可以拆分成两个2的倍数
//            findEven(A, &B);
//        }
//        else
//        {
//            //A是奇数，-1后，拆分成一个偶数和一个奇数
//            A -= 1;
//            B += B;
//            if ((A & (A - 1)) == 0)
//                recMul(&B, A);
//            else
//            {
//                //这里的A也是偶数
//                findEven(A, &B);
//            }
//        }
//
//        return B;
//    }
//}


//法二，本质解法
//面试题 08.05. 递归乘法

//乘法的本质是n个m相加
int multiply(int A, int B) {
    if (B == 1)
        return A;

    return A + multiply(A, B - 1);
}

int main()
{
    multiply(1, 10);
    return 0;
}