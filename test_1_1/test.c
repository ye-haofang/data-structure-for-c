#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
//
////面试题 01.06. 字符串压缩
//
////法一
//char* getStrNum(int num)
//{
//    char* numStr = (char*)calloc(50, sizeof(char));
//    assert(numStr);
//
//    char* tmp = numStr;
//
//    while (num)
//    {
//        *tmp++ = num % 10 + '0';
//        num /= 10;
//    }
//
//    //交换
//    char* left = numStr;
//    char* right = tmp - 1;
//    while (left < right)
//    {
//        char cc = *left;
//        *left++ = *right;
//        *right-- = cc;
//    }
//
//    return numStr;
//}
//
//char* compressString(char* S) {
//    int len = strlen(S);
//    char* tmp = (char*)calloc(len + 10, sizeof(char));
//    assert(tmp);
//
//    char* newStr = tmp;
//    char* ps = S;
//    while (*ps)
//    {
//        //对原字符串进行统计
//        char* cur = ps;
//        while (*cur == *ps)
//            cur++;  //找到不同的字符
//
//        //存在重复字符的情况
//        *newStr++ = *ps;    //字符位
//        int num = cur - ps; //获取整型数
//        char* sNum = getStrNum(num);    //获取字符串
//        char* psNum = sNum;
//        while (*sNum)
//            *newStr++ = *sNum++;    //将数字位赋过去
//
//        free(psNum);   //释放申请的内存
//
//        ps = cur;
//
//        //如果判断成立，说明压缩后的字符串长于原字符串，直接返回原字符串
//        if (newStr - tmp >= len)
//            return S;
//    }
//
//    memcpy(S, tmp, sizeof(char) * (newStr - tmp + 1));  //拷贝
//    free(tmp);
//    tmp = NULL;
//    return S;
//}

//法二，追加
//面试题 01.06. 字符串压缩

char* compressString(char* S) {
    int len = strlen(S);
    char* tmp = (char*)calloc(len + 10, sizeof(char));
    assert(tmp);

    char* newStr = tmp;
    char* ps = S;
    while (*ps)
    {
        //对原字符串进行统计
        char* cur = ps;
        while (*cur == *ps)
            cur++;  //找到不同的字符

        //存在重复字符的情况
        *newStr = *ps;    //字符位
        int num = cur - ps; //获取整型数
        char sNum[50] = "0";   //存储字符串
        sprintf(sNum, "%d", num);   //将整型转为字符串
        strcat(newStr, sNum);
        while (*newStr++);
        newStr--;

        ps = cur;

        //如果判断成立，说明压缩后的字符串长于原字符串，直接返回原字符串
        if (newStr - tmp >= len)
            return S;
    }

    memcpy(S, tmp, sizeof(char) * (newStr - tmp + 1));  //拷贝
    free(tmp);
    tmp = NULL;
    return S;
}

int main()
{
    char str[] = "aabcccccaa";
    compressString(str);
    return 0;
}