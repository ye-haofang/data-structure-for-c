#define _CRT_SECURE_NO_WARNINGS 1	

////965.单值二叉树
//bool isUnivalTree(struct TreeNode* root) {
//    if (!root)
//        return true;
//
//    if (root->left && root->left->val != root->val)
//        return false;
//
//    if (root->right && root->right->val != root->val)
//        return false;
//
//    return isUnivalTree(root->left) && isUnivalTree(root->right);
//}

////100.相同的树
//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (!p && !q)
//        return true;
//    if (p && q && p->val != q->val)
//        return false;
//    if (!p || !q)
//        return false;
//
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}
//
//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (p == NULL && q == NULL)
//        return true;
//    if (p && q && p->val != q->val)
//        return false;
//    if (!p || !q)
//        return false;
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}
//
//bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
//    if (!root)
//        return false;
//
//    if (isSameTree(root, subRoot))
//        return true;
//
//    return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
//}

//KY11 二叉树遍历
#include <stdio.h>
#include <stdlib.h>

struct BinaryTree
{
    char val;
    struct BinaryTree* left;
    struct BinaryTree* right;
};

struct BinaryTree* getBinaryTree(char* pa, int* pi)
{
    if (pa[*pi] == '#')
    {
        (*pi)++;
        return NULL;
    }

    struct BinaryTree* newnode = (struct BinaryTree*)malloc(sizeof(struct BinaryTree));
    newnode->val = pa[(*pi)++];

    newnode->left = getBinaryTree(pa, pi);
    newnode->right = getBinaryTree(pa, pi);

    return newnode;
}

void BTPrevOrder(struct BinaryTree* root)
{
    if (!root)
        return;

    BTPrevOrder(root->left);
    printf("%c ", root->val);
    BTPrevOrder(root->right);
}

int main() {
    char arr[100];
    scanf("%s", arr);

    int i = 0;
    struct BinaryTree* root = getBinaryTree(arr, &i);
    BTPrevOrder(root);
    return 0;
}