#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>


//14. 最长公共前缀
//https://leetcode.cn/problems/longest-common-prefix/

char* longestCommonPrefix(char** strs, int strsSize) {

    char* psL = strs[0];

    strcpy(psL, strs[0]);

    char* begin = psL;

    int lenEst = 200;
    int i = 1;
    while (i < strsSize)
    {
        int len = 0;

        char* psR = strs[i];
        while (*psL && *psR && len < lenEst)
        {
            if (*psL == *psR)
                len++;
            else
                break;

            psL++, psR++;
        }

        //刷新前缀值
        if (len <= lenEst)
            lenEst = len;

        psL = begin;
        *(psL + lenEst) = '\0';

        i++;
    }

    return psL;
}

int main()
{
    char* str[1] = {"a"};
    char*s = longestCommonPrefix(str, 1);
    return 0;
}