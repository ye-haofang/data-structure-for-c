#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<windows.h>

typedef int QListDataType;	//队列的数据类型

//这是队列中，单个节点的信息
typedef struct QListNode
{
	QListDataType data;
	struct QListNode* next;
}QNode;

//这是整个队列的信息，包含了队头和队尾两个指针
typedef struct QueueNode
{
	QNode* front;	//队头指针
	QNode* rear;	//队尾指针
	size_t size;	//队列长度
}Queue;

void QueueInit(Queue* pq);	//队列的初始化
void QueueDestroy(Queue* pq);	//队列的销毁

void QueuePrint(Queue* pq);	//队列的遍历打印

void QueuePush(Queue* pq, QListDataType x);	//入队
void QueuePop(Queue* pq);	//出队

QListDataType QueueFront(Queue* pq);	//查看队头元素
QListDataType QueueRear(Queue* pq);	//查看队尾元素

int QueueSize(Queue* pq);	//当前队列的有效元素数
bool QueueEmpty(Queue* pq);	//判断当前队空情况