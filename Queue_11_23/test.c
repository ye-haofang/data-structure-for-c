#define _CRT_SECURE_NO_WARNINGS 1	
#include"Queue.h"

void TestQueue1()
{
	Queue Q;
	QueueInit(&Q);

	int i = 0;
	while (i++ < 10)
	{
		QueuePush(&Q, i);
		QueuePrint(&Q);
	}

	i = 0;
	while (i++ < 10)
	{
		QueuePop(&Q);
		QueuePrint(&Q);
	}

	QueueDestroy(&Q);
}

void TestQueue2()
{
	Queue Q;
	QueueInit(&Q);

	int i = 0;
	while (i++ < 10000)
	{
		QueuePush(&Q, i);
		QueuePrint(&Q);
	}

	printf("队头元素为:%d\n", QueueFront(&Q));
	printf("队尾元素为:%d\n", QueueRear(&Q));
	printf("有效元素数为:%d\n", QueueSize(&Q));
	printf("队空情况:%d\n", QueueEmpty(&Q));

	i = 0;
	while (i++ < 10)
	{
		QueuePop(&Q);
		QueuePrint(&Q);
	}

	//printf("队头元素为:%d\n", QueueFront(&Q));
	//printf("队尾元素为:%d\n", QueueRear(&Q));
	printf("有效元素数为:%d\n", QueueSize(&Q));
	printf("队空情况:%d\n", QueueEmpty(&Q));

	QueueDestroy(&Q);
}


void menu()
{
	printf("****************************\n");
	printf("***	0.退出	1.打印	 ***\n");
	printf("***	2.入队	3.出队	 ***\n");
	printf("***	4.队头	5.队尾	 ***\n");
	printf("***	6.查看有效元素数 ***\n");
	printf("***	7.查看队列是否空 ***\n");
	printf("****************************\n");
}

int main()
{
	//TestQueue2();
	Queue Q;
	QueueInit(&Q);
	int input = 1;
	int val = 0;
	while (input)
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case 0:
			QueueDestroy(&Q);
			break;
		case 1:
			printf("注意：打印队列需要弹出队列内所有元素\n是否确认(1/0) : ");
			scanf("%d", &val);
			val == 0 ? printf("取消成功\n") : QueuePrint(&Q);
			break;
		case 2:
			printf("请输入入队元素:>");
			scanf("%d", &val);
			QueuePush(&Q, val);
			break;
		case 3:
			QueuePop(&Q);
			break;
		case 4:
			printf("队尾元素为:%d\n",QueueFront(&Q));
			break;
		case 5:
			printf("队头元素为:%d\n", QueueRear(&Q));
			break;
		case 6:
			printf("当前队内有效元素:%d\n", QueueSize(&Q));
			break;
		case 7:
			printf("当前队空情况:%d\n", QueueEmpty(&Q));
			break;
		}
	}

	QueueDestroy(&Q);
	return 0;
}