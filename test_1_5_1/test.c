#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>

//35. 搜索插入位置

int searchInsert(int* nums, int numsSize, int target) {
    //数组为有序，二分查找
    int left = 0;
    int right = numsSize - 1;
    int mid = 0;
    while (left < right)
    {
        mid = (left + right) / 2;

        if (nums[mid] < target)
            left = mid + 1;
        else if (nums[mid] > target)
            right = mid - 1;
        else
            return mid;
    }

    if (target < nums[left] || target == nums[left])
        return left;
    else
        return left + 1;
}

int main()
{
    int arr[] = { 1 };
    searchInsert(arr, sizeof(arr) / sizeof(arr[0]), 1);
    return 0;
}