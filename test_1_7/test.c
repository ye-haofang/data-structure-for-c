#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<stdbool.h>

typedef struct ListNode
{
    int val;
    struct ListNode* next;
}SLTNode;

//NC21 链表内指定区间反转

struct ListNode* reverseBetween(struct ListNode* head, int m, int n) {
    // write code here
    //两种特殊情况
    //1、m 和 n 相同，防止头节点 head 被反转至后面，需要核验
    //2、mNode 被交换至后面，此时不需要进行区间一的链接

    int len = n - m;    //需要反转的节点数
    struct ListNode* tmp = head;
    int begin = m - 1;  //不是数组，需要 -1
    struct ListNode* mNode = tmp;   //衔接区间一，将它和 beginNode 链接
    struct ListNode* nNode = tmp;   //衔接区间二，将它和 endNode 链接
    bool flag = false;
    while (n - m && begin--)
    {
        flag = true;
        if (begin == 0)
            mNode = tmp;
        tmp = tmp->next;    //移动至起始区间
    }

    struct ListNode* beginNode = tmp;
    struct ListNode* endNode = tmp;
    struct ListNode* cur = tmp;

    //反转单链表
    tmp = tmp->next;
    while (len--)
    {
        beginNode = tmp;
        tmp = tmp->next;
        beginNode->next = cur;
        cur = beginNode;
    }

    nNode = tmp;

    //更改链接关系
    if (flag)
        mNode->next = beginNode;
    endNode->next = nNode;

    //核验节点
    if (head->next == NULL || !flag)
        return beginNode;   //如果成立，说明原头节点已经被反转到后面了

    return head;
}

extern void SLTPushBack(SLTNode** pphead, int* arr, int len);

int main()
{
    int arr[] = { 3,1,3,4,1,4,-1 };
    SLTNode* L1 = NULL;
    SLTPushBack(&L1, arr, sizeof(arr) / sizeof(arr[0]));
    reverseBetween(L1, 3, 7);
    return 0;
}