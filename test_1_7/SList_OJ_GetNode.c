#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef struct ListNode
{
    int val;
    struct ListNode* next;
}SLTNode;

static SLTNode* BuyNewNode(int x)
{
    SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode) * 1);
    assert(newnode);

    newnode->val = x;
    newnode->next = NULL;

    return newnode;
}

//尾插
void SLTPushBack(SLTNode** pphead, int* arr,int len)
{
    int i = 0;
    for (i = 0; i < len; i++)
    {
        SLTNode* newnode = BuyNewNode(arr[i]);
        if (*pphead == NULL)
            (*pphead) = newnode;
        else
        {
            SLTNode* tail = *pphead;
            while (tail->next)
                tail = tail->next;

            tail->next = newnode;
        }
    }
}