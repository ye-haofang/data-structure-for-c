#define _CRT_SECURE_NO_WARNINGS 1	
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

typedef struct TreeNode* QListDataType;	//队列的数据类型

//这是队列中，单个节点的信息
typedef struct QListNode
{
    QListDataType data;
    struct QListNode* next;
}QNode;

//这是整个队列的信息，包含了队头和队尾两个指针
typedef struct QueueNode
{
    QNode* front;	//队头指针
    QNode* rear;	//队尾指针
    size_t size;	//队列长度
}Queue;

bool QueueEmpty(Queue* pq);	//判断当前队空情况

void QueueInit(Queue* pq)	//队列的初始化
{
    assert(pq);

    pq->front = pq->rear = NULL;
    pq->size = 0;   //初始化状态
}

void QueueDestroy(Queue* pq)	//队列的销毁
{
    assert(pq);

    if (pq->front)
    {
        while (pq->front)
        {
            QNode* tmp = pq->front->next;
            free(pq->front);
            pq->front = tmp;
        }
    }

    pq->front = pq->rear = NULL;
    pq->size = 0;
}

static QNode* BuyNewNode(QListDataType x)   //买节点
{
    QNode* newNode = (QNode*)malloc(sizeof(QNode) * 1);
    assert(newNode);

    newNode->data = x;
    newNode->next = NULL;

    return newNode;
}

void QueuePush(Queue* pq, QListDataType x)	//入队
{
    assert(pq);

    QNode* newNode = BuyNewNode(x);

    if (pq->front)
    {
        pq->rear->next = newNode;
        pq->rear = newNode;
    }
    else
        pq->front = pq->rear = newNode;

    pq->size++;
}

void QueuePop(Queue* pq)	//出队
{
    assert(pq);
    assert(!QueueEmpty(pq));    //栈空不能出队

    QNode* tmp = pq->front->next;
    free(pq->front);
    pq->front = tmp;
    pq->size--;
}

QListDataType QueueFront(Queue* pq)	//查看队头元素
{
    assert(pq);
    assert(!QueueEmpty(pq));

    return pq->front->data;
}

bool QueueEmpty(Queue* pq)	//判断当前队空情况
{
    assert(pq);

    return pq->size == 0;
}

// 层序遍历
bool BinaryTreeLevelOrder(struct TreeNode* root)
{
    //思路：利用队列，一层出带一层入
    //设置一个权重值，从0开始，即 2^0 表示第一层需要质检 1 个数(权重方案失效)
    //此时的权值值为偶数，表示这个是奇数层(奇偶判断改为信号)
    //注：因为不是完全二叉树，所以需要特殊处理
    //初始节点数为1，至于下一层有多少节点数，需要靠本层的节点来计算

    int sign = 1;    //1是奇，0是偶
    Queue Q1;
    QueueInit(&Q1);

    if (root)
        QueuePush(&Q1, root);

    int tmpNum = 1; //这个值比较重要，默认从1开始，因为第一层就一个节点
    int tmpVal = 0;  //需要遵循递增/递减规则
    int nodeNum = 0; //统计节点数的，当 tmpNum=0 时，赋值给 tmpNum
    while (!QueueEmpty(&Q1))
    {
        struct TreeNode* tmpNode = QueueFront(&Q1);
        QueuePop(&Q1);
        tmpNum--;    //成功取到一个数

        int tmpNodeVal = tmpNode->val;

        //对取到的数进行质检，首先要先判断奇偶
        if (sign)
        {
            //此时是奇数层的质检方式
            if (tmpNodeVal % 2 == 0)
                return false;

            if (tmpVal >= tmpNodeVal)
                return false;

            tmpVal = tmpNodeVal;
        }
        else
        {
            //偶数层
            if (tmpNodeVal % 2 != 0)
                return false;

            if (tmpVal <= tmpNodeVal)
                return false;

            tmpVal = tmpNodeVal;    //记录此时的值，方便下次进行比较 
        }

        if (tmpNode->left)
        {
            QueuePush(&Q1, tmpNode->left);
            //存在的节点，都会被统计进 nodeNum 中，方便后续判断
            nodeNum++;
        }
        if (tmpNode->right)
        {
            QueuePush(&Q1, tmpNode->right);
            nodeNum++;
        }

        //如果 tmpNum 变为0，就意味着进入了下一层
        //此时的节点数、奇偶性、临时比较值 tmpVal 都要发生变化(奇数层初始为0，偶数层初始为1000001)

        if (tmpNum == 0)
        {
            //奇偶变换
            sign = (sign + 1) % 2; //解决奇偶变换问题

            //节点重置
            tmpNum = nodeNum;
            nodeNum = 0; //需要重新累计

            //临时值比较值初始化
            if (sign)
                tmpVal = 0;
            else
                tmpVal = 1000001;
        }
    }

    QueueDestroy(&Q1);

    return true;
}

//1609. 奇偶树
//https://leetcode.cn/problems/even-odd-tree/submissions/

bool isEvenOddTree(struct TreeNode* root) {
    return BinaryTreeLevelOrder(root);
}