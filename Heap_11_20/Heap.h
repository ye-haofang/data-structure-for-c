#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<time.h>

typedef int HPDataType;

typedef struct HeapInfo
{
	HPDataType* data;
	int size;
	int capacity;
}HP;

void HeadInit(HP* php);	//初始化
void HeadCreat(HP* php, HPDataType* pa, int len);	//构建堆
void HeadCreatPlus(HP* php, HPDataType* pa, int len);	//构建堆_建堆算法（向下调整）
void HeadDestroy(HP* php);	//销毁
void HeadPrint(HP* php);	//打印

void HeadPush(HP* php, HPDataType x);	//插入数据
void HeadPop(HP* php);	//删除数据

HPDataType HeapTop(HP* php);	//取堆顶的数据
size_t HeapSize(HP* php);	//求堆的元素个数（大小）
bool HeadEmpty(HP* php);	//判断堆是否为空

void PrintTopK(int* a, int n, int k);	//Top-K 问题

// 对数组进行堆排序
void HeapSort(int* a, int n);