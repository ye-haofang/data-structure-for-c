#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

void HeadInit(HP* php)	//初始化
{
	assert(php);

	//不申请空间，直接完全初始化
	php->data = NULL;
	php->size = php->capacity = 0;
}

void AdjustUp(HPDataType* pa, int len, int child);
void AdjustDown(HPDataType* pa, int len, int parent);
void HeadPush(HP* php, HPDataType x);

void HeadCreat(HP* php, HPDataType* pa, int len)	//构建堆_插入式建堆（向上调整）
{
	assert(php && pa);

	//pa是目标数组，len是目标数组的长度
	int i = 0;
	while (i < len)
	{
		HeadPush(php, pa[i++]);
	}
	
}
void HeadCreatPlus(HP* php, HPDataType* pa, int len)	//构建堆_建堆算法（向下调整）
{
	assert(php && pa);

	HPDataType* tmp = (HPDataType*)realloc(php->data, sizeof(HPDataType) * len);
	if (!tmp)
	{
		perror("realloc fail");
		exit(-1);
	}

	php->data = tmp;

	memcpy(php->data, pa, sizeof(HPDataType) * len);
	php->size = php->capacity = len;

	int parent = (php->size - 1 - 1) / 2;	//取得堆中最后一个元素的父亲
	while (parent >= 0)
	{
		//不断向下调整
		//父亲也会跟着调整
		AdjustDown(php->data, len, parent);
		parent--;
	}
}

void HeadDestroy(HP* php)	//销毁
{
	assert(php);
	
	if (php->data)
	{
		free(php->data);
		php->data = NULL;
		php->size = php->capacity = 0;
	}
}

void HeadPrint(HP* php)	//打印
{
	assert(php);

	int i = 0;
	for (i = 0; i < php->size; i++)
	{
		printf("%d ", php->data[i]);
	}
	printf("\n");
}

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustUp(HPDataType* pa, int len, int child)
{
	assert(pa);

	int parent = (child - 1) / 2;	//左右孩子都一样
	while (child > 0)
	{
		//如果孩子 < 父亲，是在建小堆
		//反之，孩子 > 父亲，是在建大堆
		//可以根据符号一眼看出大小堆
		if (pa[child] > pa[parent])
		{
			Swap(&pa[child], &pa[parent]);	//交换
			child = parent;	//孩子成为父亲
			parent = (child - 1) / 2;	//更新父亲
		}
		else
			break;
	}
}

void HeadPush(HP* php, HPDataType x)	//插入数据
{
	assert(php);

	//判断扩容
	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->data, sizeof(HPDataType) * newcapacity);
		if (!tmp)
		{
			perror("realloc fail");
			exit(-1);
		}

		php->data = tmp;
		php->capacity = newcapacity;
	}

	php->data[php->size++] = x;

	//需要向上调整
	AdjustUp(php->data, php->size, php->size - 1);
}

void AdjustDown(HPDataType* pa, int len, int parent)
{
	assert(pa);

	//向下调整，主要是把大孩子(小孩子)往上面交换

	//假设法，假设左孩子为大孩子(小孩子)
	int child = (parent * 2) + 1;
	while (child < len)
	{
		if (child + 1 < len && pa[child + 1] > pa[child])
			child += 1;	//变成右孩子，child+1 < len，防止越界

		//和向上调整一样的逻辑，推荐用符号判别大小堆
		if (pa[child] > pa[parent])
		{
			Swap(&pa[child], &pa[parent]);
			parent = child;	//父亲变成孩子
			child = (parent * 2) + 1;	//更新孩子
		}
		else
			break;
	}
}

void HeadPop(HP* php)	//删除数据
{
	assert(php);

	//先交换堆顶和堆底的数据
	//然后size--
	//再向下调整

	Swap(&php->data[0], &php->data[php->size - 1]);
	php->size--;

	AdjustDown(php->data, php->size, 0);
}

HPDataType HeapTop(HP* php)	//取堆顶的数据
{
	assert(php);

	return php->data[0];	//下标0处就是堆顶
}

size_t HeapSize(HP* php)	//求堆的元素个数（大小）
{
	assert(php);

	return php->size;	//size就是元素个数
}

bool HeadEmpty(HP* php)	//判断堆是否为空
{
	assert(php);

	return php->size == 0;	//个数为0，说明堆空
}

void PrintTopK(int* a, int n, int k)	//Top-K 问题
{
	//现在求最大的前k个数，建小堆
	assert(a);

	HP minHeap;
	HeadInit(&minHeap);

	HeadCreatPlus(&minHeap, a, k);	//建k个大小的小堆

	int i = k;
	while (i < n)
	{
		//如果数组a的元素比小堆的堆顶小，就沉入小堆中
		int val = a[i];
		if (val > minHeap.data[0])
		{
			minHeap.data[0] = val;
			AdjustDown(minHeap.data, k, 0);	//向下调整
		}
		i++;
	}

	HeadPrint(&minHeap);	//打印验证

	HeadDestroy(&minHeap);
}

// 对数组进行堆排序
void HeapSort(int* a, int n)
{
	assert(a);

	//建堆，排序，升序，建大堆
	
	/*
	* 步骤
	* 1.建堆
	* 2.把堆顶的元素，交换到堆底
	* 3.交换完成后，堆的长度缩减1
	* 4.如此重复，直到交换完成
	*/

	int i = (n - 1 - 1) / 2;
	for (; i >= 0; i--)
	{
		AdjustDown(a, n, i);	//向下调整，建堆
	}

	i = n - 1;	//这是堆底
	while (i > 0)
	{
		Swap(&a[0], &a[i]);
		AdjustDown(a, i, 0);	//向下调整，建堆
		i--;
	}

	for (i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}