#define _CRT_SECURE_NO_WARNINGS 1	
#include"Heap.h"

void TestHeap1()
{
	//测试常规插入、删除及其余功能
	HP H;
	HeadInit(&H);

	HeadPush(&H, 5);
	HeadPush(&H, 19);
	HeadPush(&H, 4);
	HeadPush(&H, 23);
	HeadPush(&H, 2);
	HeadPrint(&H);

	HeadPop(&H);
	HeadPop(&H);
	HeadPop(&H);
	HeadPrint(&H);


	int i = 0;
	for (i = 10; i < 20; i++)
	{
		HeadPush(&H, i);
		HeadPrint(&H);
	}

	printf("当前堆顶元素为：%d\n", HeapTop(&H));
	printf("当前堆的大小为：%d\n", HeapSize(&H));
	printf("当前堆满状态：%d\n", HeadEmpty(&H));

	HeadDestroy(&H);
}
void TestHeap2()
{
	//测试creat构造堆
	//常规建堆及建堆算法均有测试
	HP H;
	HeadInit(&H);
	int arr[] = { 1,3,4,45,56,45,3,4,2,312,32 };

	//HeadCreat(&H, arr, sizeof(arr) / sizeof(arr[0]));

	HeadCreatPlus(&H, arr, sizeof(arr) / sizeof(arr[0]));	//速度更快的建堆算法

	HeadPrint(&H);

	HeadDestroy(&H);
}
void TestHeap3()
{
	//测试单，Top-K
	int arr[] = { 1,2,32,234,5,45,34,563,5123 };
	//最大的前五个数为：5123、563、234、45、34
	PrintTopK(arr, sizeof(arr) / sizeof(arr[0]), 5);
}

void TestTopk()	//调用函数
{
	//测试文件读取，Top-K问题
	//涉及大量数据的排序，可以验证建堆算法的优势性
	
	//向文件中随机写入一堆随机数
	FILE* fin = fopen("Heap.txt", "w");
	if (fin == NULL)
	{
		perror("fopen fail");
		return;
	}
	
	int n = 0;
	int k = 0;
	printf("请输入数量及k值:>");
	scanf("%d %d", &n, &k);

	int kk = k;
	srand((size_t)time(NULL));
	int i = 0;
	while (i < n)
	{
		int num = rand() % 10000;	//随机生成的数，在1w以内
		//可能出现，kk没有用完的情况
		if (num % 3 == 0 && (kk > 0))
		{
			num += 999999;	//随机分布
			kk--;
		}
		fprintf(fin, "%d\n", num);
		i++;
	}

	fclose(fin);
	fin = NULL;

	/////////////////////////////////////////////////////////
	//读取文件

	FILE* fout = fopen("Heap.txt", "r");
	if (fout == NULL)
	{
		perror("fopen fail");
		return;
	}

	//目标找出文件 Heap.txt 中最大的前k个数，数据位于文件中
	//得先读出前k个数据，放到堆中

	HPDataType* minHeap = (HPDataType*)malloc(sizeof(HPDataType) * k);
	if (!minHeap)
	{
		perror("malloc fail");
		return;
	}

	kk = 0;
	while (kk < k)
	{
		fscanf(fout, "%d", &minHeap[kk++]);	//先读取k个数
	}

	extern void AdjustDown(HPDataType*, int, int);	//声明外部函数
	kk = (k - 1 - 1) / 2;	//父亲坐标
	while (kk >= 0)
	{
		//建堆
		AdjustDown(minHeap, k, kk);
		kk--;
	}

	HPDataType val = 0;
	while (fscanf(fout, "%d", &val) == 1)
	{
		if (val > minHeap[0])
		{
			minHeap[0] = val;
			AdjustDown(minHeap, k, 0);
		}
	}

	kk = k-1;
	while (kk >= 0)
	{
		printf("%d ", minHeap[kk]);
		kk--;
	}

	fclose(fout);
	fout = NULL;
}

void TestHeap4()
{
	int a[] = { 1,5,3,2,7,8,4,78 };
	HeapSort(a, sizeof(a) / sizeof(a[0]));
}

int main()
{
	TestHeap2();
	//TestTopk();
	return 0;
}

/*
* //2022.11.20 学习二叉树笔记
typedef int BTDataType;

typedef struct BTInfo
{
	BTDataType data;
	struct BTInfo* left;
	struct BTInfo* right;
}BTNode;


void TailOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	TailOrder(root->left);
	TailOrder(root->right);
	printf("%d ", root->data);
}

int TreeHeight(BTNode* root)
{
	//递归，后序遍历
	if (root == NULL)
		return 0;
}
*/