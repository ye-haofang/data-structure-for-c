#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//汉诺塔问题
/*
思想：大事化小事
1.先将A柱中除最底下大盘外的所有盘子移动到B柱(需要借助C柱)，大盘A->C
2.然后再将B柱中所有盘子移动到C柱
假设盘子数为n
移动所耗费时间为 2^n - 1
*/

void movePrint(char pos1, char pos2)
{
	printf(" %c->%c ", pos1, pos2);
}

void Hanoi(int n, char pos1, char pos2, char pos3)
{
	if (n == 1)
	{
		movePrint(pos1, pos3);	//打印出来，更加直观
	}
	else
	{
		Hanoi(n - 1, pos1, pos3, pos2);
		movePrint(pos1, pos3);
		Hanoi(n - 1, pos2, pos1, pos3);
	}
}

int main()
{
	Hanoi(3, 'A', 'B', 'C');
	return 0;
}