#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

//超时未解决
//1664. 生成平衡数组的方案数
//https://leetcode.cn/problems/ways-to-make-a-fair-array/

int waysToMakeFair(int* nums, int numsSize) {
    int oddVal = 0;
    int evenVal = 0;

    int oddTmp = 0;
    int evenTmp = 0;

    int schNum = 0; //方案数
    for (int i = 0; i < numsSize; i++)
    {
        for (int j = i; j < numsSize; j++)
        {
            //在i之前的数不受变换影响
            if (j == i)
            {
                if (j % 2 == 0)
                    evenTmp += nums[j];
                else
                    oddTmp += nums[j];
            }
            else if (j > i)
            {
                if (j % 2 == 0)
                    oddVal += nums[j];
                else
                    evenVal += nums[j];
            }
        }

        if (i % 2 == 0)
            evenVal -= nums[i];
        else
            oddVal -= nums[i];

        if ((oddVal + oddTmp) == (evenVal + evenTmp))
            schNum++;

        oddVal = evenVal = 0;
    }

    return schNum;
}

int main()
{
    int arr[] = { 2,1,4,6 };
    int n = waysToMakeFair(arr, sizeof(arr) / sizeof(arr[0]));
    return 0;
}