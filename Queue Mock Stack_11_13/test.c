#define _CRT_SECURE_NO_WARNINGS 1	
//力扣 225.用队列实现栈
//https://leetcode.cn/problems/implement-stack-using-queues/

#include<assert.h>
#include<stdbool.h>
#include<stdlib.h>
//先写一个队列
typedef int QDataType;

typedef struct QueueNode
{
    QDataType data;
    struct QueueNode* next;
}QNode;

typedef struct Queue
{
    QNode* head;    //队头指针
    QNode* tail;    //队尾指针
    size_t size;    //大小
}Queue;

void QueueInit(Queue* pq)
{
    assert(pq);
    pq->head = pq->tail = NULL;
    pq->size = 0;
}

void QueueDestroy(Queue* pq)
{
    assert(pq);

    QNode* cur = pq->head;
    while (cur)
    {
        QNode* tmp = cur->next;
        free(cur);
        cur = tmp;
    }

    pq->head = pq->tail = NULL;
    pq->size = 0;
}

bool QueueEmpty(Queue* pq)
{
    assert(pq);

    return pq->head == NULL && pq->tail == NULL;    //如果为空，返回true
}

void QueuePush(Queue* pq, QDataType x)
{
    assert(pq);

    QNode* newnode = (QNode*)malloc(sizeof(QNode));
    if (!newnode)
    {
        perror("newnode malloc fail");
        exit(-1);
    }
    newnode->data = x;
    newnode->next = NULL;

    if (pq->tail == NULL)
    {
        pq->head = pq->tail = newnode;
    }
    else
    {
        pq->tail->next = newnode;
        pq->tail = newnode; //更新尾节点
    }
    pq->size++;
}

void QueuePop(Queue* pq)
{
    assert(pq);
    assert(!QueueEmpty(pq));

    if (pq->head->next == NULL)
    {
        free(pq->head); //只有一个节点，直接释放
        pq->head = pq->tail = NULL;
    }
    else
    {
        QNode* cur = pq->head;
        pq->head = pq->head->next;  //更新头节点信息
        free(cur);
        cur = NULL;
    }
    pq->size--;
}

QDataType QueueBack(Queue* pq)
{
    assert(pq);
    assert(!QueueEmpty(pq));

    return pq->tail->data;
}

QDataType QueueFront(Queue* pq)
{
    assert(pq);
    assert(!QueueEmpty(pq));

    return pq->head->data;
}

size_t QueueSize(Queue* pq)
{
    assert(pq);

    return pq->size;
}

typedef struct
{
    Queue Q1;
    Queue Q2;   //创建两个队列
} MyStack;


MyStack* myStackCreate() {
    //初始化+确定栈
    MyStack* obj = (MyStack*)malloc(sizeof(MyStack));
    if (!obj)
    {
        perror("malloc fail");
        exit(-1);
    }

    QueueInit(&obj->Q1);
    QueueInit(&obj->Q2);

    return obj;
}

bool myStackEmpty(MyStack* obj);

void myStackPush(MyStack* obj, int x) {
    assert(obj);

    //如果Q1非空，就对其进行操作，否则选择Q2
    if (!QueueEmpty(&obj->Q1))
        QueuePush(&obj->Q1, x);
    else
        QueuePush(&obj->Q2, x);

}

int myStackPop(MyStack* obj) {
    assert(obj);
    assert(!myStackEmpty(obj));

    Queue* empty = &obj->Q1;  //假设Q1为空
    Queue* noEmpty = &obj->Q2;    //假设Q2非空
    if (!QueueEmpty(&obj->Q1))
    {
        empty = &obj->Q2;
        noEmpty = &obj->Q1;   //颠倒
    }

    while (QueueSize(noEmpty) != 1)
    {
        QDataType tmp = QueueFront(noEmpty);
        QueuePop(noEmpty);

        QueuePush(empty, tmp); //把数据导到空队中
    }

    //此时的noEmpty只剩一个元素，就是待出栈的元素
    QDataType x = QueueFront(noEmpty);
    QueuePop(noEmpty);

    return x;
}

int myStackTop(MyStack* obj) {
    assert(obj);
    assert(!myStackEmpty(obj));

    Queue* empty = &obj->Q1;  //假设Q1为空
    Queue* noEmpty = &obj->Q2;    //假设Q2非空
    if (!QueueEmpty(&obj->Q1))
    {
        empty = &obj->Q2;
        noEmpty = &obj->Q1;   //颠倒
    }

    return QueueBack(noEmpty);
}

bool myStackEmpty(MyStack* obj) {
    assert(obj);

    return QueueEmpty(&obj->Q1) && QueueEmpty(&obj->Q2);    //两个队列都为空，说明栈为空
}

void myStackFree(MyStack* obj) {
    QueueDestroy(&obj->Q1);
    QueueDestroy(&obj->Q2);
    //&obj->Q1 = &obj->Q2 = NULL:
    free(obj);
    obj = NULL;
}

int main()
{
    MyStack* obj = myStackCreate();
    myStackPush(obj, 1);
    myStackPush(obj, 2);

    int a = myStackTop(obj);
    a  = myStackPop(obj);

    a = myStackEmpty(obj);
    return 0;
}