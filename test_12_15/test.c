#define _CRT_SECURE_NO_WARNINGS 1	

//�ַ���ת����
int getLucky(char* s, int k) {
    int num = 0;
    while (*s)
    {
        int tmp = (*s) - 96;
        if (tmp >= 10)
        {
            num += (tmp / 10);
            num += (tmp % 10);
        }
        else
            num += tmp;
        s++;
    }

    int i = k;
    while (--i)
    {
        int tmp = num;
        num = 0;
        while (tmp)
        {
            num += (tmp % 10);
            tmp /= 10;
        }
    }

    return num;
}