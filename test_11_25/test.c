#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

//struct TreeNode 
//{
//    int val;
//    struct TreeNode* left;
//    struct TreeNode* right;
//};


////965.单值二叉树
////思路：在有左右子树的前提下，根的值和左右子树相等，返回true
//bool isUnivalTree(struct TreeNode* root) {
//
//    if (!root)
//        return true;
//    if ((root->left) && root->val != root->left->val)
//        return false;
//    if ((root->right) && root->val != root->right->val)
//        return false;
//
//    return isUnivalTree(root->left) && isUnivalTree(root->right);
//}

//#include<string.h>
//
//void BTPrevOrder(struct TreeNode* root, char** pc)
//{
//    if (!root)
//    {
//        *(*pc) = 'x';   //避免出现 [1,1]、[1,null,,1] 这样的测试用例，空也给安排成字符
//        return;
//    }
//
//    *(*pc) += root->val + '0';  //尽量弄成字符的形式，后续好判断
//    (*pc)++;
//    BTPrevOrder(root->left, pc);
//    BTPrevOrder(root->right, pc);
//}
//
//void BTInOrder(struct TreeNode* root, char** pc)
//{
//    if (!root)
//    {
//        *(*pc) = 'x';
//        return;
//    }
//
//    BTPrevOrder(root->left, pc);
//    *(*pc) += root->val + '0';  //尽量弄成字符的形式，后续好判断
//    (*pc)++;
//    BTPrevOrder(root->right, pc);
//}
//
////100.相同的树
////尝试新思路：前序、中序确定唯一的树
//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    char* pc1 = (char*)calloc(sizeof(char), 110);  //属于 p 的空间
//    char* tmp1 = pc1;   //方便回溯
//    char* pc2 = (char*)calloc(sizeof(char), 110);  //属于 q 的空间
//    char* tmp2 = pc2;
//
//    //先前序遍历两颗树
//    BTPrevOrder(p, &pc1);
//    BTPrevOrder(q, &pc2);
//
//    pc1 = tmp1, pc2 = tmp2; //回溯后继续遍历
//    BTInOrder(p, &pc1);
//    BTInOrder(q, &pc2);
//
//    int num = strcmp(tmp1, tmp2);
//    free(tmp1);
//    free(tmp2);
//    tmp1 = tmp2 = pc1 = pc2 = NULL;
//
//    return num == 0;
//}
//int main()
//{
//    struct TreeNode* p1 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//    struct TreeNode* p2 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//    struct TreeNode* p3 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//
//    struct TreeNode* q1 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//    struct TreeNode* q2 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//    struct TreeNode* q3 = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//
//    p1->val = q1->val = 1, p2->val = q2->val = 2, p3->val = q3->val = 3;
//
//    p1->left = p2, p1->right = p3, p2->left = p2->right = p3->left = p3->right = NULL;
//    q1->left = q2, q1->right = q3, q2->left = q2->right = q3->left = q3->right = NULL;
//
//    printf("%d\n", isSameTree(p1, q1));
//
//	return 0;
//}
//
////572.另一棵树的子树
////思路：判断 root 树中的每颗子树，是否和 subRoot 树相等
//bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
//    if (!root && !subRoot)
//        return false;
//
//    if (!root || !subRoot)
//        return false;
//
//    if (isSameTree(root, subRoot))
//        return true;
//
//    return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
//}

////KY11 二叉树遍历
//#include<stdio.h>
//#include<stdlib.h>
//#include<assert.h>
//
//typedef char BTDataType;
//
//typedef struct BinaryTreeNode
//{
//    BTDataType data;
//    struct BinaryTreeNode* left;
//    struct BinaryTreeNode* right;
//}BTNode;
//
//void BinaryTreeDestroy(BTNode* root)
//{
//    if (!root)
//        return;
//
//    BinaryTreeDestroy(root->left);
//    BinaryTreeDestroy(root->right);
//
//    free(root);
//}
//
//BTNode* BinaryTreeCreate(char* pc, int* i)
//{
//    assert(pc);
//
//    if (pc[*i] == '#')
//    {
//        (*i)++;
//        return NULL;
//    }
//
//    BTNode* newnode = (BTNode*)malloc(sizeof(BTNode));
//    if (!newnode)
//    {
//        perror("malloc::fail");
//        exit(-1);
//    }
//    newnode->data = pc[(*i)++]; //可能有bug
//
//    newnode->left = BinaryTreeCreate(pc, i);
//    newnode->right = BinaryTreeCreate(pc, i);
//
//    return newnode;
//}
//
//void BinaryTreePrevOrder(BTNode* root)
//{
//    if (!root)
//        return;
//
//    BinaryTreePrevOrder(root->left);
//    printf("%c ", root->data);
//    BinaryTreePrevOrder(root->right);
//}
//
//int main()
//{
//    char str[100];
//    scanf("%s", str);
//    int i = 0;
//    BTNode* root = BinaryTreeCreate(str, &i);
//    BinaryTreePrevOrder(root);
//    BinaryTreeDestroy(root);
//    return 0;
//}

struct TreeNode {
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
};


#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<string.h>

struct TreeNode;

typedef struct TreeNode* QListDataType;	//队列的数据类型

//这是队列中，单个节点的信息
typedef struct QListNode
{
	QListDataType data;
	struct QListNode* next;
}QNode;

//这是整个队列的信息，包含了队头和队尾两个指针
typedef struct QueueNode
{
	QNode* front;	//队头指针
	QNode* rear;	//队尾指针
	size_t size;	//队列长度
}Queue;

void QueueInit(Queue* pq)	//队列的初始化
{
	assert(pq);

	//初始化状态：队头、队尾指针都指向空，队列大小为0
	pq->front = pq->rear = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)	//队列的销毁
{
	assert(pq);

	//销毁思路：利用临时指针进行销毁
	QNode* cur = pq->front;
	while (cur)
	{
		QNode* tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	pq->front = pq->rear = NULL;
	pq->size = 0;
}
bool QueueEmpty(Queue* pq)	//判断当前队空情况
{
	assert(pq);

	return pq->front == NULL;
}
static QNode* buyNode(QListDataType x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc :: fail");
		exit(-1);
	}

	newnode->data = x;	//赋值就是新节点
	newnode->next = NULL;

	return newnode;
}

void QueuePush(Queue* pq, QListDataType x)	//入队
{
	assert(pq);

	//先买一个节点
	QNode* newnode = buyNode(x);

	//分情况：如果队头为空，说明队空，此时直接将新节点赋值给队头、队尾
	if (pq->front == NULL)
	{
		pq->front = pq->rear = newnode;
		pq->size++;
	}
	else
	{
		//否则就是将新节点，链接到队尾，然后更新队尾
		pq->rear->next = newnode;	//链接
		pq->rear = newnode;	//更新队尾
		pq->size++;
	}
}
void QueuePop(Queue* pq)	//出队
{
	assert(pq);
	assert(!QueueEmpty(pq));	//如果队空，是不能出队的

	//出队思想：有元素才能出队，更新队头，销毁原队头
	QNode* cur = pq->front;
	pq->front = pq->front->next;	//更新队头指针
	free(cur);
	cur = NULL;
	pq->size--;
}
QListDataType QueueFront(Queue* pq)	//查看队头元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->front->data;
}

// 二叉树节点个数
int BinaryTreeSize(struct TreeNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	//根节点+左右节点
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

bool isCompleteTree(struct TreeNode* root) {
	Queue Q;
	QueueInit(&Q);

	if (root)
		QueuePush(&Q, root);	//首先把根节点入队
	else
		return true;
	int treeSize = BinaryTreeSize(root);

	char* pc = (char*)calloc(sizeof(char), treeSize + 1);
	if (!pc)
	{
		perror("calloc::fail");
		exit(-1);
	}

	struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	assert(node);
	node->val = '\0';

	if (treeSize == 1)
		return true;

	if (!root->right)
		return false;

	int i = 0;
	while (i < treeSize)
	{
		struct TreeNode* tmp = QueueFront(&Q);
		QueuePop(&Q);
		pc[i++] = tmp->val;

		if (tmp->left)
			QueuePush(&Q, tmp->left);
		else
			QueuePush(&Q, node);
		if (tmp->right)
			QueuePush(&Q, tmp->right);
		else
			QueuePush(&Q, node);
	}

	QueueDestroy(&Q);

	int size = strlen(pc);
	return size == treeSize;
}