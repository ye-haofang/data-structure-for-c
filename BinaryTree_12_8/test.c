#define _CRT_SECURE_NO_WARNINGS 1
#include"BinaryTree.h"

//2022_12_10 隔离的第五天，二叉树博客已发布

void TestBinaryTree0()
{
	//注：当前的二叉树节点由自己手动进行申请、赋值、链接
	BTNode* N1 = (BTNode*)malloc(sizeof(BTNode));
	BTNode* N2 = (BTNode*)malloc(sizeof(BTNode));
	BTNode* N3 = (BTNode*)malloc(sizeof(BTNode));
	BTNode* N4 = (BTNode*)malloc(sizeof(BTNode));
	BTNode* N5 = (BTNode*)malloc(sizeof(BTNode));
	assert(N1 && N2 && N3 && N4 && N5);	//断言一下，不然VS会报警告

	N1->data = 'A', N2->data = 'B', N3->data = 'C', N4->data = 'D', N5->data = 'E';	//赋值
	N1->left = N2, N1->right = N3;
	N2->left = N4, N2->right = N5;
	N3->left = N3->right = NULL;
	N4->left = N4->right = NULL;
	N5->left = N5->right = NULL;	//手动链接

	printf("当前二叉树节点数为:%d\n", BinaryTreeSize(N1));
	BinaryTreePrevOrder(N1);	//前序
	printf("\n");
	BinaryTreeInOrder(N1);	//中序
	printf("\n");
	BinaryTreePostOrder(N1);	//后序
	printf("\n");

	BinaryTreeDestory(&N1);
}

void TestBinaryTree1()
{
	//第一组测试
	//构建、销毁、节点数、叶子节点数
	char a[] = "ABD##E#H##CF##G##";
	int len = strlen(a);
	int i = 0;
	BTNode* B1 = BinaryTreeCreate(a, len, &i);	//构建二叉树

	printf("节点数%d\n", BinaryTreeSize(B1));
	printf("叶子节点数%d\n", BinaryTreeLeafSize(B1));

	//BinaryTreeLevelOrder(B1);	//借助层序遍历测一下构建树是否正确

	BinaryTreeDestory(&B1);	//销毁
}

void TestBinaryTree2()
{
	//第二组测试
	//构建、深度、k层节点数、查找节点
	char a[] = "ABD##E#H##CF##G##";
	int len = strlen(a);
	int i = 0;
	BTNode* B2 = BinaryTreeCreate(a, len, &i);	//构建二叉树

	printf("深度%d\n", BinaryTreeDepth(B2));
	printf("%d层节点数%d\n", 3, BinaryTreeLevelKSize(B2, 3));
	printf("查找节点为：%p\n", BinaryTreeFind(B2, 'H'));	//已验证，查找正确

	//BinaryTreeLevelOrder(B2);	//借助层序遍历测一下构建树是否正确

	BinaryTreeDestory(&B2);	//销毁
}

void TestBinaryTree3()
{
	//第三组测试
	//构建、前序、中序、后序、层序、判断是否为完全二叉树
	char a[] = "ABD##E#H##CF##G##";
	//char a[] = "ABD##E##CF##G##";	//完全二叉树已过
	int len = strlen(a);
	int i = 0;
	BTNode* B3 = BinaryTreeCreate(a, len, &i);	//构建二叉树

	BinaryTreePrevOrder(B3);	//前序
	printf("\n");
	BinaryTreeInOrder(B3);	//中序
	printf("\n");
	BinaryTreePostOrder(B3);	//后序
	printf("\n");
	BinaryTreeLevelOrder(B3);	//层序遍历
	printf("\n");

	printf("是否为完全二叉树 %d\n", BinaryTreeComplete(B3));

	BinaryTreeDestory(&B3);	//销毁
}

int main()
{
	  TestBinaryTree0();
	return 0;
}