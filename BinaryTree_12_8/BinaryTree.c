#define _CRT_SECURE_NO_WARNINGS 1	
#include"BinaryTree.h"
#include"Queue.h"

// 通过前序遍历的数组"A B D # # E # H # # C F # # G # #"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	assert(a);

	//如果一开始就为 # 就没必要创建了
	if (a[*pi] == '#')
	{
		(*pi)++;	//向后移动，找到下一个值
		return NULL;
	}

	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	assert(node);

	node->data = a[(*pi)++];	//赋值，并向后移动

	node->left = BinaryTreeCreate(a, n, pi);	//左右链接
	node->right = BinaryTreeCreate(a, n, pi);

	return node;	//最开始的节点就是根节点
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	assert(root);	//传过来的这个二级指针不能为空

	//先销毁左孩子，再销毁右孩子，最后销毁根 ---> 后序
	if (!(*root))
		return;	//叶子，不必销毁

	//左右根
	BinaryTreeDestory(&((*root)->left));
	BinaryTreeDestory(&((*root)->right));	//取地址 --- 函数形参为二级指针

	free((*root));	//销毁当前节点
	(*root) = NULL;
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	//一级指针，不能断言，不然就无法递归

	if (!root)
		return 0;

	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);	//根 + 左 + 右
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (!root)
		return 0;

	//叶子节点：没有孩子的节点
	if (!root->left && !root->right)
		return 1;

	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

//二叉树的深度
int BinaryTreeDepth(BTNode* root)
{
	if (!root)
		return 0;

	//大问题化小问题：求左右子树的最大深度

	int leftDepth = BinaryTreeDepth(root->left);
	int rightDepth = BinaryTreeDepth(root->right);

	return (leftDepth > rightDepth ? leftDepth : rightDepth) + 1;	//左右根
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (!root)
		return 0;

	if (k == 1)
		return 1;	//第一层只有一个节点

	//统计第k层左右节点数之和
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (!root)
		return NULL;

	//根左右，前序寻找
	//找返回地址，终止递归

	if (root->data == x)
		return root;

	BTNode* left = BinaryTreeFind(root->left, x);
	BTNode* right = BinaryTreeFind(root->right, x);

	if (left)
		return left;
	else if (right)
		return right;
	else
		return false;
}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}

	//前序：根左右
	printf("%c ", root->data);

	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}

// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}

	//中序：左根右
	BinaryTreeInOrder(root->left);
	
	printf("%c ", root->data);

	BinaryTreeInOrder(root->right);
}

// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (!root)
	{
		printf("NULL ");
		return;
	}

	//后序：左右根
	BinaryTreePostOrder(root->left);
	BinaryTreePostOrder(root->right);

	printf("%c ", root->data);
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)	//需要借助队列
{
	if (!root)
	{
	    printf("队列为空!\n");
		return;
	}

	//思路：根节点先入队，出队时，带左右孩子入队（如果存在的话）
	//如此重复，直到队空
	Queue tmp;
	QueueInit(&tmp);
	QueuePush(&tmp, root);	//根节点入队
	
	while (!QueueEmpty(&tmp))
	{
		//取队头节点，出队
		QListDataType node = QueueFront(&tmp);
		QueuePop(&tmp);	//出队
		printf("%c ", node->data);	//打印元素

		//判断左右节点是否存在，存在就入队
		if (node->left)
			QueuePush(&tmp, node->left);
		if (node->right)
			QueuePush(&tmp, node->right);
	}

	QueueDestroy(&tmp);
}

// 判断二叉树是否是完全二叉树
bool BinaryTreeComplete(BTNode* root)
{
	//空树也是完全二叉树
	if (!root)
		return true;

	//完全二叉树一定是有序的，需要利用层序遍历思想
	//循环节点次
	//取节点的时候，如果节点为空，说明不是完全二叉树
	Queue tmp;
	QueueInit(&tmp);
	QueuePush(&tmp, root);	//根节点入队
    
	int countNode = BinaryTreeSize(root);	//获取节点数

	while (countNode--)
	{
		QListDataType node = QueueFront(&tmp);
		QueuePop(&tmp);

		//如果取到空，说明不是完全二叉树
		if (!node)
			return false;

		//左右孩子都入队
		QueuePush(&tmp, node->left);
		QueuePush(&tmp, node->right);
	}

	QueueDestroy(&tmp);
	return true;
}