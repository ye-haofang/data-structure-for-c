# 注意：为了避免平台检测外链过多而引发限流，现将所有OJ试题链接挂在这里

- #### [965. 单值二叉树](https://leetcode.cn/problems/univalued-binary-tree/)

- #### [104. 二叉树的最大深度](https://leetcode.cn/problems/maximum-depth-of-binary-tree/)

- #### [100. 相同的树](https://leetcode.cn/problems/same-tree/)

- #### [144. 二叉树的前序遍历](https://leetcode.cn/problems/binary-tree-preorder-traversal/)

- #### [226. 翻转二叉树](https://leetcode.cn/problems/invert-binary-tree/)

- #### [101. 对称二叉树](https://leetcode.cn/problems/symmetric-tree/)

- #### [572. 另一棵树的子树](https://leetcode.cn/problems/subtree-of-another-tree/)

- [**KY11** **二叉树遍历**]([二叉树遍历_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/4b91205483694f449f94c179883c1fef?tpId=60&&tqId=29483&rp=1&ru=/activity/oj&qru=/ta/tsing-kaoyan/question-ranking))

- #### [110. 平衡二叉树](https://leetcode.cn/problems/balanced-binary-tree/)

- #### [958. 二叉树的完全性检验](https://leetcode.cn/problems/check-completeness-of-a-binary-tree/)



`感谢支持，理解万岁！`

