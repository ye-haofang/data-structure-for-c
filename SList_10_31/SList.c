#define _CRT_SECURE_NO_WARNINGS 1	
#include"SList.h"

SLT* CreateSList(int n)	//调试
{
	SLT* s = NULL, *tail = NULL;
	int i = 0;
	for (i = 0; i < n; i++)
	{
		SLT* Node = (SLT*)malloc(sizeof(SLT));
		assert(Node);
		Node->data = i;
		Node->next = NULL;
		if (!s)
			tail = s = Node;
		else
		{
			tail->next = Node;
			tail = Node;
		}
	}
	return s;
}

void SLTDestroy(SLT** pplist)	//销毁链表
{
	assert(*pplist && pplist);

	SLT* prev = NULL;
	SLT* cur = *pplist;
	while (cur)
	{
		prev = cur->next;
		free(cur);
		cur = prev;
	}

	*pplist = NULL;
}
void SLTPrint(SLT* plist)	//打印链表
{

	SLT* cur = plist;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

static SLT* buyNewNode(SLTDatatype x)	//买节点
{
	SLT* newNode = (SLT*)malloc(sizeof(SLT));
	assert(newNode);
	newNode->data = x;
	newNode->next = NULL;
	return newNode;	//购买成功，返回节点地址
}

//尾插与尾删
void SLTPushBack(SLT** pphead, SLTDatatype x)
{
	assert(pphead);

	SLT* newNode = buyNewNode(x);
	if (!(*pphead))
		*pphead = newNode;
	else
	{
		SLT* tail = *pphead;
		while (tail->next)
			tail = tail->next;
		tail->next = newNode;
	}
}

void SLTPopBack(SLT** pphead)
{
	assert(*pphead && pphead);

	SLT* prev = NULL;
	SLT* tail = *pphead;
	while (tail->next)
	{
		prev = tail;
		tail = tail->next;
	}
	free(tail);
	if (prev)
		prev->next = NULL;
	else
		*pphead = NULL;
}

//头插与头删
void SLTPushFront(SLT** pphead, SLTDatatype x)
{
	assert(pphead);

	SLT* newnode = buyNewNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SLTPopFront(SLT** pphead)
{
	assert(*pphead && pphead);
	SLT* cur = *pphead;
	cur = (*pphead)->next;
	free(*pphead);
	*pphead = cur;
}

//查找
SLT* SListFind(SLT* plist, SLTDatatype x)
{
	assert(plist);
	SLT* cur = plist;
	while (cur)
	{
		if (cur->data == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

//任意位置插入，后插
void SListInsertAfter(SLT** pphead, SLT* pos, SLTDatatype x)
{
	assert(SListFind(*pphead, pos->data) && pphead);

	SLT* newnode = buyNewNode(x);
	if (!(*pphead))
		*pphead = newnode;
	else
	{
		SLT* prev = pos->next;
		SLT* ptail = pos;
		ptail->next = newnode;
		newnode->next = prev;
	}
}

//任意位置删除，后删
void SListEraseAfter(SLT** pphead, SLT* pos)
{
	assert(*pphead && pphead);	//报错好像不是那么明显，比如直接传递空指针，断言并没有报错	
	assert(SListFind(*pphead, pos->data) && SListFind(*pphead, pos->data)->next);

	SLT* prev = pos->next->next;
	free(pos->next);
	pos->next = prev;
}