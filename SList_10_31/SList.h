#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLTDatatype;	//链表的数据类型
typedef struct SListNode
{
	SLTDatatype data;
	struct SListNode* next;
}SLT;

SLT* CreateSList(int n);	//调试

void SLTDestroy(SLT** pplist);	//销毁链表
void SLTPrint(SLT* plist);	//打印链表

//尾插与尾删
void SLTPushBack(SLT** pphead, SLTDatatype x);
void SLTPopBack(SLT** pphead);

//头插与头删
void SLTPushFront(SLT** pphead, SLTDatatype x);
void SLTPopFront(SLT** pphead);

//查找
SLT* SListFind(SLT* plist, SLTDatatype x);

//任意位置插入，后插
void SListInsertAfter(SLT** pphead, SLT* pos, SLTDatatype x);
//任意位置删除，后删
void SListEraseAfter(SLT** pphead, SLT* pos);