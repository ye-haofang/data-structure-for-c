#define _CRT_SECURE_NO_WARNINGS 1	
#include"SList.h"

void TestSList1()
{
	SLT* s1 = CreateSList(5);
	SLTPushBack(&s1,5);
	SLTPushBack(&s1,6);
	SLTPushBack(&s1,7);
	SLTPrint(s1);

	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);
	SLTPopBack(&s1);	//成功报错

	SLTPrint(s1);

	SLTDestroy(&s1);
}

void TestSList2()
{
	SLT* s1 = CreateSList(5);
	SLTPushFront(&s1, 1);
	SLTPushFront(&s1, 1);
	SLTPushFront(&s1, 1);
	SLTPrint(s1);

	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);
	SLTPopFront(&s1);	//成功报错
	SLTPrint(s1);

	SLTDestroy(&s1);
}

void TestSList3()
{
	SLT* s1 = CreateSList(5);
	printf("%p\n", SListFind(s1, 3));
	printf("%p\n", SListFind(s1, 5));

	SListInsertAfter(&s1, s1->next, 100);
	SListInsertAfter(&s1, s1->next->next, 200);
	SListInsertAfter(&s1, s1->next->next->next, 300);
	SLTPrint(s1);

	SListEraseAfter(&s1, s1);
	SListEraseAfter(&s1, s1->next);
	SListEraseAfter(&s1, s1->next->next);
	SLTPrint(s1);

	//SListEraseAfter(NULL, s1);	//直接传递空指针还不会报错

	SLTDestroy(&s1);
}

void menu()
{
	printf("0.销毁   1.打印\n");
	printf("2.尾插   3.尾删\n");
	printf("4.头插   5.头删\n");
	printf("6.任意插 7.任意删\n");
	printf("8.查找   9.多组输入(尾插)\n");
}
int main()
{
	//TestSList1();
	int input = 1;
	SLT* s = NULL;
	while (input)
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		SLTDatatype x = 0;
		SLTDatatype y = 0;
		switch (input)
		{
		case 0:
			SLTDestroy(&s);
			break;
		case 1:
			SLTPrint(s);
			break;
		case 2:
			scanf("%d", &x);
			SLTPushBack(&s, x);
			break;
		case 3:
			SLTPopBack(&s);
			break;
		case 4:
			scanf("%d", &x);
			SLTPushFront(&s, x);
			break;
		case 5:
			SLTPopFront(&s);
			break;
		case 6:
			scanf("%d %d", &y, &x);
			SListInsertAfter(&s, SListFind(s, y), x);
			break;
		case 7:
			scanf("%d", &y);
			SListEraseAfter(&s, SListFind(s, y));
			break;
		case 8:
			scanf("%d", &x);
			printf("地址为:%p\n", SListFind(s, x));
			break;
		case 9:
			printf("多组输入，键入三次 Ctrl + Z 结束\n");
			while (scanf("%d", &x) != EOF)
				SLTPushBack(&s, x);
			break;
		default :
			printf("选择错误\n");
			break;
		}
	}
	return 0;
}