#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//力扣 20.有效的括号
bool isValid(char* s)
{
    assert(s);

    ST s1;
    STInit(&s1);
    //左扩号入栈，右括号出栈+匹配
    while (*s)
    {
        if ((*s == '{') || (*s == '[') || (*s == '('))
            STPush(&s1, *s);
        else
        {
            if (STEmpty(&s1))
            {
                STDestroy(&s1);
                return false;
            }
            char tmp = STTop(&s1);
            STPop(&s1);
            if ((*s == '}' && tmp != '{')
                || (*s == ']' && tmp != '[')
                || (*s == ')' && tmp != '('))
            {
                STDestroy(&s1);
                return false;
            }
        }
        s++;
    }

    //如果出来的时候，栈为空说明已全部匹配
    if (STEmpty(&s1))
    {
        STDestroy(&s1);
        return true;
    }
    else
    {
        STDestroy(&s1);
        return false;
    }
}