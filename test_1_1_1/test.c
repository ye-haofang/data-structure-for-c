#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>


//面试题 05.07. 配对交换

int exchangeBits(int num) {
    //二进制的奇偶位交换

    //先取全是奇位的二进制，然后左移
    int odd = num & 1431655765;
    odd <<= 1;

    //再去全是偶位的二进制，然后右移
    int even = num & 2863311530;
    even >>= 1;


    num = odd | even;

    return num;
}

int main()
{
    exchangeBits(3);
    return 0;
}