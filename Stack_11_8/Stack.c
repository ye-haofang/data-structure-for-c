#define _CRT_SECURE_NO_WARNINGS 1	
#include"Stack.h"

void STInit(ST* ps, int capacity = 4)	//初始化
{
	assert(ps);

	//假如没传参数，默认大小为4，传了就按实际大小初始化栈
	ps->data = (STDataType*)malloc(sizeof(STDataType) * capacity);
	if (ps->data == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	ps->pos = 0;	//这里默认 pos-1 为栈顶元素
	ps->capacity = capacity;	//缺省参数的实际运用
}
void STDestroy(ST* ps)	//销毁
{
	assert(ps);

	free(ps->data);
	ps->data = NULL;
	ps->pos = ps->capacity = 0;
}

void STPush(ST* ps, STDataType x)	//入栈
{
	assert(ps);

	if (ps->pos == ps->capacity)
	{
		STDataType* tmp = (STDataType*)realloc(ps->data, sizeof(STDataType) * ps->capacity * DEFAULT_MUL);
		if (tmp == NULL)
		{
			perror("realloc fail");
			exit(-1);
		}

		ps->data = tmp;
		ps->capacity *= DEFAULT_MUL;
	}

	ps->data[ps->pos++] = x;	//入栈完成
}

void STPop(ST* ps)	//出栈
{
	assert(ps);
	assert(ps->pos > 0);	//有元素，才能出栈

	ps->pos--;
}

STDataType STTop(ST* ps)	//获取栈顶元素
{
	assert(ps);
	assert(ps->pos > 0);	//至少有一个元素，才能获取

	return ps->data[ps->pos - 1];
}

size_t STSize(ST* ps)	//获取栈大小
{
	assert(ps);

	return (size_t)ps->pos;
}

bool STEmpty(ST* ps)	//判断栈空
{
	assert(ps);

	return ps->pos == 0;
}