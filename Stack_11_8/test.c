#define _CRT_SECURE_NO_WARNINGS 1
#include"Stack.h"

void TestStack1()
{
	ST s;
	STInit(&s);

	STPush(&s, 1);
	STPush(&s, 2);
	STPush(&s, 3);
	STPush(&s, 4);
	STPush(&s, 5);
	printf("当前栈顶元素为%d 当前栈大小为%d\n", STTop(&s), STSize(&s));

	STPop(&s);
	STPop(&s);
	STPop(&s);
	STPop(&s);
	printf("当前栈顶元素为%d 当前栈大小为%d\n", STTop(&s), STSize(&s));

	STPop(&s);
	printf("当前栈大小为%d\n", STSize(&s));

	//STPop(&s);	//成功报错

	if (STEmpty(&s))
		printf("栈空\n");
	else
		printf("栈不空\n");

	STDestroy(&s);
}

int main()
{
	TestStack1();
	return 0;
}