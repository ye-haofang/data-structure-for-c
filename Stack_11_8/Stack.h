#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

#define DEFAULT_SIZE 5	//默认大小为5
#define DEFAULT_MUL 2	//默认扩大的倍数为2

typedef int STDataType;

typedef struct StackInfo
{
	STDataType* data;	//数据
	int pos;	//栈顶
	int capacity;	//容量
}ST;

void STInit(ST* ps);	//初始化
void STDestroy(ST* ps);	//销毁

void STPush(ST* ps, STDataType x);	//入栈
void STPop(ST* ps);	//出栈
STDataType STTop(ST* ps);	//获取栈顶元素
size_t STSize(ST* ps);	//获取栈大小
bool STEmpty(ST* ps);	//判断栈空