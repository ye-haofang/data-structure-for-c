#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<windows.h>
#include<stdbool.h>

//typedef int SLDataType;	//顺序表内元素的类型_整型版

//typedef float SLDataType;	//顺序表内元素的类型_浮点数版

typedef char SLDataType;	//顺序表内元素的类型_字符版

//顺序表主体框架，动态版
typedef struct SeqListInfo
{
	SLDataType* data;	//数据域
	size_t size;	//有效数据数
	size_t capacity;	//顺序表容量
}SL;

void SLInit(SL* ps);	//初始化顺序表
void SLDestroy(SL* ps);	//销毁顺序表

void SLPrint(SL* ps);	//打印顺序表

void SLPushBack(SL* ps, SLDataType x);	//尾插
void SLPopBack(SL* ps);	//尾删

void SLPushFront(SL* ps, SLDataType x);	//头插
void SLPopFront(SL* ps);	//头删

bool SLFind(SL* ps);	//查找，按元素或按下标

void SLModify(SL* ps);	//修改

void SLInsert(SL* ps, SLDataType x);	//任意位置插入
void SLErase(SL* ps);	//任意位置删除

size_t SLSize(SL* ps);	//统计当前顺序表大小

void SLPurge(SL* ps);	//清除所有元素

SLDataType IO(void);	//输入函数