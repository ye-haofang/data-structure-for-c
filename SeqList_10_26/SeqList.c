#define _CRT_SECURE_NO_WARNINGS 1	
#include"SeqList.h"

void SLInit(SL* ps)	//初始化顺序表
{
	ps->data = NULL;	//指向空地址处
	ps->size = ps->capacity = 0;	//归零
}

void SLDestroy(SL* ps)	//销毁顺序表
{
	//释放空间，指针置空
	free(ps->data);
	ps->data = NULL;
}

void SLPrint(SL* ps)	//打印顺序表
{
	if (ps->size == 0)
	{
		printf("当前顺序表中没有信息\n");
	}
	else
	{
		for (size_t i = 0; i < ps->size; i++)
			//printf("%d ", ps->data[i]);	//整型版
			//printf("%.2f ", ps->data[i]);	//浮点数版
			printf("%c ", ps->data[i]);	//字符版
		printf("\n当前顺序表信息如上\n");
	}
}

static void buyCapacity(SL* ps)	//检查容量，只能在这里使用
{
	//插入前要先判断容量
	if (ps->size == ps->capacity)
	{
		size_t newCapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;	//两倍扩大
		SLDataType* tmp = (SLDataType*)realloc(ps->data, sizeof(SLDataType) * newCapacity);
		if (!tmp)
		{
			perror("realloc fail::tmp");
			exit(-1);	//退出程序
		}
		ps->data = tmp;
		ps->capacity = newCapacity;	//更新信息

		printf("扩容成功\n");
	}
}

void SLPushBack(SL* ps, SLDataType x)	//尾插
{
	buyCapacity(ps);

	ps->data[ps->size++] = x;
	printf("尾插成功\n");
}

void SLPopBack(SL* ps)	//尾删
{
	assert(ps->size > 0);	//如果有效数据为0，就无法删除
	ps->size--;	//有效数字-1

	printf("尾删成功\n");
}

void SLPushFront(SL* ps, SLDataType x)	//头插
{
	buyCapacity(ps);	//检查容量
	size_t i = ps->size;
	for (; i > 0; i--)
		ps->data[i] = ps->data[i - 1];
	ps->data[i] = x;
	ps->size++;

	printf("头插成功\n");
}

void SLPopFront(SL* ps)	//头删
{
	assert(ps->size > 0);	//必须有元素可删
	size_t i = 0;
	for (; i < ps->size - 1; i++)
		ps->data[i] = ps->data[i + 1];
	ps->size--;

	printf("头删成功\n");
}

int POS = -1;	//全局变量

void SLFindByPos(SL* ps, bool* flag)	//按下标查找
{
	size_t pos = 0;
	printf("请输入下标:");
	scanf("%zu", &pos);

	if (pos >= ps->size)
		printf("这个下标不存在\n");
	else
	{
		//printf("找到了，%zu下标对应的元素为%d\n", pos, ps->data[pos]);	//整型版

		//printf("找到了，%zu下标对应的元素为%.2f\n", pos, ps->data[pos]);	//浮点数版

		printf("找到了，%zu下标对应的元素为%c\n", pos, ps->data[pos]);		//字符版
		*flag = true;
		POS = pos;
	}
}

void SLFindByElement(SL* ps, bool* flag)	//按元素值查找
{
	SLDataType tmp = 0;
	printf("请输入元素值:");
	//scanf("%d", &tmp);	//整型版

	//scanf("%f", &tmp);	//浮点数版

	getchar();	//字符版，吞掉多余的字符
	scanf("%c", &tmp);	//字符版

	size_t i = 0;
	for (; i < ps->size; i++)
	{
		if (tmp == ps->data[i])
		{
			//printf("找到了，元素%d第一次出现的下标是%zu\n", tmp, i);	//整型版

			//printf("找到了，元素%.2f第一次出现的下标是%zu\n", tmp, i); //浮点数版

			printf("找到了，元素%c第一次出现的下标是%zu\n", tmp, i);	//字符版
			*flag = true;
			POS = i;
			return;
		}
	}

	printf("没有找到这个元素\n");
}

bool SLFind(SL* ps)	//查找，按元素或按下标
{
	size_t input = 0;
	printf("按下标或元素值查找(1/2):");
	scanf("%zu", &input);
	bool flag = false;

	if (input == 1)
		SLFindByPos(ps, &flag);	//按下标查找
	else if (input == 2)
		SLFindByElement(ps, &flag);	//按元素值查找
	else
		printf("输错了，重新来吧\n");

	return flag;
}

void SLModify(SL* ps)	//修改
{
	if (SLFind(ps) == false)
		printf("元素不存在，无法修改\n");
	else
	{
		SLDataType tmp = 0;
		printf("请输入你想修改的值:");
		//scanf("%d", &tmp);	//整型版

		//scanf("%f", &tmp);	//浮点数版

		getchar();	//字符版，吞字符
		scanf("%c", &tmp);	//字符版

		ps->data[POS] = tmp;
		printf("修改成功\n");

		POS = -1;
	}
}

void SLInsert(SL* ps, SLDataType x)	//任意位置插入
{
	if (SLFind(ps) == false)
		printf("元素不存在，无法插入\n");
	else
	{
		if ((size_t)POS == 0)
			SLPushFront(ps, x);
		else
		{
			size_t i = ps->size;
			for (; i > (size_t)POS; i--)
				ps->data[i] = ps->data[i - 1];

			ps->data[POS] = x;
			ps->size++;
			printf("插入成功\n");
		}
		POS = -1;
	}
}

void SLErase(SL* ps)	//任意位置删除
{
	if (SLFind(ps) == false)
		printf("元素不存在，无法删除\n");
	else
	{
		size_t i = (size_t)POS;
		for (; i < ps->size - 1; i++)
			ps->data[i] = ps->data[i + 1];
		ps->size--;
		printf("删除成功\n");
		POS = -1;
	}
}

size_t SLSize(SL* ps)	//统计当前顺序表大小
{
	printf("当前顺序表内共有%d个有效元素\n", ps->size);
	return ps->size;
}

void SLPurge(SL* ps)	//清除所有元素
{
	assert(ps->size > 0);	//没有元素就无法清除
	ps->size = 0;	//直接赋0
	printf("成功清空\n");
}

SLDataType IO(void)	//输入函数
{
	SLDataType n = 0;
	printf("请输入一个值:>");
	//scanf("%d", &n);	//整型版
	
	//scanf("%f", &n);	//浮点数版

	getchar();	//字符版，吞字符
	scanf("%c", &n);	//字符版
	return n;
}