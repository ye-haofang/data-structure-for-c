#define _CRT_SECURE_NO_WARNINGS 1
//顺序表
//包含基础的增删查改，任意位置插入删除，大小统计

#include"SeqList.h"

void TestSeqList1(void)
{
	SL s1;
	SLInit(&s1);

	SLPushBack(&s1, 1);
	SLPushBack(&s1, 2);
	SLPushBack(&s1, 3);

	
	SLPrint(&s1);

	SLPopBack(&s1);
	SLPopBack(&s1);

	SLPrint(&s1);

	SLPopBack(&s1);
	SLPrint(&s1);

	SLPopBack(&s1);	//测试边界，成功报错

	SLDestroy(&s1);
}

void TestSeqList2(void)	//测试为头插和头删
{
	SL s2;
	SLInit(&s2);

	SLPushFront(&s2, 1);
	SLPushFront(&s2, 2);
	SLPushFront(&s2, 3);
	SLPrint(&s2);

	SLPopFront(&s2);
	SLPopFront(&s2);
	SLPrint(&s2);

	SLPopFront(&s2);
	SLPrint(&s2);

	SLPopFront(&s2);	//测试边界 成功报错
	SLPrint(&s2);

	SLDestroy(&s2);
}

void TestSeqList3(void)	//测试查找与修改
{
	SL s3;
	SLInit(&s3);

	SLPushBack(&s3, 1);
	SLPushBack(&s3, 2);
	SLPushBack(&s3, 3);
	SLPushFront(&s3, 4);
	SLPushFront(&s3, 5);
	SLPushFront(&s3, 6);
	SLPrint(&s3);

	SLPopBack(&s3);
	SLPopFront(&s3);
	SLPrint(&s3);

	SLFind(&s3);

	SLModify(&s3);

	SLPrint(&s3);

	SLDestroy(&s3);
}

void TestSeqList4(void)	//测试剩下功能
{
	SL s4;
	SLInit(&s4);

	SLPushBack(&s4, 1);
	SLPushBack(&s4, 2);
	SLPushBack(&s4, 3);
	SLPushFront(&s4, 4);
	SLPushFront(&s4, 5);
	SLPushFront(&s4, 6);
	SLPrint(&s4);

	SLInsert(&s4, 10);
	SLInsert(&s4, 100);
	SLPrint(&s4);

	SLErase(&s4);
	SLErase(&s4);
	SLPrint(&s4);

	SLSize(&s4);

	SLPurge(&s4);
	SLPrint(&s4);
	SLSize(&s4);

	SLDestroy(&s4);
}

void menu()
{
	printf("******************************\n");
	printf("******	0.退出	1.打印	*******\n");
	printf("******	2.尾插	3.尾删	******\n");
	printf("******	4.头插	5.头删	******\n");
	printf("******	6.查找	7.修改	******\n");
	printf("******  8.任意插 9.任意删******\n");
	printf("******	10.大小	11.清空	******\n");
	printf("******************************\n");

}
enum Menu
{
	退出,
	打印,
	尾插,
	尾删,
	头插,
	头删,
	查找,
	修改,
	任意插,
	任意删,
	大小,
	清空
};
void Test(void)	//主测试函数
{
	int input = 1;
	SL s;
	SLInit(&s);
	while (input)
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case 退出:
			SLDestroy(&s);
			printf("顺序表已销毁，安全退出\n");
			break;
		case 打印:
			SLPrint(&s);
			break;
		case 尾插:
			SLPushBack(&s, IO());
			break;
		case 尾删:
			SLPopBack(&s);
			break;
		case 头插:
			SLPushFront(&s, IO());
			break;
		case 头删:
			SLPopFront(&s);
			break;
		case 查找:
			SLFind(&s);
			break;
		case 修改:
			SLModify(&s);
			break;
		case 任意插:
			SLInsert(&s, IO());
			break;
		case 任意删:
			SLErase(&s);
			break;
		case 大小:
			SLSize(&s);
			break;
		case 清空:
			SLPurge(&s);
			break;
		default :
			printf("选择错误,请重新选择\n");
			break;
		}
	}
}
int main()
{
	//TestSeqList1();	//测试为尾插和尾删
	//TestSeqList2();	//测试为头插和头删
	//TestSeqList3();	//测试查找与修改
	//TestSeqList4();	//测试剩下功能
	Test();
	return 0;
}