#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<math.h>

//9. 回文数
//https://leetcode.cn/problems/palindrome-number/

bool isPalindrome(int x) {

    if (x < 0)
        return false;

    //获取位数
    int numDig = 0;
    int tmp = x;
    while (tmp)
    {
        numDig++;
        tmp /= 10;
    }

    //对比
    tmp = x;
    int dig = numDig;
    int zero = 0;
    while (tmp > 9)
    {
        int left = tmp / (int)pow(10, numDig - 1);

        if (dig != numDig)
            left = 0, zero--;

        int right = tmp % 10;
        if (left != right)
            return false;

        tmp %= (int)pow(10, numDig - 1);
        tmp /= 10;

        numDig -= 2;
        dig = 0;
        int ttmp = tmp;
        while (ttmp)
        {
            dig++;
            ttmp /= 10;
        }

        if(!zero && numDig > dig && tmp)
            zero = numDig - dig;
    }

    //得出结论

    return !zero && (numDig <= 2 || !tmp);
}

int main()
{
    int n = isPalindrome(1000030001);
    return 0;
}