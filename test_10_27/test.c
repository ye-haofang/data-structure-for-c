#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<string.h>
#include<assert.h>

//int main()
//{
//	char* str = "abccd";
//	char* ptr = "aba";
//	while (*str != '\0')
//	{
//		char* temps = str;
//		char* tempp = ptr;
//		while (*tempp == *temps && *temps != '\0')
//		{
//			if (*tempp == *temps)
//			{
//				tempp++;
//				temps++;
//			}
//			else
//				break;
//		}
//		if (*tempp == '\0')
//		{
//			printf("找到了\n");
//			return 0;
//		}
//		str++;
//	}
//	printf("没找到\n");
//	return 0;
//}

////模拟实现strstr
//#include<string.h>
//#include<assert.h>
//char* myStrstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//
//	//排除特殊情况
//	//情况1，str1为空串，str2也为空串
//	//情况2，str1为空串，str2不为空串，此时是肯定找不到的
//	if (!*str1)
//	{
//		
//		if (!*str2)	//情况1
//			return (char*)str1;	//找到了，直接返回str1就行了，因为此时两个串相同，即都是空串
//		else	//情况2
//			return NULL;	//返回空指针，反正肯定没找到
//	}
//
//	char* s1 = (char*)str1;	//随便找两个指针指一下
//	char* s2 = (char*)str2;
//	while (*s1)
//	{
//		char* ps1 = s1;	//指向str1
//		char* ps2 = s2;	//指向str2
//		//查找策略：将str2逐字符与str1进行比较
//		while (*ps2)
//		{
//			if (*ps1 != *ps2)	//如果不相同，可以直接结束了
//				break;
//			ps1++;
//			ps2++;
//		}
//		if (*ps2 == '\0')	//如果是正常退出，即找到的情况，此时ps2肯定是指向'\0'的
//			return s1;	//需要返回s1，而不是ps1，因为此时ps1已经移动过了
//		s1++;	//没找到，s1++，指向str1的下一个字符，修正比较路线
//	}
//	return NULL;	//没找到的情况，返回空指针
//}
//int main()
//{
//	char* str1 = "s";
//	char* str2 = "";
//	char* ps = myStrstr(str1, str2);
//	char* pm = strstr(str1, str2);
//	printf("%s\n%s\n", ps, pm);
//	return 0;
//}