#define _CRT_SECURE_NO_WARNINGS 1	

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

struct ListNode* mergeInBetween(struct ListNode* list1, int a, int b, struct ListNode* list2) {
    //notch1 指向链表1的左断层处，需要与链表2的起始位置链接，即 notch1->notch11
    //notch2 指向链表2的末尾处(非空)，需要与链表1的右断层处链接，即 notch2->notch22
    //目标：使 notch1、notch11、notch2、notch22 指向正确位置，然后更改链接关系即可

    struct ListNode* notch1, * notch11, * notch2, * notch22;
    notch1 = notch22 = list1;

    //notch11 位置已确认
    notch11 = notch2 = list2;

    //找到 notch2 的位置
    while (notch2->next)
        notch2 = notch2->next;

    //链表1走a - 1 步找到 notch1 的位置
    while (--a)
        notch1 = notch1->next;

    //链表1走 b + 1 步找到 notch22 的位置
    while (b >= 0)
    {
        notch22 = notch22->next;
        b--;
    }

    //更改链接关系
    notch1->next = notch11, notch2->next = notch22;

    return list1;
}