#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdbool.h>


////暴力遍历
//bool isIdealPermutation(int* nums, int numsSize) {
//    int i = 0;
//    int j = 0;
//    int inver = 0;  //倒置值
//    for (; i < numsSize; i++)
//    {
//        for (j = i + 1; j < numsSize; j++)
//        {
//            if (nums[i] > nums[j])
//                inver++;
//        }
//        if (i < numsSize - 1)
//        {
//            if (nums[i] > nums[i + 1])
//                inver--;
//        }
//    }
//    return inver == 0;
//}

//遍历
//空间换时间
//一个数字一个位
#include<stdlib.h>
#include<assert.h>
bool isIdealPermutation(int* nums, int numsSize) {
    int* pa = (int*)calloc(numsSize, sizeof(int));
    assert(pa);

    int i = 0;
    int j = 0;
    int inver = 0;
    int lowSum = 0; //(首项+尾项)*项数 / 2
    for (i = 0; i < numsSize; i++)
    {
        lowSum = (nums[i] - 1) * nums[i] / 2;
        for (j = 0; j < nums[i]; j++)
        {
            if (pa[j] == -1)
                lowSum--;
        }
        if (nums[i] == 0)
            continue;
        for (j = i + 1; j < numsSize; j++)
        {
            if (lowSum < 0)
                break;

            if (nums[i] > nums[j])
            {
                inver++;
                lowSum -= (nums[j] == 0 ? 1 : nums[j]);
                if ((j - i) == 1)
                    inver--;
            }
        }
        pa[nums[i]] = -1;   //出现过了
    }

    return inver == 0;
}
int main()
{
    int arr[] = { 1,2,0 };
    if (isIdealPermutation(arr, 3))
    {
        printf("true");
    }
	return 0;
}