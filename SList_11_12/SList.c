#define _CRT_SECURE_NO_WARNINGS 1	
#include"SList.h"

//不带哨兵位的单链表不需要初始化
void SLTDestroy(SLT** pphead)	//单链表的销毁
{
	assert(pphead);	//一二级指针都不能为空

	//空表不走销毁程序，但不能报错
	if (*pphead)
	{
		while (*pphead)
		{
			SLT* cur = (*pphead)->next;	//记录头的下一个位置
			free(*pphead);
			*pphead = cur;	//向后移动，不断销毁
		}
	}
}

void SLTPrint(const SLT** pphead)	//单链表的打印
{
	assert(pphead);	//不需要断言 *pphead ，因为存在空链表打印的情况，是合法的

	printf("\n\n当前链表为: ");
	const SLT* cur = *pphead;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;	//cur要向后移动
	}

	printf("NULL\n\n\n");	//链表最后为空
}

static SLT* buyNode(const SLTDataType x)	//买节点
{
	SLT* newnode = (SLT*)malloc(sizeof(SLT));
	assert(newnode);	//防止申请失败的情况

	newnode->data = x;
	newnode->next = NULL;

	return newnode;	//返回买好的节点
}

void SLTPushBack(SLT** pphead, const SLTDataType x)	//尾插
{
	assert(pphead);

	SLT* newnode = buyNode(x);
	SLT* tail = *pphead;
	//尾插分情况，链表为空，直接赋值，不为空，找到尾，再链接
	if (tail == NULL)
	{
		*pphead = tail = newnode;	//直接赋值
	}
	else
	{
		while (tail->next != NULL)
		{
			tail = tail->next;	//找到尾节点
		}
		tail->next = newnode;	//链接
	}

	//SLTInsert(pphead, NULL, x);		//可以复用任意位置前插
}

void SLTPopBack(SLT** pphead)	//尾删
{
	assert(pphead);
	assert(*pphead);	//如果链表为空，是删不了的

	SLT* tail = *pphead;
	SLT* prev = NULL;
	while (tail->next)
	{
		prev = tail;	//保存上一个节点信息
		tail = tail->next;	//找到尾节点
	}

	free(tail);
	
	//分情况，如果prev是空，说明删除的尾节点同时也是头节点
	if (prev)
		prev->next = tail = NULL;	//把尾节点的上一个节点指向空
	else
		*pphead = NULL;	//此时直接把prev置空

	/*SLT* tail = *pphead;
	while (tail->next)
	{
		tail = tail->next;
	}
	SLTErase(pphead, tail);*/	//可以复用任意位置删除
}

void SLTPushFront(SLT** pphead, const SLTDataType x)	//头插
{
	assert(pphead);

	SLT* newhead = buyNode(x);
	newhead->next = *pphead;	//直接链接
	*pphead = newhead;	//更新头节点信息
	//代码简洁之道
	
	//SLTInsert(pphead, *pphead, x);	//可以复用任意位置前插
}

void SLTPopFront(SLT** pphead)	//头删
{
	assert(pphead);
	assert(*pphead);	//头删同样存在空不能删的情况

	//先找到头的下一个节点，然后赋值新头
	SLT* cur = *pphead;	//指向头节点
	SLT* newhead = cur->next;	//保存头节点的下一个节点信息

	free(cur);
	cur = NULL;

	*pphead = newhead;	//赋值新头

	//SLTErase(pphead, *pphead);	//可以复用任意位置删除
}

//这两个有缺陷，不能对头节点进行操作，但实现起来比较简单
void SLTInsertAfter(SLT* nodeAfter, const SLTDataType x)	//任意插，后插法
{
	assert(nodeAfter);

	SLT* cur = nodeAfter;
	SLT* newnode = buyNode(x);
	SLT* tail = cur->next;	//先保存被插入节点的下一个节点信息
	
	//更改链接关系，后插完成
	cur->next = newnode;
	newnode->next = tail;
}

void SLTEraseAfter(SLT* nodeAfter)	//任意删，删除后面节点
{
	assert(nodeAfter);
	assert(nodeAfter->next);	//如果目标节点的下一个为空，是后删不了的

	SLT* cur = nodeAfter;
	SLT* tail = cur->next;	//待删除的节点
	SLT* newtail = tail->next;	//新尾

	free(tail);
	tail = NULL;

	cur->next = newtail;	//更改链接关系
}

//这两个实现起来比较麻烦，但是很全能
void SLTInsert(SLT** pphead, SLT* node, const SLTDataType x)	//任意插，前插法
{
	assert(pphead);

	SLT* newnode = buyNode(x);
	SLT* cur = *pphead;
	SLT* prev = NULL;
	while (cur != node)
	{
		prev = cur;	//要找到目标节点的上一个节点
		cur = cur->next;	
	}

	//判断一下，是否为空表插入
	//走的是尾插的那一套思想
	if (prev)
	{
		prev->next = newnode;
		newnode->next = node;
	}
	else
	{
		newnode->next = node;
		*pphead = newnode;	//空表需要更新头节点信息
	}
}

void SLTErase(SLT** pphead, SLT* node)	//任意删，删除当前节点
{
	assert(pphead);
	assert(node);	//不必检查*pphead的合法性，查验node就行了

	SLT* cur = *pphead;	//走的是尾删的那一套思想
	SLT* prev = NULL;
	SLT* tail = node->next;
	while (cur != node)
	{
		prev = cur;
		cur = cur->next;
	}

	free(node);
	//跟尾插一样，需要判断一下
	if (prev)
		prev->next = tail;
	else
		*pphead = tail;
}


SLT* SLTFind(const SLT** pphead, const SLTDataType x)	//查找值为x的节点(第一次出现)
{
	assert(pphead);

	SLT* cur = (SLT*)*pphead;	//指向头节点
	while (cur)
	{
		if (cur->data == x)
			return cur;	//找到了，返回相关节点信息
		cur = cur->next;
	}

	return NULL;	//没有找到的情况
}

void SLTModify(SLT* node, const SLTDataType val)	//修改 node 节点处的值
{
	//注意：在调用函数时，可以通过链式访问，将查找函数的返回值作为形参一传入就行了
	assert(node);	//断言，如果节点node是空指针，是不能做修改的

	node->data = val;
}

bool SLTEmpty(const SLT** pphead)	//判断当前链表是否为空
{
	assert(pphead);

	return *pphead == NULL;	//如果为空，返回true
}