#define _CRT_SECURE_NO_WARNINGS 1	
#include"SList.h"

void TestDemo(void)
{
	SLT* s = NULL;
	int i = 0;

	for (i = 0; i < 3; i++)
	{
		SLTPushBack(&s, i);
		SLTPrint(&s);
	}

	for (i = 0; i < 3; i++)
	{
		SLTPopBack(&s);
		SLTPrint(&s);
	}

	for (i = 0; i < 3; i++)
	{
		SLTPushFront(&s, i);
		SLTPrint(&s);
	}

	for (i = 0; i < 2; i++)
	{
		SLTPopFront(&s);
		SLTPrint(&s);
	}

	SLTInsertAfter(SLTFind(&s, 0), 100);
	SLTPrint(&s);
	SLTInsertAfter(SLTFind(&s, 100), 200);
	SLTPrint(&s);

	SLTEraseAfter(SLTFind(&s, 0));
	SLTPrint(&s);

	SLTInsert(&s, NULL, 300);
	SLTPrint(&s);
	SLTErase(&s, SLTFind(&s, 0));
	SLTPrint(&s);

	SLTModify(SLTFind(&s, 200), 500);
	SLTPrint(&s);

	if (SLTEmpty(&s))
		printf("空\n");
	else
		printf("非空\n");
	SLTDestroy(&s);
}

void menu()
{
	printf("0.退出\t\t\t1.打印\n");
	printf("2.尾插\t\t\t3.尾删\n");
	printf("4.头插\t\t\t5.头删\n");
	printf("6.任意插(后插)\t 7.任意删(后删)\n");
	printf("8.任意插(前插)\t 9.任意删(当前)\n");
	printf("10.查找\t\t\t11.修改\n");
}
int main()
{
	//TestDemo();
	SLT* s = NULL;
	int input = 1;
	while (input)
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");
		int val = 0;	//待插入值
		int pos = 0;	//待查找节点值
		switch (input)
		{
		case 0:
			printf("成功退出\n");
			break;
		case 1:
			SLTPrint(&s);
			break;
		case 2:
			printf("请输入一个数:>");
			scanf("%d", &val);
			SLTPushBack(&s, val);
			break;
		case 3:
			SLTPopBack(&s);
			break;
		case 4:
			printf("请输入一个数:>");
			scanf("%d", &val);
			SLTPushFront(&s, val);
			break;
		case 5:
			SLTPopFront(&s);
			break;
		case 6:
			printf("请输入被插入的节点值:>");
			scanf("%d", &pos);
			printf("请输入一个数:>");
			scanf("%d", &val);
			SLTInsertAfter(SLTFind(&s, pos), val);
			break;
		case 7:
			printf("请输入被删除的节点值:>");
			scanf("%d", &pos);
			SLTEraseAfter(SLTFind(&s, pos));
			break;
		case 8:
			printf("请输入被插入的节点值:>");
			scanf("%d", &pos);
			printf("请输入一个数:>");
			scanf("%d", &val);
			SLTInsert(&s, SLTFind(&s, pos), val);
			break;
		case 9:
			printf("请输入被删除的节点值:>");
			scanf("%d", &pos);
			SLTErase(&s, SLTFind(&s, pos));
			break;
		case 10:
			printf("请输入被查找的节点值:>");
			scanf("%d", &pos);
			SLT* tmp = SLTFind(&s, pos);
			printf("当前节点的地址为:%p\n", tmp);
			break;
		case 11:
			printf("请输入被修改的节点值:>");
			scanf("%d", &pos);
			printf("请输入一个数:>");
			scanf("%d", &val);
			SLTModify(SLTFind(&s, pos), val);
			break;
		default :
			printf("选择错误，重新选择!\n");
			break;
		}
	}
	SLTDestroy(&s);
	return 0;
}