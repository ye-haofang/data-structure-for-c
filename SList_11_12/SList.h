#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<windows.h>

typedef int SLTDataType;	//单链表的数据类型

typedef struct SListNode
{
	SLTDataType data;	//数据域
	struct SListNode* next;	//指针域
}SLT;	//重命名为 SLT

void SLTDestroy(SLT** pphead);	//单链表的销毁
void SLTPrint(const SLT** pphead);	//单链表的打印

void SLTPushBack(SLT** pphead, const SLTDataType x);	//尾插
void SLTPopBack(SLT** pphead);	//尾删

void SLTPushFront(SLT** pphead, const SLTDataType x);	//头插
void SLTPopFront(SLT** pphead);	//头删

//这两个有缺陷，不能对头节点进行操作，但实现起来比较简单
void SLTInsertAfter(SLT* nodeAfter, const SLTDataType x);	//任意插，后插法
void SLTEraseAfter(SLT* nodeAfter);	//任意删，删除后面节点

//这两个实现起来比较麻烦，但是很全能
void SLTInsert(SLT** pphead, SLT* node, const SLTDataType x);	//任意插，前插法
void SLTErase(SLT** pphead, SLT* node);	//任意删，删除当前节点

SLT* SLTFind(const SLT** pphead, const SLTDataType x);	//查找值为x的节点(第一次出现)

void SLTModify(SLT* node, const SLTDataType val);	//修改 node 节点处的值

bool SLTEmpty(const SLT** pphead);	//判断当前链表是否为空