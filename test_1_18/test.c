#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//2293. 极大极小游戏
//https://leetcode.cn/problems/min-max-game/

int minMaxGame(int* nums, int numsSize) {
    int* newNums = (int*)malloc(sizeof(int) * (numsSize / 2));
    assert(newNums);

    while (numsSize != 1)
    {
        int pos = numsSize / 2;
        for (int i = 0; i < pos; i++)
        {
            if (i % 2 == 0)
            {
                int val = nums[i * 2];
                if (val > nums[i * 2 + 1])
                    val = nums[i * 2 + 1];
                nums[i] = val;
            }
            else
            {
                int val = nums[i * 2];
                if (val < nums[i * 2 + 1])
                    val = nums[i * 2 + 1];
                nums[i] = val;
            }
        }

        numsSize = pos;
    }

    return nums[0];
}

int main()
{
    int arr[] = { 1,3,5,2,4,8,2,2 };
    minMaxGame(arr, sizeof(arr) / sizeof(arr[0]));
    return 0;
}