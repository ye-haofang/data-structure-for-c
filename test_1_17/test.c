#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdbool.h>

////1813. 句子相似性 III
////https://leetcode.cn/problems/sentence-similarity-iii/
//
//bool areSentencesSimilar(char* sentence1, char* sentence2) {
//    char* begin1, * begin2;
//    begin1 = sentence1, begin2 = sentence2;
//
//    char* end1 = sentence1;
//    while (*(end1 + 1))
//        end1++;
//
//    char* end2 = sentence2;
//    while (*(end2 + 1))
//        end2++;
//
//    bool flag1 = true;
//    bool flag2 = true;
//    int space = 0;
//
//    while ((*begin1 && *begin2) && *begin1 == *begin2)
//    {
//        begin1++;
//        begin2++;
//
//        if (*begin1 == ' ' || *begin2 == ' ')
//            space++;
//    }
//
//    if (!space)
//        flag1 = false;
//
//    space = 0;
//    while ((end1 >= sentence1 && end2 >= sentence2) && *end1 == *end2)
//    {
//        end1--;
//        end2--;
//
//        if ((end1 >= sentence1 && end2 >= sentence2) && (*end1 == ' ' || *end2 == ' '))
//            space++;
//    }
//
//    if (end1 <= begin1 || end2 <= begin2)
//        flag2 = false;
//
//    if (!space)
//        flag2 = false;
//
//    return flag1 || flag2;
//}

//1813. 句子相似性 III
//https://leetcode.cn/problems/sentence-similarity-iii/

bool areSentencesSimilar(char* sentence1, char* sentence2) {
    char* longStr = sentence1;
    char* shortStr = sentence2;

    if (strlen(longStr) < strlen(shortStr))
    {
        longStr = sentence2;
        shortStr = sentence1;
    }

    char* b1 = longStr;
    char* b2 = shortStr;

    char* e1 = b1 + strlen(b1) - 1;
    char* e2 = b2 + strlen(b2) - 1;

    int space = 0;

    while ((*b1 && *b2) && *b1 == *b2)
    {
        if (*b1 == ' ')
            space++;

        b1++;
        b2++;
    }

    while ((e1 >= longStr && e2 >= shortStr) && *e1 == *e2)
    {
        e1--;
        e2--;
    }

    //如果短句子中没有空闲空间，证明无法插入
    if ((e2 > b2 || !space) && *b2 && e2 + 1 != shortStr)
        return false;

    if (!*b2 && *b1 && *b1 != ' ' && *b1 != 'B')
        return false;

    if (e2 < b2 && e1 == b1 && *(e1 + 1) != ' ' && *e1 != ' ')
        return false;

    return true;
}

int main()
{
    char* p1 = "C";
    char* p2 = "CB B C";
    int n = areSentencesSimilar(p1, p2);
    return 0;
}