#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

////BC134 蛇形矩阵
//#include <stdio.h>
//
//int main()
//{
//    int n = 4;
//    int arr[4][4];
//    int i = 0;
//    int j = 0;
//    int num = 2;
//    arr[0][0] = 1;
//    int flag = 0;   //向右，向下
//    while (num < n * n)
//    {
//        if (i == 0 && flag == 0)
//        {
//            j++;
//            flag = 1;   //上锁
//        }
//        else if (j == 0 || flag == 0)
//        {
//            i++;
//            flag = -1;  //上锁
//        }
//        if (flag == 1)
//        {
//            arr[i][j] = num;
//            i++;
//            j--;
//            if (j == 0)
//                flag = 0;   //解锁
//        }
//        if (flag == -1)
//        {
//            arr[i][j] = num;
//            i--;
//            j++;
//            if (i == 0)
//                flag = 0;   //解锁
//        }
//        num++;
//    }
//    arr[n - 1][n - 1] = num;
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//BC134 蛇形矩阵
#include <stdio.h>

int main()
{
    int n = 9;
    int arr[12][12] = { 0 };
    int i = 0;
    int j = 0;
    int num = 1;
    int flag = 0;   //向右，向下
    arr[0][0] = num++;  //去头
    while (num < n * n)
    {
        //蛇身向右摆
        if (i == 0 && j < n - 1)
        {
            j++;
            flag = 1;
            arr[i][j] = num++;
        }

        //蛇身向下摆
        if (j == 0 && i < n -1)
        {
            i++;
            flag = -1;
            arr[i][j] = num++;
        }

        //特殊情况之一，需要向右摆一位，然后向上移动
        if (i == n - 1 && num - arr[i][j] == 1 && num - arr[i - 1][j + 1] == 2)
        {
            j++;
            flag = -1;
            arr[i][j] = num++;
        }

        //特殊情况之一，需要向下摆一位，然后向下移动
        if (j == n - 1 && num - arr[i][j] == 1 && num - arr[i + 1][j - 1] == 2)
        {
            i++;
            flag = 1;
            arr[i][j] = num++;
        }

        //根据flag判断是需要向下移动，还是向上移动
        if (flag == 1)
        {
            i++;
            j--;
            arr[i][j] = num;
        }
        else  if (flag == -1)
        {
            i--;
            j++;
            arr[i][j] = num;
        }
        num++;  //数值++
    }
    arr[n - 1][n - 1] = num;    //去尾

    //打印
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%-4d", arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}