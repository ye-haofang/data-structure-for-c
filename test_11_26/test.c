#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>


struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

/*
* 方向错了
// 通过前序遍历的数组" A B D # # E # H # # C F # # G # # " 构建二叉树
struct TreeNode* BinaryTreeCreate(int* pc, int n, int* pi)
{
    assert(pc);

    if (pc[*pi] == '#')
    {
        (*pi)++;
        return NULL;
    }

    struct TreeNode* newnode = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    if (!newnode)
    {
        perror("malloc::fail");
        exit(-1);
    }
    newnode->val = pc[(*pi)++]; //可能有bug
    n--;	//这个n好像没啥用，假设是数组的大小，那么每成功创建一个就-1

    newnode->left = BinaryTreeCreate(pc, n, pi);
    newnode->right = BinaryTreeCreate(pc, n, pi);

    return newnode;
}
//144.二叉树的前序遍历
int* preorderTraversal(struct TreeNode* root, int* returnSize) {
    if (!root)
        return NULL;
    static int* pa = NULL;
    int* tmp = (int*)realloc(pa, sizeof(int) * (*returnSize + 1));
    pa = tmp;
    pa[(*returnSize)++] = root->val;
    int* tmp1 = preorderTraversal(root->left, returnSize);
    int* tmp2 = preorderTraversal(root->right, returnSize);

    if (tmp1)
        pa = tmp1;
    if (tmp2)
        pa = tmp2;

    return pa;
}
*/


////144.二叉树的前序遍历
////思路：分工协作，先开辟空间，再利用函数进行前序遍历
//void getPrevOrder(struct TreeNode* root, int* pa, int* returnSize)
//{
//    if (!root)
//        return;
//
//    //前序，根左右
//    pa[(*returnSize)++] = root->val;    // (*returnSize)++ 这个括号得加上
//    getPrevOrder(root->left, pa, returnSize);
//    getPrevOrder(root->right, pa, returnSize);
//}
//
//int* preorderTraversal(struct TreeNode* root, int* returnSize) {
//    *returnSize = 0;    //*returnSize 一开始不为0，初始化一下，防止空树
//    
//    if (!root)
//        return NULL;
//
//    int* pa = (int*)malloc(sizeof(int) * 100);  //开辟空间
//
//    getPrevOrder(root, pa, returnSize); //遍历赋值
//    return pa;
//}

////226.翻转二叉树
////翻转小细节：根左右，前序
////翻转本质：更改链接关系
//
//struct TreeNode* resverTree(struct TreeNode* root)
//{
//    //借助第三方节点
//    struct TreeNode* tmp = root->left;
//    root->left = root->right;
//    root->right = tmp;
//
//    return root;
//}
//
//struct TreeNode* invertTree(struct TreeNode* root) {
//    if (!root)
//        return NULL;
//
//    //将遍历中的打印换成交换
//    resverTree(root);
//
//    invertTree(root->left);
//    invertTree(root->right);
//
//    return root;
//}

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<string.h>

struct TreeNode;

typedef struct TreeNode* QListDataType;	//队列的数据类型

//这是队列中，单个节点的信息
typedef struct QListNode
{
	QListDataType data;
	struct QListNode* next;
}QNode;

//这是整个队列的信息，包含了队头和队尾两个指针
typedef struct QueueNode
{
	QNode* front;	//队头指针
	QNode* rear;	//队尾指针
	size_t size;	//队列长度
}Queue;

void QueueInit(Queue* pq)	//队列的初始化
{
	assert(pq);

	//初始化状态：队头、队尾指针都指向空，队列大小为0
	pq->front = pq->rear = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)	//队列的销毁
{
	assert(pq);

	//销毁思路：利用临时指针进行销毁
	QNode* cur = pq->front;
	while (cur)
	{
		QNode* tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	pq->front = pq->rear = NULL;
	pq->size = 0;
}
bool QueueEmpty(Queue* pq)	//判断当前队空情况
{
	assert(pq);

	return pq->front == NULL;
}
static QNode* buyNode(QListDataType x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc :: fail");
		exit(-1);
	}

	newnode->data = x;	//赋值就是新节点
	newnode->next = NULL;

	return newnode;
}

void QueuePush(Queue* pq, QListDataType x)	//入队
{
	assert(pq);

	//先买一个节点
	QNode* newnode = buyNode(x);

	//分情况：如果队头为空，说明队空，此时直接将新节点赋值给队头、队尾
	if (pq->front == NULL)
	{
		pq->front = pq->rear = newnode;
		pq->size++;
	}
	else
	{
		//否则就是将新节点，链接到队尾，然后更新队尾
		pq->rear->next = newnode;	//链接
		pq->rear = newnode;	//更新队尾
		pq->size++;
	}
}
void QueuePop(Queue* pq)	//出队
{
	assert(pq);
	assert(!QueueEmpty(pq));	//如果队空，是不能出队的

	//出队思想：有元素才能出队，更新队头，销毁原队头
	QNode* cur = pq->front;
	pq->front = pq->front->next;	//更新队头指针
	free(cur);
	cur = NULL;
	pq->size--;
}
QListDataType QueueFront(Queue* pq)	//查看队头元素
{
	assert(pq);
	assert(!QueueEmpty(pq));

	return pq->front->data;
}

// 二叉树节点个数
int BinaryTreeSize(struct TreeNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	//根节点+左右节点
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

bool isCompleteTree(struct TreeNode* root) {
	Queue Q;
	QueueInit(&Q);

	if (root)
		QueuePush(&Q, root);	//首先把根节点入队
	else
		return true;
	int treeSize = BinaryTreeSize(root);

	char* pc = (char*)calloc(sizeof(char), treeSize + 1);
	if (!pc)
	{
		perror("calloc::fail");
		exit(-1);
	}

	struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	assert(node);
	node->val = 'x';

	if (treeSize == 1)
		return true;

	if (!root->right && treeSize != 2)
		return false;

	int i = 0;
	while (i < treeSize)
	{
		struct TreeNode* tmp = QueueFront(&Q);
		QueuePop(&Q);

		pc[i++] = (tmp->val == 'x' ? '\0' : 'a');

		int len = strlen(pc);
		if (len < i)
		{
			return false;
		}

		if (tmp->left)
			QueuePush(&Q, tmp->left);
		else
			QueuePush(&Q, node);
		if (tmp->right)
			QueuePush(&Q, tmp->right);
		else
			QueuePush(&Q, node);
	}

	QueueDestroy(&Q);

	int size = strlen(pc);
	return size == treeSize;
}
int main()
{
    //char arr[] = "1#23###";
    //int i = 0;
    //struct TreeNode* root = BinaryTreeCreate(arr, strlen(arr), &i);

    struct TreeNode* a = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    struct TreeNode* b = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	struct TreeNode* c = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	struct TreeNode* d = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	struct TreeNode* e = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	struct TreeNode* f = (struct TreeNode*)malloc(sizeof(struct TreeNode));


	a->val = 1, b->val = 2, c->val = 3, d->val = 4, e->val = 5, f->val = 7;

    a->left = b, a->right = c;
    b->left = d, b->right = e;
	c->left = NULL, c->right = f;
	d->left = d->right = e->left = e->right = f->left = f->right = NULL;

	printf("%d\n", isCompleteTree(a));

    return 0;
}