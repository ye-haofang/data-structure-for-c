#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdbool.h>

//NC31 第一个只出现一次的字符

#include<stdlib.h>
#include<assert.h>
#include<string.h>

int FirstNotRepeatingChar(char* str) {
    //映射
    int* tmp = (int*)malloc(sizeof(int) * 53);
    assert(tmp);
    memset(tmp, -1, sizeof(int) * 53);

    int pos = 0;
    while (*(str + pos))
    {
        int num = 0;
        if (*(str + pos) >= 97)
            num = *(str + pos) - 70; //小写，确保 a 落在 27
        else
            num = *(str + pos) - 64;

        if (tmp[num] == -1)
            tmp[num] = pos; //初始化
        else
            tmp[num] = -10;  //多次出现，就非法

        pos++;
    }

    pos = 0;
    while (*(str + pos))
    {
        int num = 0;
        if (*(str + pos) >= 97)
            num = *(str + pos) - 70; //小写，确保 a 落在 27
        else
            num = *(str + pos) - 64;

        if (tmp[num] >= 0)
            return tmp[num];

        pos++;
    }

    free(tmp);
    tmp = NULL;

    return -1;
}


//面试题 01.01. 判定字符是否唯一

int cmp(const void* e1, const void* e2)
{
    return *(char*)e1 - *(char*)e2;
}

bool isUnique(char* astr) {
    //侵入式编程，直接排序判断
    int len = strlen(astr);
    qsort(astr, len, sizeof(char), cmp);
    for (int i = 1; i < len; i++)
    {
        if (astr[i - 1] == astr[i])
            return false;
    }

    return true;
}

//面试题 01.01. 判定字符是否唯一

bool isUnique(char* astr) {
    //映射
    int* tmp = (int*)malloc(sizeof(int) * 26);  //26个字母
    assert(tmp);
    memset(tmp, -1, sizeof(int) * 26);

    char* ps = astr;
    while (*ps)
        tmp[*ps++ - 'a']++;   //映射到数组中

    for (int i = 0; i < 26; i++)
    {
        if (tmp[i] > 0)
            return false;
    }

    return true;
}

int main()
{
    char arr[20] = "kzwunahkiz";
    printf("%d\n", isUnique(arr));
    return 0;
}