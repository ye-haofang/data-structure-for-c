#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<ctype.h>

//1859. 将句子排序
//https://leetcode.cn/problems/sorting-the-sentence/

char* sortSentence(char* s) {
    int len = strlen(s);
    char* newWord = (char*)calloc(len, sizeof(char));
    assert(newWord);

    char* pstr[9] = { "0" };
    char* cur = s;
    char* prev = s;
    int num = 0;
    while (*cur != '\0')
    {
        if (isdigit(*cur))
        {
            //识别到了数字
            int pos = *cur - '0' - 1;   //转为下标
            *cur = '\0';
            pstr[pos] = prev;   //捕获一个单词
            num++;
            cur++;
            if (*cur)
                cur++;
            prev = cur; //prev指向下一个单词
        }
        else
            cur++;  //cur向后找数字
    }

    int pos = 0;
    for (int i = 0; i < num; i++)
    {
        if (*pstr[i])
        {
            while (*pstr[i])
            {
                newWord[pos++] = *pstr[i];
                pstr[i]++;  //指向下一个字符
            }
            newWord[pos++] = ' ';
        }
    }

    //把最后一个位置植入 '\0'
    newWord[--pos] = '\0';

    strcpy(s, newWord);
    free(newWord);
    newWord = NULL;

    return s;
}
int main()
{
    char str[] = "is2 sentence4 This1 a3";
    char* pstr = sortSentence(str);
    return 0;
}