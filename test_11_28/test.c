#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//110.平衡二叉树
//思路：判断左右子树深度差是否合法
//二叉树的深度
#include<stdlib.h>
int BinaryTreeDepth(struct TreeNode* root)
{
    if (root == NULL)
        return 0;

    int leftLen = BinaryTreeDepth(root->left);
    int rightLen = BinaryTreeDepth(root->right);

    return 1 + (leftLen > rightLen ? leftLen : rightLen);
}

bool isBalanced(struct TreeNode* root) {
    if (!root)
        return true;

    bool b = true;
    int leftDepth = BinaryTreeDepth(root->left);
    int rightDepth = BinaryTreeDepth(root->right);

    if (abs(leftDepth - rightDepth) > 1)
        b = false;

    return b && isBalanced(root->left) && isBalanced(root->right);
}