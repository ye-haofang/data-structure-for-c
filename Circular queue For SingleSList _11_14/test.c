#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdbool.h>

#include<assert.h>
#include<stdlib.h>
//单向循环链表实现循环队列
typedef int SListDataType;

typedef struct SListNode
{
    SListDataType data;
    struct SListNode* next; //指向下一个节点
}SLNode;

typedef struct {
    SLNode* head;
    SLNode* tail;
} MyCircularQueue;

SLNode* buyNode(void)
{
    SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));
    assert(newnode);

    newnode->data = 0;
    newnode->next = NULL;
    return newnode;
}
void SListPush(MyCircularQueue* obj, int k)
{
    while (k--)
    {
        SLNode* newnode = buyNode();
        if (obj->tail == NULL)
        {
            //如果尾为空，为第一次插入，需要把头尾指针的指向处理一下
            obj->head = obj->tail = newnode;
        }
        else
        {
            obj->tail->next = newnode;
            obj->tail = newnode;
        }
    }

    obj->tail->next = obj->head;

    obj->tail = obj->head;  //头尾相遇
}

MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    assert(obj);

    obj->head = obj->tail = NULL;
    SListPush(obj, k + 1);    //多申请一个

    return obj;
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj);
bool myCircularQueueIsFull(MyCircularQueue* obj);

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    assert(obj);
    //满了，是入不了队的
    if (myCircularQueueIsFull(obj))
        return false;

    //关于入队：即链表的尾插
    obj->tail->data = value;
    obj->tail = obj->tail->next;    //尾节点向后移动
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    assert(obj);
    //空了，是出不了队的
    if (myCircularQueueIsEmpty(obj))
        return false;

    //关于出队: 头节点向后移动，就行了，有效数据为[头，尾]
    obj->head = obj->head->next;
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    //队头元素，就是头节点的数据域
    return obj->head->data;
}

int myCircularQueueRear(MyCircularQueue* obj) {
    assert(obj);
    if (myCircularQueueIsEmpty(obj))
        return -1;

    //队尾元素，就是尾节点的上一个节点数据域,单链表需要遍历
    SLNode* prev = obj->tail;    //记录当前为节点信息
    //循环遍历，找到tail的上一个
    while (prev->next != obj->tail)
    {
        prev = prev->next;  //向后转动
    }

    return prev->data;  //此时的 prev 就是队尾元素
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    assert(obj);
    //头尾相遇，链表为空

    return obj->head == obj->tail;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    assert(obj);
    //如果尾的下一个节点等于头节点，就说明满了

    SLNode* tmp = obj->tail->next;
    return tmp == obj->head;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    assert(obj);

    //关于循环单链表释放：从头往后走，干掉一个走一个
    SLNode* cur = obj->head->next;
    while (cur != obj->head)
    {
        SLNode* tmp = cur;
        cur = cur->next;    //向后移动
        free(tmp);
    }
    free(cur);
    obj->head = obj->tail = cur = NULL;
    free(obj);
    obj = NULL;
}

/*
["MyCircularQueue","enQueue","enQueue","enQueue","enQueue","Rear","isFull","deQueue","enQueue","Rear"]
[[3],[1],[2],[3],[4],[],[],[],[4],[]]
*/
int main()
{
    int a = 0;
    bool b = true;
    MyCircularQueue* M1 = myCircularQueueCreate(3);

    b = myCircularQueueEnQueue(M1, 1);
    b = myCircularQueueEnQueue(M1, 2);
    b = myCircularQueueEnQueue(M1, 3);
    b = myCircularQueueEnQueue(M1, 4);

    a = myCircularQueueRear(M1);

    b = myCircularQueueIsFull(M1);

    b = myCircularQueueDeQueue(M1);

    b = myCircularQueueEnQueue(M1, 4);

    a = myCircularQueueRear(M1);
    return 0;
}