#define _CRT_SECURE_NO_WARNINGS 1	

//58. 最后一个单词的长度
//https://leetcode.cn/problems/length-of-last-word/

int lengthOfLastWord(char* s) {
    int endLen = strlen(s);

    char* pend = s + endLen;

    //先让 pend 指向最后一个单词的最后一个字母，然后从后往前统计长度
    while (!isalpha(*pend))
        pend--;

    int n = 0;
    while (pend >= s && *pend != ' ')
    {
        n++;
        pend--;
    }

    return n;
}