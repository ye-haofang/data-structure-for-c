#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//OR135 单词缩写
#include <stdio.h>
#include<string.h>

int main() {
    int n = 0;
    scanf("%d", &n);
    while (n--)
    {
        char word[101] = "0";
        scanf("%s", word);
        int len = strlen(word);
        if (len < 10)
            printf("%s\n", word);
        else
        {
            char str[5] = "0";
            char end = *(word + len - 1);   //取最后一个字符
            str[0] = *word; //植入第一个字符
            len -= 2;   //去头去尾
            int i = 1;
            char n1 = (len / 10) + '0';
            char n2 = (len % 10) + '0';
            if (n1 > '0')
                str[i++] = n1;
            str[i++] = n2;
            str[i] = end;   //植入最后一个字符
            printf("%s\n", str);
        }
    }
    return 0;
}