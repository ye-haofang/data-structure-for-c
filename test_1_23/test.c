#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//2303. 计算应缴税款总额
//https://leetcode.cn/problems/calculate-amount-paid-in-taxes/

double calculateTax(int** brackets, int bracketsSize, int* bracketsColSize, int income) {
    double tax = 0;

    //总收入减完，计算就结束了
    int row = 0;
    int col = 0;
    int val = 0;
    int tmp = income;
    int preVal = 0;
    while (tmp > 0)
    {
        if (income > brackets[row][col])
            val = brackets[row][col] - val;
        else
            val = income - preVal;

        tmp -= val;
        //求和
        tax += (val * (double)brackets[row][col + 1]);
        preVal = val = brackets[row][col];
        row++;
    }

    return tax / 100;
}

int main()
{
    int arr[][2] = { 2,7,3,17,4,37,7,6,9,83,16,67,19,29 };
    int col = 2;
    double n = calculateTax(arr, sizeof(arr), &col, 18);
    return 0;
}