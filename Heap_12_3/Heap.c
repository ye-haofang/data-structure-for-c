#define _CRT_SECURE_NO_WARNINGS 1	
#include"Heap.h"

void HeapInit(HP* ph)	//堆的初始化
{
	assert(ph);

	ph->data = NULL;
	ph->size = ph->capacity = 0;	//初始化
}

void HeapDestroy(HP* ph)	//堆的销毁
{
	assert(ph);

	//如果堆的数据域（指针data）指向空，是不需要进行销毁的
	if (ph->data)
	{
		free(ph->data);
		ph->data = NULL;

		ph->size = ph->capacity = 0;
	}
}

void HeapPrint(HP* ph)	//打印堆
{
	assert(ph);

	for (int i = 0; i < ph->size; i++)
		printf("%d ", ph->data[i]);

	printf("\n");
}

void AdjustUp(HPDataType* pa, int n, int child);
void AdjustDown(HPDataType* pa, int n, int parent);

void HeapCreat(HP* ph, HPDataType* pa, int n)	//构建堆
{

	/*
	* 2023.2.19 修正
	* 原向上调整建堆存在 bug
	* 1、数据拷贝时存在漏拷贝的情况
	* 2、对已存在堆(无序)进行向上调整，需每次确认较大值，依次放入堆中，比较麻烦且容易出问题
	*
	* 解决方案：遵循向上调整的思路，将 pa 中的元素依次入堆，每次入堆都进行调整，这样可以确保始终为大堆，且运行稳定
	* 注：在堆进行操作前，先要确保堆已初始化
	*/
	////建堆有两种方式
	////1.向上调整
	////2.向下调整

	//assert(pa);

	////开辟一块空间
	//HPDataType* ptmp = (HPDataType*)malloc(sizeof(HPDataType) * n);
	//assert(ptmp);
	//ph->data = ptmp;
	//ph->size = ph->capacity = n;

	//////将数据拷贝至开辟空间中
	////ph->data = memcpy(ph->data, pa, sizeof(HPDataType) * (n - 1));

	//////向上调整
	////for (int i = n - 1; i > 0; i--)
	////	AdjustUp(ph->data, i, i - 1);

	//新解决方案
	assert(ph && pa);	//断言
	HeapInit(ph);	//确保堆已被初始化

	int pos = 0;
	while (pos < n)
	{
		//入堆，入堆本身自带向上调整
		HeapPush(ph, pa[pos]);
		pos++;
	}
}

void HeapCreat2(HP* ph, HPDataType* pa, int n)	//构建堆
{
	//建堆有两种方式
	//1.向上调整
	//2.向下调整

	assert(pa);

	//开辟一块空间
	HPDataType* ptmp = (HPDataType*)malloc(sizeof(HPDataType) * n);
	assert(ptmp);
	ph->data = ptmp;
	ph->size = ph->capacity = n;

	//将数据拷贝至开辟空间中
	memcpy(ph->data, pa, sizeof(HPDataType) * (n - 1));

	//向下调整，建堆算法
	int parent = (n - 1 - 1) / 2;
	for (int i = parent; i >= 0; i--)
		AdjustDown(ph->data, n - 1, i);
}

void Swap(HPDataType* num1, HPDataType* num2)
{
	HPDataType tmp = *num1;
	*num1 = *num2;
	*num2 = tmp;
}

//向上调整，根据孩子找父亲
void AdjustUp(HPDataType* pa, int n, int child)
{
	assert(pa);

	//大堆：父亲比孩子都大
	//小堆：父亲比孩子都小

	int parent = (child - 1) / 2;

	while (child > 0)
	{
		//大堆，孩子比父亲大，就调整
		//如果条件为假，说明此位置是合法的
		if (pa[child] > pa[parent])
		{
			Swap(&pa[child], &pa[parent]);
			child = parent;	//孩子移动
			parent = (child - 1) / 2;	//父亲更新
		}
		else
			break;
	}
}

void HeapPush(HP* ph, HPDataType x)	//入堆
{
	assert(ph);

	//考虑扩容
	if (ph->size == ph->capacity)
	{
		HPDataType newcapacity = ph->capacity == 0 ? 4 : ph->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(ph->data, sizeof(HPDataType) * newcapacity);
		if (!tmp)
		{
			perror("realloc fail");
			exit(-1);
		}

		ph->capacity = newcapacity;
		ph->data = tmp;
	}

	//入堆，直接尾插，然后向上调整
	ph->data[ph->size++] = x;
	AdjustUp(ph->data, ph->size, ph->size - 1);
}

void AdjustDown(HPDataType* pa, int n, int parent)	//向下调整
{
	assert(pa);

	//大堆，向下调整，需要找出大孩子，然后比较是否需要交换
	int child = parent * 2 + 1;	//假设左孩子为大孩子
	while (child < n)
	{
		//必须有右孩子才能进行判断
		if ((child + 1) < n && pa[child + 1] > pa[child])
			child++;

		if (pa[child] > pa[parent])
		{
			Swap(&pa[child], &pa[parent]);
			parent = child;
			child = parent * 2 + 1;	//循环
		}
		else
			break;
	}
}

void HeapPop(HP* ph)	//出堆
{
	assert(ph);
	assert(!HeapEmpty(ph));

	//出的是堆顶的元素
	//先把堆顶和堆底元素交换，然后向下调整

	Swap(&ph->data[0], &ph->data[ph->size - 1]);	//交换
	ph->size--;	//有效元素-1

	AdjustDown(ph->data, ph->size - 1, 0);	//向下调整
}

HPDataType HeapTop(HP* ph)	//取堆顶数据
{
	assert(ph);
	assert(!HeapEmpty(ph));

	return ph->data[0];
}

size_t HeapSize(HP* ph)	//堆的有效元素个数
{
	assert(ph);

	return ph->size;
}

bool HeapEmpty(HP* ph)	//判空
{
	assert(ph);

	return ph->size == 0;
}

void HeapSort(HPDataType* pa, int n)	//堆排序
{
	assert(pa);

	//升序，建大堆，降序，建小堆
	//注意：对数据进行排序，数组就是一个天然的堆，调整下就行了
	//均采用向下调整建堆
	int i = (n - 1 - 1) / 2;
	for (; i >= 0; i--)
		AdjustDown(pa, n, i);

	//大堆（升序），此时堆顶元素就是最大值，将其沉底，然后调整堆
	for (int i = 0; i < n - 1; i++)
	{
		Swap(&pa[0], &pa[n - 1 - i]);	//交换
		AdjustDown(pa, n - 1 - i, 0);	//调整
	}
}

void TopK(HPDataType* pa, int n, int k)	//TopK问题
{
	assert(pa);

	//最大，小堆
	//最小，大堆

	HP h;
	HeapInit(&h);

	int* tmp = (int*)malloc(sizeof(int) * k);
	assert(tmp);
	h.data = tmp;
	h.size = h.capacity = k;

	memcpy(h.data, pa, sizeof(int) * k);
	int parent = (k - 1 - 1) / 2;
	for (int i = parent; i >= 0; i--)
		AdjustDown(h.data, k, i);

	int i = k;
	while (i < n)
	{
		//现在是大堆，比较条件是数组元素小于堆顶元素，取的是最小的前k个数
		if (pa[i] < h.data[0])
		{
			Swap(&pa[i], &h.data[0]);
			AdjustDown(h.data, k - 1, 0);
		}
		i++;

		////现在是小堆，比较条件是数组元素大于堆顶元素，取的是最大的前k个数
		//if (pa[i] > h.data[0])
		//{
		//	Swap(&pa[i], &h.data[0]);
		//	AdjustDown(h.data, k, 0);
		//}
		//i++;
	}

	//排序一下，显示效果更好
	HeapSort(h.data, k);
	HeapPrint(&h);

	HeapDestroy(&h);
}