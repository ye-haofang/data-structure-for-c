#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"


void TestHeap1()
{
	HP h, h2, h3;
	HeapInit(&h);
	HeapInit(&h2);
	HeapInit(&h3);

	for (int i = 0; i < 10; i++)
		HeapPush(&h, i);

	HeapPrint(&h);

	for (int i = 10; i < 20; i++)
		HeapPush(&h2, i);

	HeapPrint(&h2);

	for (int i = 45; i < 55; i++)
		HeapPush(&h3, i);

	HeapPrint(&h3);

	HeapDestroy(&h);
}

void TestHeap2()
{
	HP h;
	HeapInit(&h);

	for (int i = 0; i < 10; i++)
		HeapPush(&h, i);

	HeapPrint(&h);

	for (int i = 0; i < 5; i++)
		HeapPop(&h);

	HeapPrint(&h);

	for (int i = 45; i < 55; i++)
		HeapPush(&h, i);

	HeapPrint(&h);

	for (int i = 0; i < 10; i++)
		HeapPop(&h);

	HeapPrint(&h);

	HeapDestroy(&h);
}

void TestHeap3()
{
	HP h;
	HeapInit(&h);

	srand((size_t)time(NULL));	//种子

	for (int i = 0; i < 30; i++)
		HeapPush(&h, rand() % 100);

	HeapPrint(&h);

	printf("堆顶元素 %d\n", HeapTop(&h));
	printf("元素数量 %d\n", HeapSize(&h));
	printf("是否为空 %d\n", HeapEmpty(&h));

	HeapDestroy(&h);
}

void TestHeap4()
{
	HP h;
	HeapInit(&h);
	
	HP h2;
	HeapInit(&h2);

	//int arr[] = { 1,4,5,7,3,7,9,4,3 };
	//int len = sizeof(arr) / sizeof(arr[0]);

	//向下调整比向上调整要快 20%
	int n = 10000000;
	int* pa = (int*)malloc(sizeof(int) * n);
	assert(pa);

	for (int i = 0; i < n; i++)
	{
		pa[i] = rand();
		//pa[i] = i;
	}

	int begin1 = clock();
	HeapCreat(&h, pa, n);
	int end1 = clock();

	int begin2 = clock();
	HeapCreat2(&h2, pa, n);
	int end2 = clock();

	printf("向上调整所耗时间 %d ms\n向下调整所耗时间 %d ms\n", end1 - begin1, end2 - begin2);

	free(pa);
	pa = NULL;
	HeapDestroy(&h);
}

void TestHeap5()
{
	int n = 10;
	int* pa = (int*)malloc(sizeof(int) * n);
	assert(pa);

	for (int i = 0; i < n; i++)
	{
		pa[i] = rand() % 100;
		//pa[i] = i;
	}

	for (int i = 0; i < n; i++)
		printf("%d ", pa[i]);
	printf("\n");

	HeapSort(pa, n);

	for (int i = 0; i < n; i++)
		printf("%d ", pa[i]);
	printf("\n");

	free(pa);
	pa = NULL;
}

void TestHeap6()
{
	int n = INT_MAX / 100;
	int* pa = (int*)malloc(sizeof(int) * n);
	assert(pa);

	for (int i = 0; i < n; i++)
	{
		//pa[i] = rand();
		pa[i] = i;
	}

	//for (int i = 0; i < n; i++)
	//	printf("%d ", pa[i]);
	//printf("\n");

	int begin1 = clock();
	TopK(pa, n, 5);
	int end1 = clock();

	printf("%d个数\nTop-K耗时 %d ms\n", n, end1 - begin1);

	free(pa);
	pa = NULL;
}

int main()
{
	TestHeap6();
	return 0;
}