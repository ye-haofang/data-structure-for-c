#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<time.h>
#include<string.h>

typedef int HPDataType;	//堆的元素类型

typedef struct HeapInfo
{
	HPDataType* data;	//数据域
	int size;	//堆的有效元素数
	int capacity;	//堆的容量，方便后续进行扩容
}HP;

void HeapInit(HP* ph);	//堆的初始化
void HeapDestroy(HP* ph);	//堆的销毁
void HeapPrint(HP* ph);	//打印堆
void HeapCreat(HP*ph, HPDataType* pa, int n);	//构建堆

void HeapPush(HP* ph, HPDataType x);	//入堆
void HeapPop(HP* ph);	//出堆

HPDataType HeapTop(HP* ph);	//取堆顶数据
size_t HeapSize(HP* ph);	//堆的有效元素个数
bool HeapEmpty(HP* ph);	//判空

void HeapSort(HPDataType* pa, int n);	//堆排序
void TopK(HPDataType* pa, int n, int k);	//TopK问题

void HeapCreat2(HP* ph, HPDataType* pa, int n);	//构建堆