#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>

////方案废弃，无法解决负数
////4. 寻找两个正序数组的中位数
////https://leetcode.cn/problems/median-of-two-sorted-arrays/
//
//double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
//
//    //判断是否存在空的情况
//    if (nums1Size == 0 && nums2Size != 0)
//    {
//        //判断奇偶
//        if (nums2Size % 2 == 0)
//            return ((double)nums2[nums2Size / 2] + nums2[nums2Size / 2 - 1]) / 2.0;
//        else
//            return nums2[nums2Size / 2];
//    }
//    else if (nums1Size != 0 && nums2Size == 0)
//    {
//        //判断奇偶
//        if (nums1Size % 2 == 0)
//            return ((double)nums1[nums1Size / 2] + nums1[nums1Size / 2 - 1]) / 2.0;
//        else
//            return nums1[nums1Size / 2];
//    }
//    else if (!nums1Size && !nums2Size)
//        return 0.0;
//
//    int* max = nums1;
//    int maxSize = nums1Size;
//    int* min = nums2;
//    int minSize = nums2Size;
//
//    if (*nums2 > *max)
//    {
//        max = nums2;
//        maxSize = nums2Size;
//        min = nums1;
//        minSize = nums1Size;
//    }
//
//    int* maxEnd = max + maxSize;
//    int* minEnd = min + minSize;
//
//    //接轨
//    int flag = (nums1Size + nums2Size) / 2;
//
//    //判断奇偶
//    if (flag % 2 == 0)
//    {
//        flag++;
//
//        int sign = 1;
//
//        int leftVal = 0;
//        int rightVal = 0;
//
//        while (flag > 0 && min < minEnd && max < maxEnd)
//        {
//            if (sign > 0)
//            {
//                leftVal = *min;
//                min++;
//            }
//            else
//            {
//                rightVal = *max;
//                max++;
//            }
//
//            sign *= -1;
//            flag--;
//        }
//
//        if (flag > 0)
//        {
//            sign = 1;
//
//            if (min < minEnd)
//            {
//                max = min;
//                while (flag > 0)
//                {
//                    if (sign > 0)
//                    {
//                        min = max + 1;
//                        leftVal = *min;
//                    }
//                    else
//                    {
//                        max = min + 1;
//                        rightVal = *max;
//                    }
//
//                    sign *= -1;
//                    flag--;
//                }
//            }
//            else if (max < maxEnd)
//            {
//                while (flag > 0)
//                {
//                    if (sign > 0)
//                    {
//                        min = max + 1;
//                        leftVal = *min;
//                    }
//                    else
//                    {
//                        max = min + 1;
//                        rightVal = *max;
//                    }
//
//                    sign *= -1;
//                    flag--;
//                }
//            }
//        }
//
//        return ((double)leftVal + rightVal) / 2.0;
//    }
//    else
//    {
//        double val = (double)*min;
//
//        int sign = 1;
//        while (flag > 0 && min < minEnd && max < maxEnd)
//        {
//            if (sign > 0)
//            {
//                val = *max;
//                min++;
//            }
//            else
//            {
//                val = *min;
//                max++;
//            }
//
//            sign *= -1;
//            flag--;
//        }
//
//        if (flag > 0)
//        {
//            while (flag > 0 && min < minEnd)
//                min++, flag--;
//
//            while (flag > 0 && max < maxEnd)
//                max++, flag--;
//        }
//
//        return val;
//    }
//}

////方案二，谁小谁就走，需要改进，通过了1756个测试用例
// 
////4. 寻找两个正序数组的中位数
////https://leetcode.cn/problems/median-of-two-sorted-arrays/
//
//double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
//
//    //判断是否存在空的情况
//    if (nums1Size == 0 && nums2Size != 0)
//    {
//        //判断奇偶
//        if (nums2Size % 2 == 0)
//            return ((double)nums2[nums2Size / 2] + nums2[nums2Size / 2 - 1]) / 2.0;
//        else
//            return nums2[nums2Size / 2];
//    }
//    else if (nums1Size != 0 && nums2Size == 0)
//    {
//        //判断奇偶
//        if (nums1Size % 2 == 0)
//            return ((double)nums1[nums1Size / 2] + nums1[nums1Size / 2 - 1]) / 2.0;
//        else
//            return nums1[nums1Size / 2];
//    }
//    else if (!nums1Size && !nums2Size)
//        return 0.0;
//
//    int* nums1End = nums1 + nums1Size;
//    int* nums2End = nums2 + nums2Size;
//
//    //接轨
//    int len = (nums1Size + nums2Size);
//    int flag = len / 2;
//
//    //判断奇偶
//    if (len % 2 == 0)
//    {
//        int leftVal = 0;
//        int rightVal = 0;
//
//        //打擂台，小的值会被丢弃
//        while (flag && nums1 < nums1End && nums2 < nums2End)
//        {
//            if (*nums1 < *nums2)
//            {
//                leftVal = *nums1;
//                rightVal = *nums2;
//
//                if (nums1 < nums1End - 1 && *nums2  > *(nums1 + 1))
//                    leftVal = *(nums1 + 1);
//
//                nums1++;
//            }
//            else
//            {
//                leftVal = *nums2;
//                rightVal = *nums1;
//
//                if (nums2 < nums1End - 1 && *nums1  > *(nums2 + 1))
//                    rightVal = *(nums1 + 1);
//
//                nums2++;
//            }
//
//            flag--;
//        }
//
//        while (flag)
//        {
//            if (nums1 < nums1End)
//                nums2 = nums1 + 1;
//            else if (nums2 < nums2End)
//                nums1 = nums2 + 1;
//
//            leftVal = *nums1;
//            rightVal = *nums2;
//
//            flag--;
//        }
//
//        return ((double)leftVal + rightVal) / 2.0;
//    }
//    else
//    {
//        flag++;
//
//        double val = 0.0;
//
//        //原理同上
//        while (flag && nums1 < nums1End && nums2 < nums2End)
//        {
//            if (*nums1 < *nums2)
//            {
//                val = *nums1;
//                nums1++;
//            }
//            else
//            {
//                val = *nums2;
//                nums2++;
//            }
//
//            flag--;
//        }
//
//        while (flag && nums1 < nums1End)
//        {
//            val = *nums1;
//            nums1++;
//            flag--;
//        }
//
//        while (flag && nums2 < nums2End)
//        {
//            val = *nums2;
//            nums2++;
//            flag--;
//        }
//
//        return val;
//    }
//}

////方案三，改进。
////已通过

//4. 寻找两个正序数组的中位数
//https://leetcode.cn/problems/median-of-two-sorted-arrays/

/*
* 思路：计算出中位数的位置，通过不断移动找到中位数
* 需要根据奇偶制定不同的方法
* 奇数：直接算出中间位值即可
* 偶数：需要两两配对，通过不断比较向后移动，直到中位数用完
* 注意：存在两个数组长短不一的情况，此时需要再次进行移动
*/
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {

    //判断是否存在空的情况
    if (nums1Size == 0 && nums2Size != 0)
    {
        //判断奇偶
        if (nums2Size % 2 == 0)
            return ((double)nums2[nums2Size / 2] + nums2[nums2Size / 2 - 1]) / 2.0;
        else
            return nums2[nums2Size / 2];
    }
    else if (nums1Size != 0 && nums2Size == 0)
    {
        //判断奇偶
        if (nums1Size % 2 == 0)
            return ((double)nums1[nums1Size / 2] + nums1[nums1Size / 2 - 1]) / 2.0;
        else
            return nums1[nums1Size / 2];
    }
    else if (!nums1Size && !nums2Size)
        return 0.0;

    int* nums1End = nums1 + nums1Size;
    int* nums2End = nums2 + nums2Size;

    //接轨
    int len = (nums1Size + nums2Size);
    int flag = len / 2;

    //判断奇偶
    if (len % 2 == 0)
    {
        int leftVal = 0;
        int rightVal = 0;

        //两两背着走，小的值会被丢弃
        while (flag && nums1 < nums1End && nums2 < nums2End)
        {
            if (*nums1 < *nums2)
            {
                leftVal = *nums1;
                rightVal = *nums2;

                if (nums1 < nums1End - 1 && *nums2 > *(nums1 + 1))
                    rightVal = *(nums1 + 1);

                nums1++;
            }
            else
            {
                leftVal = *nums2;
                rightVal = *nums1;

                if (nums2 < nums2End - 1 && *nums1 > *(nums2 + 1))
                    rightVal = *(nums2 + 1);

                nums2++;
            }

            flag--;
        }

        int* pn = NULL;
        if (nums1 < nums1End)
            pn = nums1 + 1;
        if (nums2 < nums2End)
            pn = nums2 + 1;

        if (leftVal > rightVal)
        {
            int tmp = leftVal;
            leftVal = rightVal;
            rightVal = tmp;
        }

        while (flag)
        {
            if (pn)
            {
                if (leftVal < *pn)
                {
                    leftVal = rightVal;
                    rightVal = *pn;
                }
                else
                {
                    rightVal = leftVal;
                    leftVal = *pn;
                }

                pn++;
            }

            flag--;
        }

        return ((double)leftVal + rightVal) / 2.0;
    }
    else
    {
        flag++;

        double val = 0.0;

        //原理同上
        while (flag && nums1 < nums1End && nums2 < nums2End)
        {
            if (*nums1 < *nums2)
            {
                val = *nums1;
                nums1++;
            }
            else
            {
                val = *nums2;
                nums2++;
            }

            flag--;
        }

        while (flag && nums1 < nums1End)
        {
            val = *nums1;
            nums1++;
            flag--;
        }

        while (flag && nums2 < nums2End)
        {
            val = *nums2;
            nums2++;
            flag--;
        }

        return val;
    }
}


int main()
{
    int arr1[] = { 1 };
    int arr2[] = { 2,3,4,5,6 };
    
    double val = findMedianSortedArrays(arr1, sizeof(arr1) / sizeof(arr1[0]), arr2, sizeof(arr2) / sizeof(arr2[0]));

     return 0;
}