#define _CRT_SECURE_NO_WARNINGS 1	

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

////废弃方案1
////2287. 重排字符形成目标字符串
////https://leetcode.cn/problems/rearrange-characters-to-make-target-string/
//
//int rearrangeCharacters(char* s, char* target) {
//    //映射统计
//    int* space = (int*)malloc(sizeof(int) * 26);
//    assert(space);
//    memset(space, 0, sizeof(int) * 26);
//
//    char* ps = s;
//    char* pt = target;
//    while (*pt)
//    {
//        space[*pt - 'a']++;
//        pt++;
//    }
//
//    while (*ps)
//    {
//        int val = space[*ps - 'a'];
//
//        if (val > 0)
//            space[*ps - 'a']++;
//        else
//            space[*ps - 'a']--;
//
//        ps++;
//    }
//
//    //木桶效应
//    int min = 100;
//    for (int i = 0; i < 26; i++)
//    {
//        if (space[i] > 0 && space[i] < min)
//            min = space[i];
//    }
//
//    free(space);
//    space = NULL;
//
//    return min - 1;
//}

//2287. 重排字符形成目标字符串
//https://leetcode.cn/problems/rearrange-characters-to-make-target-string/

int rearrangeCharacters(char* s, char* target) {
    //映射统计
    int* space = (int*)malloc(sizeof(int) * 26);
    assert(space);
    memset(space, 0, sizeof(int) * 26);

    char* ps = s;

    while (*ps)
    {
        space[*ps - 'a']++;
        ps++;
    }


    int num = 0;

    ps = s;
    while (*ps)
    {
        char* pt = target;
        while (*pt)
        {
            if (space[*pt - 'a'] > 0)
                space[*pt - 'a']--;
            else
                return num;

            pt++;
        }
        num++;
        ps++;
    }

    free(space);
    space = NULL;

    return num;
}

int main()
{
    char* s = "ilovecodingonleetcode";
    char* tar = "code";
    rearrangeCharacters(s, tar);
    return 0;
}