#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>

//思路一
//分割重组
//错误原因：tmp[0] 非法，换成动态内存开辟就行了
#include<stdlib.h>
#include<assert.h>
void rotate(int* nums, int numsSize, int k) {
	k %= numsSize;
	int ret = k;
	int* tmp = (int*)malloc(sizeof(int) * k);
	assert(tmp);
	int* pt = tmp;
	int i = 0;
	int j = numsSize - k;
	//分割存储
	while (ret--)
	{
		tmp[i] = nums[j];
		i++;
		j++;
	}

	ret = numsSize - k;
	i = numsSize - 1;
	j = numsSize - k - 1;
	//移动
	while (ret--)
	{
		nums[i] = nums[j];
		i--;
		j--;
	}

	ret = k;
	i = 0;
	//重新赋值
	while (ret--)
	{
		nums[i] = tmp[i];
		i++;
	}
	free(pt);
	pt = tmp = NULL;
}
int main()
{
	int nums[] = { 1,2,3,4,5,6,7 };
	rotate(nums, 7, 1);
	for (int i = 0; i < 7; i++)
		printf("%d ", nums[i]);
	return 0;
}