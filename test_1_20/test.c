#define _CRT_SECURE_NO_WARNINGS 1	

//2299. 强密码检验器 II
//https://leetcode.cn/problems/strong-password-checker-ii/

bool strongPasswordCheckerII(char* password) {
    char specilChar[] = "!@#$%^&*()-+";
    char* cur = password + 1;
    char* prev = password;
    int len = 0;
    bool flagUp = false;
    bool flagLow = false;
    bool flagDig = false;
    bool flagSpecil = false;

    while (*prev)
    {
        //排除两个字符相同的情况
        if (*cur && *prev == *cur)
            return false;

        //至少有一个大写字母
        if (isupper(*prev))
            flagUp = true;

        //至少有一个小写字母
        if (islower(*prev))
            flagLow = true;

        //至少有一个数字
        if (isdigit(*prev))
            flagDig = true;

        //至少有一个特殊字符
        char* tmp = specilChar;
        while (*tmp)
        {
            if (*tmp == *prev)
                flagSpecil = true;

            tmp++;
        }

        //两个指针向后走，密码长度+1
        prev++, cur++, len++;
    }

    return len >= 8 && flagUp && flagLow && flagDig && flagSpecil;
}