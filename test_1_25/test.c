#define _CRT_SECURE_NO_WARNINGS 1	
#include<stdio.h>
#include<stdlib.h>

//3. 无重复字符的最长子串
//https://leetcode.cn/problems/longest-substring-without-repeating-characters/

int lengthOfLongestSubstring(char* s) {
    int longestLen = 1;
    int longTmp = 1;
    char* prev = s;

    if (!(*prev))
        return 0;

    char* cur = s + 1;
    while (*cur)
    {
        if (longTmp > longestLen)
            longestLen = longTmp;

        longTmp = 1;

        char* tmp = prev;
        while (tmp < cur)
        {
            if (*tmp == *cur)
            {
                //if ((*cur) == *(cur - 1))
                //    prev = cur;
                //else
                //    prev = cur - 1;

                //回退至相同值的下一位
                prev = tmp + 1;

                break;
            }
            else
            {
                tmp++;
                longTmp++;
            }
        }

        cur++;
    }

    if (longTmp > longestLen)
        longestLen = longTmp;

    return longestLen;
}

int main()
{
    char* str = "anviaj";
    int n = lengthOfLongestSubstring(str);
    return 0;
}