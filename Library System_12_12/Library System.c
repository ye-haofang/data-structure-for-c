#define _CRT_SECURE_NO_WARNINGS 1
#include"Library System.h"

//2022_12_14 消除 Gitee 的违规提示

//2022_12_29 修改，新增部分注释，使程序更加健壮、安全、可靠

static ListNode* buyNewNode(LTDataType x);	//前置声明
static int ListSize(ListNode* pHead);	//统计长度
static int menu();	//服务于操作对象选择的菜单

void ListLoad(ListNode* pHead)	//加载文件函数
{
	assert(pHead);	//断言，防止空指针

	//打开文件读取
	FILE* pf = fopen("叶豪方35_bk.txt", "r+");
	if (!pf)
	{
		perror("fopen :: fail");
		return;
	}

	char ch[30] = "0";
	fscanf(pf, "%s %s %s %s %s %s %s", 
		ch, ch, ch, ch, ch, ch, ch);	//读取标头
	LTDataType tmp;
	while ((fscanf(pf, "%d %s %s %s %s %zu %lf", 
		&(tmp.id), tmp.type, tmp.name, tmp.author, tmp.press, &(tmp.number), &(tmp.price))) >= 1)
	{
		ListPushBack(pHead, tmp);	//直接尾插
	}

	//判断因何而读取结束
	if (feof(pf))
		printf("\nEnd by EOF\n");	//读取到文件末尾正常结束
	else if (ferror(pf))
		printf("\nEnd by IO\n");	//中途读取失败而结束

	fclose(pf);
	pf = NULL;
}

// 创建返回链表的头结点
ListNode* ListCreate()
{
	//头节点（哨bing位不存储数据）
	ListNode* gard = (ListNode*)malloc(sizeof(ListNode));
	if (!gard)
	{
		perror("malloc :: fail");
		return NULL;
	}

	memset(gard, 0, sizeof(ListNode));	//全部初始化为0

	//链接
	gard->prev = gard;
	gard->next = gard;
	
	//加载文件
	ListLoad(gard);

	return gard;
}

void ListWrite(ListNode* pHead)	//写入文件
{
	assert(pHead);

	//打开文件写入
	FILE* pf = fopen("叶豪方35_bk.txt", "w");
	if (!pf)
	{
		perror("fopen :: fail");
		return;
	}

	//先写入标头
	fprintf(pf, "%-2s\t%-10s\t%-20s\t%-10s\t%-30s\t%-2s\t%-2s\n",
		"编号", "分类", "书名", "作者", "出版社", "数量", "单价");
	int n = ListSize(pHead);
	ListNode* cur = pHead->next;
	while (n--)
	{
		ListNode* next = cur->next;
		fprintf(pf, "%-2d\t%-10s\t%-20s\t%-10s\t%-30s\t %-2zu\t%-2.2lf\n",
			cur->data.id, cur->data.type, cur->data.name, cur->data.author,
			cur->data.press, cur->data.number, cur->data.price);
		cur = next;
	}

	fclose(pf);
	pf = NULL;
}

// 双向链表销毁
void ListDestory(ListNode* pHead)
{
	assert(pHead);

	//写入文件
	ListWrite(pHead);

	//思路：遍历整个链表，挨个销毁
	ListNode* cur = pHead->next;	//存储下一个节点
	while (cur != pHead)
	{
		ListNode* next = cur->next;
		free(cur);	//释放
		cur = next;
	}

	free(pHead);	//释放哨bing位
}

// 双向链表打印
void ListPrint(const ListNode* pHead)
{
	assert(pHead);

	//打印标头
	printf("%-2s\t%-10s\t%-20s\t%-10s\t%-30s\t%-2s\t%-2s\n",
		"编号", "分类", "书名", "作者", "出版社", "数量", "单价");
	ListNode* cur = (ListNode*)pHead->next;
	while (cur != (ListNode*)pHead)
	{
		ListNode* next = cur->next;
		printf("%-2d\t%-10s\t%-20s\t%-10s\t%-30s\t%-2zu\t%-2.2lf\n",
			cur->data.id,cur->data.type,cur->data.name,cur->data.author,
			cur->data.press,cur->data.number,cur->data.price);	//打印信息
		cur = next;
	}
}

// 打印节点
void ListPrintNode(ListNode* node)
{
	assert(node);

	ListNode* cur = node;
	printf("%-2d\t%-10s\t%-20s\t%-10s\t%-30s\t%-2zu\t%-2.2lf\n",
		cur->data.id, cur->data.type, cur->data.name, cur->data.author,
		cur->data.press, cur->data.number, cur->data.price);	//打印信息
}

//打印标题
void ListPrintfTitle()
{
	//打印标头
	printf("%-2s\t%-10s\t%-20s\t%-10s\t%-30s\t%-2s\t%-2s\n",
		"编号", "分类", "书名", "作者", "出版社", "数量", "单价");
}

//购买节点只允许在这个源文件中使用
static ListNode* buyNewNode(const LTDataType x)
{
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (!newnode)
	{
		perror("malloc :: fail");
		return NULL;
	}

	newnode->data = x;	//结构体直接赋值
	newnode->prev = newnode;
	newnode->next = newnode;

	return newnode;	//返回买好的节点
}

// 双向链表尾插
void ListPushBack(ListNode* pHead, const LTDataType x)
{
	assert(pHead);

	//思路：找尾，插入
	//双向带头循环链表很简单
	ListNode* newnode = buyNewNode(x);	//买了一个新节点

	ListNode* cur = pHead->prev;	//头的上一个，就是尾

	//更改链接关系，O(1)
	cur->next = newnode;
	newnode->prev = cur;
	newnode->next = pHead;
	pHead->prev = newnode;
	
	printf("插入成功\n");
}

// 双向链表尾删
void ListPopBack(ListNode* pHead)
{
	assert(pHead);
	if (pHead->next == pHead)	//相等，说明只有一个哨bing位，不能删除
	{
		printf("当前链表为空，无法删除\n");
		return;
	}

	ListNode* cur = pHead->prev;	//跟尾插相似的原理
	ListNode* tmp = cur->prev;	//待删除节点的上一个节点
	tmp->next = pHead;
	pHead->prev = tmp;

	free(cur);
	cur = NULL;

	printf("删除成功\n");
}

// 双向链表头插
void ListPushFront(ListNode* pHead, const LTDataType x)
{
	assert(pHead);

	//买节点
	ListNode* newnode = buyNewNode(x);

	//所谓头，就是哨bing位的下一个节点
	ListNode* cur = pHead->next;

	//改变链接关系
	pHead->next = newnode;
	newnode->prev = pHead;
	newnode->next = cur;
	cur->prev = newnode;

	printf("插入成功\n");
}

// 双向链表头删
void ListPopFront(ListNode* pHead)
{
	assert(pHead);
	if (pHead->next == pHead)	//相等，说明只有一个哨bing位，不能删除
	{
		printf("当前链表为空，无法删除\n");
		return;
	}

	ListNode* cur = pHead->next;
	ListNode* tmp = cur->next;
	pHead->next = tmp;
	tmp->prev = pHead;

	free(cur);
	cur = NULL;

	printf("删除成功\n");
}

// 双向链表查找
ListNode* ListFind(const ListNode* pHead)
{
	assert(pHead);

	printf("现在选择的是查找的对象\n");
	int input = menu();
	system("cls");
	if (input == 0 || input > 7)
	{
		printf("因选择退出或选择有误，退出程序!\n");
		return NULL;
	}

	ListNode* cur = (ListNode*)pHead->next;	//强转
	ListNode* tmp = NULL;
	bool flag = true;	//只允许tmp被赋值一次
	//设置flag的原因：查找某本书并进行操作时出现多个目标，默认为第一本

	int n = 0;	//待查找目标为整型
	char str[30] = "0";	//待查找目标为字符
	size_t number = 0;	//待查找目标为无符号整型
	double price = 0;	//待查找目标为浮点型

	switch (input)
	{
	case 1:
		printf("请输入待查找编号:>");
		scanf("%d", &n);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (cur->data.id == n)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 2:
		printf("请输入待查找分类:>");
		scanf("%s", str);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (strcmp(cur->data.type, str) == 0)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 3:
		printf("请输入待查找书名:>");
		scanf("%s", str);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (strcmp(cur->data.name, str) == 0)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 4:
		printf("请输入待查找作者:>");
		scanf("%s", str);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (strcmp(cur->data.author, str) == 0)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 5:
		printf("请输入待查找出版社:>");
		scanf("%s", str);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (strcmp(cur->data.press, str) == 0)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 6:
		printf("请输入待查找数量:>");
		scanf("%zu", &number);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (cur->data.number == number)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	case 7:
		printf("请输入待查找价格:>");
		scanf("%lf", &price);
		printf("\n查找结果如下:\n");
		ListPrintfTitle();
		while (cur != (ListNode*)pHead)
		{
			ListNode* next = cur->next;
			if (cur->data.price == number)
			{
				if (flag)
				{
					tmp = cur;
					flag = false;
				}
				ListPrintNode(cur);	//打印找到的节点
			}
			cur = next;
		}
		break;
	}

	return tmp;
}

static int menu()	//服务于操作对象选择的菜单
{
	int input = 0;
	printf("0.退出\n");
	printf("1.编号\n");
	printf("2.分类\n");
	printf("3.书名\n");
	printf("4.作者\n");
	printf("5.出版社\n");
	printf("6.数量\n");
	printf("7.单价\n");
	printf("请选择操作对象:>");
	scanf("%d", &input);
	return input;
}

// 双向链表的修改
void ListModify(ListNode* pHead, ListNode* dst)	//可以调用查找
{
	assert(pHead);

	ListNode* tmp = dst;
	if (tmp)
	{
		int input = 1;
		int num = 0;
		double price = 0;
		char str[30] = "0";
		while (input)
		{
flag :
			printf("\n现在选择的是修改后的值\n");
			int input = menu();
			system("cls");
			switch (input)
			{
			case 0:
				printf("退出修改\n");
				return;
			case 1:
				printf("修改值[编号]:>");
				scanf("%d", &num);
				tmp->data.id = num;
				break;
			case 2:
				printf("修改值[分类]:>");
				scanf("%s", str);
				strcpy(tmp->data.type, str);
				break;
			case 3:
				printf("修改值[书名]:>");
				scanf("%s", str);
				strcpy(tmp->data.name, str);
				break;
			case 4:
				printf("修改值[作者]:>");
				scanf("%s", str);
				strcpy(tmp->data.author, str);
				break;
			case 5:
				printf("修改值[出版社]:>");
				scanf("%s", str);
				strcpy(tmp->data.press, str);
				break;
			case 6:
				printf("修改值[数量]:>");
				scanf("%d", &num);
				tmp->data.number = num;
				break;
			case 7:
				printf("修改值[单价]:>");
				scanf("%lf", &price);
				tmp->data.price = price;
				break;
			default :
				printf("选择错误，请重新选择\n");
				goto flag;
			}
			printf("修改成功\n");
		}
	}
	else
		printf("没有找到这本书，修改失败\n");
}

// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, const LTDataType x)
{
	assert(pos);

	//这个也是需要配合查找
	if (pos)
	{
		ListNode* newnode = buyNewNode(x);
		ListNode* prev = pos->prev;

		prev->next = newnode;
		newnode->prev = prev;
		newnode->next = pos;
		pos->prev = newnode;
		printf("插入成功\n");
	}
	else
		printf("没有这本书，插入失败\n");
}

// 双向链表删除pos位置的节点
void ListErase(ListNode* pos)
{
	if (pos)
	{
		//取前取后
		ListNode* prev = pos->prev;
		ListNode* next = pos->next;

		//更改链接
		prev->next = next;
		next->prev = prev;

		//释放
		free(pos);
		pos = NULL;

		printf("删除成功\n");
	}
	else
		printf("\n没有这本书，删除失败\n");
}

static int ListSize(ListNode* pHead)	//统计长度
{
	assert(pHead);

	int count = 0;
	ListNode* cur = pHead->next;
	while (cur != pHead)
	{
		ListNode* next = cur->next;
		count++;
		cur = next;
	}

	return count;
}

ListNode* getNode(ListNode* pHead, int n)
{
	assert(pHead);

	ListNode* cur = pHead->next;
	while (--n)
		cur = cur->next;

	return cur;
}

//排序全部保护起来
static void ShellSort(ListNode* pHead)	//希尔排序
{
	assert(pHead);

	int n = ListSize(pHead);
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 1; i <= n - gap; i++)
		{
			int end = i;
			ListNode* node1 = getNode(pHead, end + gap);
			ListNode* node2 = NULL;
			Library tmp = node1->data;	//待插入值

			while (end)
			{
				node2 = getNode(pHead, end);
				if (tmp.id < node2->data.id)
				{
					node1->data = node2->data;
					node1 = node2;
				}
				end--;
			}
			node1->data = tmp;	//插入
		}
	}
}

static void Swap(ListNode * node1, ListNode * node2)
{
	assert(node1 && node2);

	Library tmp = node1->data;
	node1->data = node2->data;
	node2->data = tmp;
}

static void AdjustDown(ListNode* pHead, int n, int parent)	//向下调整
{
	assert(pHead);

	ListNode* cur = pHead->next;	//去除哨bing位

	//找到大孩子，假设大孩子为左孩子
	int child = parent * 2;	//左孩子
	while (child <= n)
	{
		ListNode* node1 = getNode(pHead, parent);
		ListNode* node2 = getNode(pHead, child);
		if (child + 1 <= n)
		{
			ListNode* tmp = getNode(pHead, child + 1);	//右孩子
			if (strcmp(node2->data.name, tmp->data.name) == -1)
				child++;	//矫正大孩子
		}

		node2 = getNode(pHead, child);

		//大堆：如果父亲小于大孩子，就交换
		if (strcmp(node1->data.name, node2->data.name) == -1)
		{
			Swap(node1, node2);
			//父亲变成大孩子，更新大孩子
			parent = child;
			child = parent * 2;
		}
		else
			break;	//否则就退出
	}
}

static void HeapSort(ListNode* pHead)	//堆排序
{
	assert(pHead);

	//升序，建大堆，降序，建小堆

	int n = ListSize(pHead);

	//建大堆
	int parent = n / 2;	//找到最后一个孩子的父亲
	while (parent > 0)
	{
		AdjustDown(pHead, n, parent);	//可能有bug
		parent--;
	}

	//沉底，调整
	for (int i = n; i > 1; i--)
	{
		ListNode* node1 = getNode(pHead, 1);
		ListNode* node2 = getNode(pHead, i);

		Swap(node1, node2);

		AdjustDown(pHead, i - 1, 1);
	}
}

static void _QuickSort(ListNode* pHead, int begin, int end)
{
	assert(pHead);

	//此时数据已经全部分割排序完了
	if (begin >= end)
		return;

	int key = begin;
	int left = key + 1;
	int right = end;
	ListNode* keyNode = getNode(pHead, key);
	ListNode* leftNode = getNode(pHead, left);
	ListNode* rightNode = getNode(pHead, right);
	while (left < right)
	{
		//右边要找到比 key 小的
		while (left < right && rightNode->data.price > keyNode->data.price)
		{
			right--;
			rightNode = getNode(pHead, right);
		}

		//左边要找到比 key 大的
		while (left < right && leftNode->data.price <= keyNode->data.price)
		{
			left++;
			leftNode = getNode(pHead, left);
		}

		Swap(leftNode, rightNode);	//交换
	}

	//确认一下，排除bug
	if (keyNode->data.price > leftNode->data.price)
		Swap(keyNode, leftNode);

	_QuickSort(pHead, begin, left - 1);
	_QuickSort(pHead, left + 1, end);
}

//注：未加优化的快排，性能较弱
static void QuickSort(ListNode* pHead)	//快排，霍尔初始版
{
	assert(pHead);

	int n = ListSize(pHead);

	_QuickSort(pHead, 1, n);	//递归主程序
}

// 双向链表的排序
void ListSort(ListNode* pHead)
{
	assert(pHead);

	int input = 0;
flag:

	printf("请选择排序方式：\n1.希尔（按编号排序）	\n2.堆排（按书名排序）	\n3.快排（按价格排序） \n:>");
	scanf("%d", &input);
	system("cls");

	switch (input)
	{
	case 1:
		ShellSort(pHead);
		break;
	case 2:
		HeapSort(pHead);
		break;
	case 3:
		QuickSort(pHead);
		break;
	default:
		printf("选择失败，重新选择\n");
		goto flag;
	}
}

//查找模块
void BookFind(ListNode* pHead)
{
	assert(pHead);

	ListNode* tmp = ListFind(pHead);
	if (tmp)
		printf("查找结果如上\n");
	else
		printf("\n没有找到相关信息\n");
}

void menu3()
{
	printf("\n ------------\n");
	printf("| 0.退出插入 |\n");
	printf("| 1.尾插数据 |\n");
	printf("| 2.头插数据 |\n");
	printf("| 3.任意插入 |\n");
	printf(" ------------\n");
}

void NodeAdd(LTDataType* tmp)
{
	printf("\n请输入待增加节点信息:\n");
	printf("编号:>");
	scanf("%d", &(tmp->id));
	printf("分类:>");
	scanf("%s", tmp->type);
	printf("书名:>");
	scanf("%s", tmp->name);
	printf("作者:>");
	scanf("%s", tmp->author);
	printf("出版社:>");
	scanf("%s", tmp->press);
	printf("数量:>");
	scanf("%zu", &(tmp->number));
	printf("单价:>");
	scanf("%lf", &(tmp->price));
}

//插入模块
void BookInsert(ListNode* pHead)
{
	assert(pHead);

	int input = 1;
	while (input)
	{
		menu3();
		printf("请选择插入方式:>");
		scanf("%d", &input);
		system("cls");
		ListNode* dst = NULL;
		LTDataType tmp;
		if (input > 0 && input < 3)
			NodeAdd(&tmp);
		switch (input)
		{
		case 0:
			printf("退出插入模块\n");
			break;
		case 1:
			ListPushBack(pHead, tmp);
			break;
		case 2:
			ListPushFront(pHead, tmp);
			break;
		case 3:
			dst = ListFind(pHead);	//先找到节点，在它之前插入数据
			NodeAdd(&tmp);
			ListInsert(dst, tmp);
			break;
		default :
			printf("选择错误\n");
			break;
		}
	}
}

void menu4()
{
	printf("\n ------------\n");
	printf("| 0.退出删除 |\n");
	printf("| 1.尾删数据 |\n");
	printf("| 2.头删数据 |\n");
	printf("| 3.任意删除 |\n");
	printf(" ------------\n");
}

//删除模块
void BookEarse(ListNode* pHead)
{
	assert(pHead);

	int input = 1;
	while (input)
	{
		menu4();
		printf("请选择删除方式:>");
		scanf("%d", &input);
		system("cls");
		ListNode* dst = NULL;
		switch (input)
		{
		case 0:
			printf("退出删除模块\n");
			break;
		case 1:
			ListPopBack(pHead);
			break;
		case 2:
			ListPopFront(pHead);
			break;
		case 3:
			dst = ListFind(pHead);	//先找到节点，然后删除
			ListErase(dst);
			break;
		default:
			printf("选择错误\n");
			break;
		}
	}
}

//修改模块
void BookModify(ListNode* pHead)
{
	assert(pHead);

	ListNode* dst = ListFind(pHead);
	if (dst)
		ListModify(pHead, dst);
	else
		printf("没有找到目标，无法修改\n");
}

//排序模块
void BookSort(ListNode* pHead)
{
	assert(pHead);
	ListSort(pHead);
	ListPrint(pHead);
}