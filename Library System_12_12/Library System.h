#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<windows.h>
#include<stdbool.h>

//需求：创建、查找、插入、删除、更新（修改）、排序（希尔、堆排、快排）
//双向带头循环链表


typedef struct Library
{
	int id;	//编号
	char type[10];	//分类
	char name[20];	//书名
	char author[10];	//作者
	char press[30];	//出版社
	size_t number;	//数量
	double price;	//单价
}Library;

typedef Library LTDataType;	//链表的数据类型

typedef struct ListNode
{
	LTDataType data;	//数据域
	struct ListNode* prev;	//前指针
	struct ListNode* next;	//后指针
}ListNode;

// 创建返回链表的头结点
ListNode* ListCreate();

// 双向链表销毁
void ListDestory(ListNode* pHead);

// 双向链表打印
void ListPrint(const ListNode* pHead);	//不进行修改，保护

// 双向链表尾插
void ListPushBack(ListNode* pHead, const LTDataType x);	//待插入数据需要保护

// 双向链表尾删
void ListPopBack(ListNode* pHead);

// 双向链表头插
void ListPushFront(ListNode* pHead, const LTDataType x);	//待插入数据需要保护

// 双向链表头删
void ListPopFront(ListNode* pHead);

// 双向链表查找
ListNode* ListFind(const ListNode* pHead);	//待查找数据不能被修改，保护

// 双向链表的修改
void ListModify(ListNode* pHead, ListNode* dst);	//可以调用查找

// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, const LTDataType x);	//保护待插入数据

// 双向链表删除pos位置的节点
void ListErase(ListNode* pos);

// 双向链表的排序
void ListSort(ListNode* pHead);

//-----------------------------------

//查找模块
void BookFind(ListNode* pHead);

//插入模块
void BookInsert(ListNode* pHead);

//删除模块
void BookEarse(ListNode* pHead);

//修改模块
void BookModify(ListNode* pHead);

//排序模块
void BookSort(ListNode* pHead);