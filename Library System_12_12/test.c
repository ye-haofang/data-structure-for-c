#define _CRT_SECURE_NO_WARNINGS 1	
#include"Library System.h"

void TestDemo1()
{
	//测试：创建、销毁、打印
	ListNode* head = ListCreate();

	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void TestDemo2()
{
	//测试：尾插尾删、头插头删
	ListNode* head = ListCreate();

	LTDataType b1 = { 1,"社会科学","寻觅中华","余秋雨","作家出版社",1,38 };
	LTDataType b2 = { 2,"自然科学","C程序设计","谭浩强","清华大学出版社",3,26 };
	LTDataType b3 = { 3,"英文书","博弈 The Game","Jones","Oversea Publishing House",1,87 };

	printf("\n测试尾插\n");
	ListPushBack(head, b1);
	ListPushBack(head, b2);
	ListPrint(head);

	printf("\n测试尾删\n");
	ListPopBack(head);
	ListPrint(head);

	printf("\n测试头插\n");
	ListPushFront(head,b1);
	ListPushFront(head,b2);
	ListPushFront(head,b3);
	ListPrint(head);

	printf("\n测试头删\n");
	ListPopFront(head);
	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void TestDemo3()
{
	//测试：查找、修改
	ListNode* head = ListCreate();

	LTDataType b1 = { 1,"社会科学","寻觅中华","余秋雨","作家出版社",1,38 };
	LTDataType b2 = { 2,"自然科学","C程序设计","谭浩强","清华大学出版社",3,26 };
	LTDataType b3 = { 3,"英文书","博弈 The Game","Jones","Oversea Publishing House",1,87 };

	ListPushBack(head, b1);
	ListPushBack(head, b2);
	ListPushBack(head, b3);
	ListPrint(head);

	printf("\n测试查找\n");
	printf("%p\n", ListFind(head));

	printf("\n测试修改 -> 寻觅中华\n");
	ListModify(head, ListFind(head));
	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void TestDemo4()
{
	//测试：任意位置插入删除，排序
	ListNode* head = ListCreate();

	LTDataType b1 = { 1,"社会科学","寻觅中华","余秋雨","作家出版社",1,38 };
	LTDataType b2 = { 2,"自然科学","C程序设计","谭浩强","清华大学出版社",3,26 };
	LTDataType b3 = { 3,"英文书","博弈 The Game","Jones","Oversea Publishing House",1,87 };

	ListPushBack(head, b1);
	ListPushBack(head, b2);
	ListPushBack(head, b3);
	ListPrint(head);

	printf("\n任意位置插入\n");
	ListInsert(ListFind(head), b3);
	ListPrint(head);

	printf("\n任意位置删除\n");
	ListErase(ListFind(head));
	ListPrint(head);

	ListPushFront(head, b1);
	ListPushFront(head, b2);
	ListPushFront(head, b3);

	printf("\n链表排序\n");
	ListSort(head);
	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void TestDemo5()
{
	//测试：排序专场（希尔、堆排、快排）
	ListNode* head = ListCreate();

	LTDataType b1 = { 1,"社会科学","8","余秋雨","作家出版社",1,38 };
	LTDataType b2 = { 2,"自然科学","5","谭浩强","清华大学出版社",3,26 };
	LTDataType b3 = { 3,"英文书","6","Jones","Oversea Publishing House",1,87 };
	LTDataType b4 = { 1,"社会科学","7","余秋雨","作家出版社",1,38 };
	LTDataType b5 = { 2,"自然科学","3","谭浩强","清华大学出版社",3,26 };
	LTDataType b6 = { 3,"英文书","1","Jones","Oversea Publishing House",1,87 };
	LTDataType b7 = { 1,"社会科学","2","余秋雨","作家出版社",1,38 };
	LTDataType b8 = { 2,"自然科学","9","谭浩强","清华大学出版社",3,26 };

	ListPushBack(head, b1);
	ListPushBack(head, b2);
	ListPushBack(head, b3);
	ListPushBack(head, b4);
	ListPushBack(head, b5);
	ListPushBack(head, b6);
	ListPushBack(head, b7);
	ListPushBack(head, b8);

	ListPrint(head);
	printf("\n排序专场\n");
	ListSort(head);
	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void TestDemo6()
{
	//测试：文件读取与各项功能
	//将文件的读取和写入内嵌在创建与销毁函数中
	ListNode* head = ListCreate();

	ListPrint(head);

	LTDataType b1 = { 1,"社会科学","寻觅中华","余秋雨","作家出版社",1,38 };
	LTDataType b2 = { 2,"自然科学","C程序设计","谭浩强","清华大学出版社",3,26 };
	LTDataType b3 = { 3,"英文书","博弈_The_Game","Jones","Oversea_Publishing_House",1,87 };

	ListPushBack(head, b1);
	ListPushBack(head, b2);
	ListPushBack(head, b3);

	printf("\n测试查找\n");
	printf("%p\n", ListFind(head));

	printf("\n测试修改 -> 寻觅中华\n");
	ListModify(head, ListFind(head));

	printf("\n任意位置插入\n");
	ListInsert(ListFind(head), b3);
	ListPrint(head);

	printf("\n任意位置删除\n");
	ListErase(ListFind(head));
	ListPrint(head);

	ListSort(head);
	ListPrint(head);

	ListDestory(head);
	head = NULL;
}

void menu2()
{
	printf("\n ------------\n");
	printf("| 0.退出菜单 |\n");
	printf("| 1.查找模块 |\n");
	printf("| 2.插入模块 |\n");
	printf("| 3.删除模块 |\n");
	printf("| 4.修改模块 |\n");
	printf("| 5.排序模块 |\n");
	printf("| 6.打印数据 |\n");
	printf(" ------------\n");
}

void LibrarySystem(ListNode* pHead)
{
	int input = 1;
	while (input)
	{
		menu2();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case 0:
			printf("回退至上一级\n");
			return;
			break;
		case 1:
			BookFind(pHead);
			break;
		case 2:
			BookInsert(pHead);
			break;
		case 3:
			BookEarse(pHead);
			break;
		case 4:
			BookModify(pHead);
			break;
		case 5:
			BookSort(pHead);
			break;
		case 6:
			ListPrint(pHead);
			break;
		default :
			printf("选择错误，请重新选择!\n");
			break;
		}
	}
}

void menu1()
{
	printf("\n ------------\n");
	printf("| 0.退出系统 |\n");
	printf("| 1.进入菜单 |\n");
	printf(" ------------\n");
}

int main()
{
	//TestDemo6();
	//测试阶段结束，开始编写菜单
	int input = 1;
	ListNode* head = ListCreate();
	while (input)
	{
		menu1();
		printf("欢迎进入图书管理系统，请选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case 0:
			printf("成功退出");
			ListDestory(head);
			break;
		case 1:
			LibrarySystem(head);
			break;
		default :
			printf("选择错误，请重新选择!\n");
			break;
		}
	}
	return 0;
}