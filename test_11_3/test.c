#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//继续深入学习链表

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
////203.移除链表元素
////不带哨兵位版
//struct ListNode* removeElements(struct ListNode* head, int val)
//{
//    struct ListNode* cur = head;    //节点委托
//    struct ListNode* prev, * tail;   //创建新链表的头节点和尾节点
//    prev = tail = NULL; //置空
//    while (cur)
//    {
//        //如果不是待删除元素，可以尾插到新链表中
//        if (cur->val != val)
//        {
//            //如果链表为空，需要特殊处理
//            if (!tail)
//                prev = tail = cur;  //此时头尾都为新节点
//            else
//                tail->next = cur;   //常规链接
//            tail = cur; //尾节点信息更新
//        }
//        cur = cur->next;    //向后寻找
//    }
//
//    //如果尾节点或头节点为空，即空表或全都是待删除元素的情况，需要直接返回NULL
//    if (!tail)
//        return prev;    //头尾都行，都是NULL
//    tail->next = NULL;  //新链表封顶，尾节点的下一个节点置空
//    return prev;    //返回头节点
//}


//typedef struct ListNode 
//{
//    int val;
//    struct ListNode* next;
// }SLTNode;
//
// //203.移除链表元素
// //带哨兵位版 guard
//#include<stdlib.h>
//#include<assert.h>
//struct ListNode* removeElements(struct ListNode* head, int val)
//{
//    struct ListNode* cur = head;    //节点委托
//    struct ListNode* guard, * tail;  //哨兵与尾
//    guard = tail = (struct ListNode*)malloc(sizeof(struct ListNode));   //申请
//    assert(guard && tail);
//    while (cur)
//    {
//        //如果不是待删除元素，可以尾插到新链表中
//        if (cur->val != val)
//        {
//            //如果链表为空，需要特殊处理
//            if (!tail)
//                guard->next = tail = cur;  //此时头尾都为新节点
//            else
//                tail->next = cur;   //常规链接
//            tail = cur; //尾节点信息更新
//        }
//        cur = cur->next;    //向后寻找
//    }
//
//    tail->next = NULL;  //新链表封顶，尾节点的下一个节点置空
//
//    head = guard->next; //链表复位
//    free(guard);
//    guard = tail = NULL;
//    return head;    //返回头节点
//}
//
//extern void SLTPushBack(SLTNode**, int*, int);
//
//int main()
//{
//    int arr[6] = { 6,6,6,6,6,6 };
//    SLTNode* s = NULL;
//    SLTPushBack(&s, arr, sizeof(arr) / sizeof(arr[0]));
//    s = removeElements(s, 6);   //树已栽好，一劳永逸
//    while (s)
//    {
//        printf("%d ", s->val);
//        s = s->next;
//    }
//    return 0;
//}

//typedef struct ListNode 
//{
//    int val;
//    struct ListNode* next;
//}SLTNode;
//
////21.合并两个有序链表
////不带哨兵节点版
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
//{
//    struct ListNode* p1 = list1;
//    struct ListNode* p2 = list2;    //节点委托
//    //不带哨兵位，就需要特殊处理一下
//    if (!p1)
//        return p2;
//    else if (!p2)
//        return p1;
//    struct ListNode* prev, * tail;
//    prev = tail = NULL; //头节与尾节点
//    while (p1 && p2)
//    {
//        //将p1、p2中较小的节点尾插到新链表中
//        if (p1->val < p2->val)
//        {
//            //需要特殊处理，如果新链表为空，就直接赋值
//            if (!prev)
//                prev = tail = p1;
//            else
//                tail->next = p1;
//            tail = p1;  //尾节点向后移动
//            p1 = p1->next;  //p1向后移动
//        }
//        else
//        {
//            if (!prev)
//                prev = tail = p2;
//            else
//                tail->next = p2;
//            tail = p2;
//            p2 = p2->next;  //p2向后移动
//        }
//    }
//    //这里需要判断一下，是因谁而提前结束的，补上未链接的节点
//    if (!p1)
//        tail->next = p2;
//    if (!p2)
//        tail->next = p1;
//    return prev;    //返回头节点
//}
//
//extern void SLTPushBack(SLTNode**, int*, int);
//
//int main()
//{
//    int arr1[] = { 1,2,4 };
//    int arr2[] = { 1,3,4 };
//    SLTNode* s1 = NULL;
//    SLTNode* s2 = NULL;
//    SLTPushBack(&s1, arr1, 3);
//    SLTPushBack(&s2, arr2, 3);
//
//    s1 = mergeTwoLists(s1, s2);
//    while (s1)
//    {
//        printf("%d ", s1->val);
//        s1 = s1->next;
//    }
//    return 0;
//}

///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     struct ListNode *next;
// * };
// */
//
// //21.合并两个有序链表
// //哨兵节点版 guard
//#include<stdlib.h>
//#include<assert.h>
//
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
//{
//    struct ListNode* p1 = list1;
//    struct ListNode* p2 = list2;    //节点委托
//    //有了哨兵节点，就不需要进行特殊处理
//    struct ListNode* guard, * tail;
//    guard = tail = (struct ListNode*)malloc(sizeof(struct ListNode));   //开辟节点
//    assert(guard && tail);
//    while (p1 && p2)
//    {
//        //将p1、p2中较小的节点尾插到新链表中
//        if (p1->val < p2->val)
//        {
//            //需要特殊处理，如果新链表为空，就直接赋值
//            if (!guard)
//                guard = tail = p1;
//            else
//                tail->next = p1;
//            tail = p1;  //尾节点向后移动
//            p1 = p1->next;  //p1向后移动
//        }
//        else
//        {
//            if (!guard)
//                guard = tail = p2;
//            else
//                tail->next = p2;
//            tail = p2;
//            p2 = p2->next;  //p2向后移动
//        }
//    }
//    //这里需要判断一下，是因谁而提前结束的，补上未链接的节点
//    if (!p1)
//        tail->next = p2;
//    if (!p2)
//        tail->next = p1;
//    struct ListNode* prev = guard->next;
//    free(guard);    //释放堆区空间
//    guard = tail = NULL;    //置空
//    return prev;    //返回头节点
//}